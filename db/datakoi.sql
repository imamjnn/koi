-- MySQL dump 10.13  Distrib 5.7.16, for Linux (x86_64)
--
-- Host: localhost    Database: db_koiv2
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `auction`
--

LOCK TABLES `auction` WRITE;
/*!40000 ALTER TABLE `auction` DISABLE KEYS */;
INSERT INTO `auction` VALUES (21,1,'just to test','just-to-test','','2016-12-14 00:00:00','2017-01-20 09:05:00',2,NULL,NULL,NULL,4,'2016-12-15 02:49:14');
/*!40000 ALTER TABLE `auction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `auction_item`
--

LOCK TABLES `auction_item` WRITE;
/*!40000 ALTER TABLE `auction_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `auction_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `banner`
--

LOCK TABLES `banner` WRITE;
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` VALUES (3,1,'Shisuke','/media/61/e6/91/61e69154050851c0ca5dfc364db0ca55.jpg','Shinsuke','','2017-12-31 00:00:00','2016-12-03 03:49:22'),(4,1,'Flexy Coat','/media/a5/b2/af/a5b2af0343ac207f22cae1502d6060d5.jpg','Flex Coat','','2017-12-31 00:00:00','2016-12-03 03:50:05'),(5,1,'Bossco','/media/44/c1/d1/44c1d1f534457f0d3c11c8efb4a50060.jpg','Bossco','','2017-12-31 00:00:00','2016-12-03 03:50:34'),(6,1,'JPD','/media/79/fd/c3/79fdc33246b8db5b3512b3c2d83ae735.jpg','JPD','','2017-12-31 00:00:00','2016-12-03 03:51:51');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `budget_bid`
--

LOCK TABLES `budget_bid` WRITE;
/*!40000 ALTER TABLE `budget_bid` DISABLE KEYS */;
/*!40000 ALTER TABLE `budget_bid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'End of Year Demo','end-of-year-demo','/media/6e/f6/92/6ef692479202d0c267c0f3565d378af2.jpg','<p>HSasfewfhneifeijfekjgre</p>','trial','','2017-12-31 00:00:00','','','','','2016-12-03 03:59:17');
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `gallery_media`
--

LOCK TABLES `gallery_media` WRITE;
/*!40000 ALTER TABLE `gallery_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `item_bid`
--

LOCK TABLES `item_bid` WRITE;
/*!40000 ALTER TABLE `item_bid` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_bid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `item_breeder`
--

LOCK TABLES `item_breeder` WRITE;
/*!40000 ALTER TABLE `item_breeder` DISABLE KEYS */;
INSERT INTO `item_breeder` VALUES (4,1,'Dainichi Koi Farm','/media/ac/15/70/ac157093668bd45664d995d4e0eca060.jpg','','432423','glenardojopie@gmail.com','',2,'2016-12-21 16:46:01',2,'2016-12-03 04:09:50'),(5,1,'Sakai FIsh Farm','/media/ce/2d/0f/ce2d0f0501295928c93388e33d71ad0b.jpg','','543543','glenardojopie@gmail.com','',1,'2016-12-22 16:01:50',2,'2016-12-03 04:10:48'),(6,1,'Marujyu Koi Farm','/media/50/75/71/5075714fe7a8c22c551f54de3205f8a2.jpg','','54345','glenardojopie@gmail.com','',1,'2016-12-23 09:50:36',2,'2016-12-03 04:11:30'),(7,1,'Shinoda Koi Farm','/media/03/11/af/0311af8a08cb6169c206913630dcceaf.jpg','','45435','glenardojopie@gmail.com','',1,'2016-12-21 16:46:15',2,'2016-12-03 04:12:11'),(8,1,'Beppu Fish Farm','/media/31/85/cb/3185cb145e3422aa1d413fe454f71f1c.jpg','','45353','glenardojopie@gmail.com','',0,'2016-12-21 16:46:11',2,'2016-12-03 04:12:42'),(9,1,'Isa Koi Farm','/media/62/a9/c0/62a9c085104bbd58e06ce6dcfff6393c.jpg','','53532','glenardojopie@gmail.com','',1,'2016-12-07 02:50:47',2,'2016-12-03 04:13:23'),(10,1,'Tomezou Koi Farm','','','534543','glenardojopie@gmail.com','',1,'2016-12-08 01:04:33',2,'2016-12-03 04:14:05'),(11,1,'Taniguchi Koi Farm','','','efrewfew','gadingkoi@gmail.com','',1,'2016-12-21 16:46:26',2,'2016-12-08 00:32:34'),(12,1,'Sakazume Koi Farm','','','325233','glenardojopie@gmail.com','',1,'2016-12-21 16:46:19',2,'2016-12-08 00:34:46'),(13,1,'Maruyama Koi Farm','','','4253','glenardojopie@gmail.com','',1,'2016-12-08 01:08:07',2,'2016-12-08 00:35:10'),(14,1,'Momotaro Koi Farm','','','44','glenardojopie@gmail.com','',0,NULL,2,'2016-12-08 00:35:37'),(15,1,'Hiroi Koi Farm','','','3432','glenardojopie@gmail.com','',0,NULL,2,'2016-12-08 00:36:11'),(16,1,'Anneto Koi Club','','','35532','glenardojopie@gmail.com','',0,NULL,2,'2016-12-08 00:37:18'),(17,1,'Omosako Koi Farm','','','442142','glenardojopie@gmail.com','',0,NULL,2,'2016-12-08 00:37:48');
/*!40000 ALTER TABLE `item_breeder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `item_breeder_chain`
--

LOCK TABLES `item_breeder_chain` WRITE;
/*!40000 ALTER TABLE `item_breeder_chain` DISABLE KEYS */;
INSERT INTO `item_breeder_chain` VALUES (7,3,8,'2016-12-02 08:19:32'),(8,9,11,'2016-12-07 02:50:47'),(9,4,16,'2016-12-07 11:05:23'),(10,11,19,'2016-12-08 01:02:55'),(11,12,20,'2016-12-08 01:03:37'),(12,10,21,'2016-12-08 01:04:33'),(13,7,22,'2016-12-08 01:05:15'),(14,4,23,'2016-12-08 01:06:03'),(15,13,24,'2016-12-08 01:08:07'),(16,5,25,'2016-12-08 01:09:03'),(17,6,26,'2016-12-09 09:35:51');
/*!40000 ALTER TABLE `item_breeder_chain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `item_favorite`
--

LOCK TABLES `item_favorite` WRITE;
/*!40000 ALTER TABLE `item_favorite` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `item_favorite_chain`
--

LOCK TABLES `item_favorite_chain` WRITE;
/*!40000 ALTER TABLE `item_favorite_chain` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_favorite_chain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `item_variety`
--

LOCK TABLES `item_variety` WRITE;
/*!40000 ALTER TABLE `item_variety` DISABLE KEYS */;
INSERT INTO `item_variety` VALUES (11,'Kohaku','Khaki-demo',1,'',9,'2016-12-21 16:46:48','2016-12-02 08:17:49'),(12,'Sanke','sanke',1,'',0,'2016-12-21 16:46:06','2016-12-03 04:01:24'),(13,'Showa','showa',1,'',1,'2016-12-07 02:50:47','2016-12-03 04:01:29'),(14,'Shiro','shiro',1,'',0,NULL,'2016-12-03 04:01:34'),(15,'Ginrin Kohaku','ginrin-kohaku',1,'',0,'2016-12-23 09:50:36','2016-12-03 04:02:31'),(16,'Ginrin Showa','ginrin-showa',1,'',1,'2016-12-23 09:46:02','2016-12-03 04:03:12'),(17,'Ginrin Sanke','ginrin-sanke',1,'',0,NULL,'2016-12-03 04:03:22'),(18,'Ginrin Shiro','ginrin-shiro',1,'',0,NULL,'2016-12-03 04:03:30'),(19,'Tancho Ginrin','tancho-ginrin',1,'',0,NULL,'2016-12-03 04:03:46'),(20,'Kujaku','kujaku',1,'',1,'2016-12-21 16:46:10','2016-12-03 04:05:13'),(21,'Ochiba','ochiba',1,'',0,NULL,'2016-12-03 04:05:47'),(22,'Karashi','karashi',1,'',0,NULL,'2016-12-03 04:05:52'),(23,'Ogon','ogon',1,'',0,NULL,'2016-12-03 04:05:57'),(24,'Platinum','platinum',1,'',0,NULL,'2016-12-03 04:06:12'),(25,'Shusui','shusui',1,'',1,'2016-12-21 16:45:55','2016-12-03 04:06:25'),(26,'Hi Utsuri','hi-utsuri',1,'',0,NULL,'2016-12-03 04:06:30'),(27,'Tancho Sanke','tancho-sanke',1,'',0,NULL,'2016-12-03 04:06:38'),(28,'Tancho SHowa','tancho-showa',1,'',0,NULL,'2016-12-03 04:06:49'),(29,'Tancho Kohaku','tancho-kohaku',1,'',1,'2016-12-22 16:01:50','2016-12-03 04:07:31'),(30,'Goromo','goromo',1,'',1,'2016-12-21 16:46:19','2016-12-08 00:31:47'),(31,'Bekko','bekko',1,'',0,NULL,'2016-12-08 00:31:59');
/*!40000 ALTER TABLE `item_variety` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `item_variety_chain`
--

LOCK TABLES `item_variety_chain` WRITE;
/*!40000 ALTER TABLE `item_variety_chain` DISABLE KEYS */;
INSERT INTO `item_variety_chain` VALUES (9,11,9,'2016-12-07 02:44:12'),(10,11,10,'2016-12-07 02:45:46'),(11,13,11,'2016-12-07 02:50:47'),(12,11,15,'2016-12-07 11:04:42'),(13,11,16,'2016-12-07 11:05:23'),(14,11,18,'2016-12-08 01:01:54'),(15,11,19,'2016-12-08 01:02:55'),(16,30,20,'2016-12-08 01:03:37'),(17,25,21,'2016-12-08 01:04:33'),(18,16,22,'2016-12-08 01:05:15'),(19,11,23,'2016-12-08 01:06:03'),(20,11,24,'2016-12-08 01:08:07'),(21,29,25,'2016-12-08 01:09:03'),(22,20,26,'2016-12-09 09:35:51');
/*!40000 ALTER TABLE `item_variety_chain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (1,1,'Avatar-Wallpaper-Neytiri7.jpg','c15e604660b245a9d39a7db04e4030d4.jpg','/media/c1/5e/60/c15e604660b245a9d39a7db04e4030d4.jpg','/admin/me/setting.avatar',1,'2016-11-21 03:55:18'),(2,1,'kabaroto5.jpg','6eb83e5f4148de7e05668c3a88fa7032.jpg','/media/6e/b8/3e/6eb83e5f4148de7e05668c3a88fa7032.jpg','/admin/user.avatar',NULL,'2016-11-22 03:41:03'),(3,1,'Selection_001.png','0d6bc1b3ae57cb31613d4baf90c9760d.png','/media/0d/6b/c1/0d6bc1b3ae57cb31613d4baf90c9760d.png','/admin/auction/item.certificate',2,'2016-11-23 03:57:30'),(4,1,'photo_2016-11-02_15-52-55.jpg','a2142ec4ddde8540bc82b3cb38e8c6b2.jpg','/media/a2/14/2e/a2142ec4ddde8540bc82b3cb38e8c6b2.jpg','/admin/banner.media',NULL,'2016-11-23 04:25:09'),(5,1,'Selection_003.png','ce5235028a2cc374de3849cda39974ac.png','/media/ce/52/35/ce5235028a2cc374de3849cda39974ac.png','/admin/auction/item.photo',NULL,'2016-11-25 04:10:51'),(6,1,'isakoi.jpg','92d0d664b296a3ff68c3aab0afc8e070.jpg','/media/92/d0/d6/92d0d664b296a3ff68c3aab0afc8e070.jpg','/admin/supplier.media',NULL,'2016-11-28 06:11:13'),(7,1,'isakoi.jpg','8ac2433023bb2bfa35c7c846a6cb35dd.jpg','/media/8a/c2/43/8ac2433023bb2bfa35c7c846a6cb35dd.jpg','/admin/supplier.media',NULL,'2016-11-28 06:12:29'),(8,1,'sakai-ff.jpg','28b0caa82d33f523000698de431162a8.jpg','/media/28/b0/ca/28b0caa82d33f523000698de431162a8.jpg','/admin/supplier.media',NULL,'2016-11-28 06:13:05'),(9,1,'dainichi.jpg','7bbbdc7f6bb1214901c9305ee2ff3a1e.jpg','/media/7b/bb/dc/7bbbdc7f6bb1214901c9305ee2ff3a1e.jpg','/admin/supplier.media',NULL,'2016-11-28 06:13:30'),(10,1,'marujyu.jpg','119172daf6b5b4417d8a9d365efa297e.jpg','/media/11/91/72/119172daf6b5b4417d8a9d365efa297e.jpg','/admin/supplier.media',NULL,'2016-11-28 06:13:52'),(11,1,'bossco.jpg','cd6af4f3f96289c7c7278997c4ab9dff.jpg','/media/cd/6a/f4/cd6af4f3f96289c7c7278997c4ab9dff.jpg','/admin/banner.media',NULL,'2016-11-28 06:14:49'),(12,1,'flexy-coat.jpg','df592ea3552438a671cac20c9c3620ee.jpg','/media/df/59/2e/df592ea3552438a671cac20c9c3620ee.jpg','/admin/banner.media',NULL,'2016-11-28 06:15:34'),(13,1,'shinmaywa.jpg','29ba19f8b9c0858189dfa445b0dc318d.jpg','/media/29/ba/19/29ba19f8b9c0858189dfa445b0dc318d.jpg','/admin/auction/item.certificate',NULL,'2016-11-28 07:25:06'),(14,1,'bossco.jpg','c2e4f098d488b4ec2e5d5cfceaf3c340.jpg','/media/c2/e4/f0/c2e4f098d488b4ec2e5d5cfceaf3c340.jpg','/admin/auction/item.photo',NULL,'2016-11-28 07:25:09'),(15,1,'kolam-koi.jpg','6f156280aa6c7cbfc885061c695150e4.jpg','/media/6f/15/62/6f156280aa6c7cbfc885061c695150e4.jpg','/admin/auction/item/breeder.avatar',NULL,'2016-11-28 07:27:27'),(16,1,'koi-1.jpg','5df482b6938e0467384090c437a8d5ac.jpg','/media/5d/f4/82/5df482b6938e0467384090c437a8d5ac.jpg','/admin/auction/item.photo',NULL,'2016-11-28 09:09:22'),(17,1,'koi-1.jpg','03b382ae2a5efebdbf5e37f77869bee6.jpg','/media/03/b3/82/03b382ae2a5efebdbf5e37f77869bee6.jpg','/admin/auction/item.photo',NULL,'2016-11-28 09:11:37'),(18,1,'o0923r006.jpg','7df674e3373d0a943648c41524a41dbc.jpg','/media/7d/f6/74/7df674e3373d0a943648c41524a41dbc.jpg','/admin/auction/item.photo',NULL,'2016-11-29 06:35:17'),(19,1,'Selection_002.png','a67e3e6ff36c7004077380f2fb01ef07.png','/media/a6/7e/3e/a67e3e6ff36c7004077380f2fb01ef07.png','/admin/auction/item.certificate',NULL,'2016-11-29 06:35:29'),(20,1,'Selection_006.png','ca76d9b3b1d000f95f6e133a621391be.png','/media/ca/76/d9/ca76d9b3b1d000f95f6e133a621391be.png','/admin/auction/item.photo',NULL,'2016-11-29 08:47:14'),(21,1,'Selection_006.png','7f8c75cd967f13646f799d89fa39be64.png','/media/7f/8c/75/7f8c75cd967f13646f799d89fa39be64.png','/admin/gallery.cover',NULL,'2016-11-30 07:25:33'),(22,1,'Selection_001.png','7ec2d9c0514e1818a4f084dafe9006d7.png','/media/7e/c2/d9/7ec2d9c0514e1818a4f084dafe9006d7.png','/admin/auction/item.certificate',NULL,'2016-12-02 08:16:07'),(23,1,'jenis-koi-kohaku.jpg','5b088039526e6af2ede53cbe0fb140ee.jpg','/media/5b/08/80/5b088039526e6af2ede53cbe0fb140ee.jpg','/admin/auction/item.photo',NULL,'2016-12-02 08:16:17'),(24,1,'Avatar-Wallpaper-Neytiri7.jpg','77b9ae2ec98c77c972c56bf555155e38.jpg','/media/77/b9/ae/77b9ae2ec98c77c972c56bf555155e38.jpg','/admin/auction/item/breeder.avatar',NULL,'2016-12-02 08:18:03'),(25,1,'06 SAKAI LOGO.jpg','b8dec0f2ec122f6b366efcbe46b4c1dd.jpg','/media/b8/de/c0/b8dec0f2ec122f6b366efcbe46b4c1dd.jpg','/admin/supplier.media',NULL,'2016-12-03 03:44:44'),(26,1,'08 DAINICHI.jpg','9987920f8d02721989ea5d0b04144443.jpg','/media/99/87/92/9987920f8d02721989ea5d0b04144443.jpg','/admin/supplier.media',NULL,'2016-12-03 03:45:32'),(27,1,'12631551_766958343448756_1045128082500654208_n.jpg','7e9575bddee41c2003e2e5472120065b.jpg','/media/7e/95/75/7e9575bddee41c2003e2e5472120065b.jpg','/admin/supplier.media',NULL,'2016-12-03 03:45:51'),(28,1,'BeppuFF logo.jpg','a4af67d6cb7b89914fcf01d1df079e6b.jpg','/media/a4/af/67/a4af67d6cb7b89914fcf01d1df079e6b.jpg','/admin/supplier.media',NULL,'2016-12-03 03:46:22'),(29,1,'LOGO SAMURAI.jpg','5da203457708ac577f59e1e3fd44e074.jpg','/media/5d/a2/03/5da203457708ac577f59e1e3fd44e074.jpg','/admin/supplier.media',NULL,'2016-12-03 03:47:21'),(30,1,'logo.jpg','149820b20d6016c431f011d60ed2f69e.jpg','/media/14/98/20/149820b20d6016c431f011d60ed2f69e.jpg','/admin/supplier.media',NULL,'2016-12-03 03:47:39'),(31,1,'02_WEB_GADING_KOI_SHINSUKE_POSTER-resize.jpg','31d0ab9639c3532fc6b4d99afdf8a318.jpg','/media/31/d0/ab/31d0ab9639c3532fc6b4d99afdf8a318.jpg','/admin/banner.media',NULL,'2016-12-03 03:49:09'),(32,1,'Flexycoat Waterponding_Logo.jpg','83cad00be56fe4e34c8e97125000372d.jpg','/media/83/ca/d0/83cad00be56fe4e34c8e97125000372d.jpg','/admin/banner.media',NULL,'2016-12-03 03:49:41'),(33,1,'Logo Bossco (cutting sticker).jpg','82652ba092f8742e89f196de1ff8d4a6.jpg','/media/82/65/2b/82652ba092f8742e89f196de1ff8d4a6.jpg','/admin/banner.media',NULL,'2016-12-03 03:50:25'),(34,1,'IMG-20160406-WA0026.jpg','c000f3c572cb2c4550a0eadcfe8ca820.jpg','/media/c0/00/f3/c000f3c572cb2c4550a0eadcfe8ca820.jpg','/admin/banner.media',NULL,'2016-12-03 03:51:38'),(35,1,'1391866_478762022238653_1084226477_n.jpg','c3f576409a7ba87fdb8fb49f626a53f0.jpg','/media/c3/f5/76/c3f576409a7ba87fdb8fb49f626a53f0.jpg','/admin/supplier.media',NULL,'2016-12-03 03:52:34'),(36,1,'POSTER_KEEPING_CONTEST_REVISI.jpg','6ef692479202d0c267c0f3565d378af2.jpg','/media/6e/f6/92/6ef692479202d0c267c0f3565d378af2.jpg','/admin/event.cover',NULL,'2016-12-03 03:58:52'),(37,1,'08 DAINICHI.jpg','ac157093668bd45664d995d4e0eca060.jpg','/media/ac/15/70/ac157093668bd45664d995d4e0eca060.jpg','/admin/auction/item/breeder.avatar',NULL,'2016-12-03 04:08:29'),(38,1,'06 SAKAI LOGO.jpg','ce2d0f0501295928c93388e33d71ad0b.jpg','/media/ce/2d/0f/ce2d0f0501295928c93388e33d71ad0b.jpg','/admin/auction/item/breeder.avatar',NULL,'2016-12-03 04:10:30'),(39,1,'1391866_478762022238653_1084226477_n.jpg','5075714fe7a8c22c551f54de3205f8a2.jpg','/media/50/75/71/5075714fe7a8c22c551f54de3205f8a2.jpg','/admin/auction/item/breeder.avatar',NULL,'2016-12-03 04:11:01'),(40,1,'12631551_766958343448756_1045128082500654208_n.jpg','0311af8a08cb6169c206913630dcceaf.jpg','/media/03/11/af/0311af8a08cb6169c206913630dcceaf.jpg','/admin/auction/item/breeder.avatar',NULL,'2016-12-03 04:11:42'),(41,1,'BeppuFF logo.jpg','3185cb145e3422aa1d413fe454f71f1c.jpg','/media/31/85/cb/3185cb145e3422aa1d413fe454f71f1c.jpg','/admin/auction/item/breeder.avatar',NULL,'2016-12-03 04:12:21'),(42,1,'logo.jpg','62a9c085104bbd58e06ce6dcfff6393c.jpg','/media/62/a9/c0/62a9c085104bbd58e06ce6dcfff6393c.jpg','/admin/auction/item/breeder.avatar',NULL,'2016-12-03 04:13:07'),(43,1,'Screenshot_20160831-153651.png','87dfc616935ca474308a66b3a2135ac9.png','/media/87/df/c6/87dfc616935ca474308a66b3a2135ac9.png','/admin/auction/item.photo',NULL,'2016-12-07 02:43:34'),(44,1,'TAniguci 70.jpg','3ffb0b339d58f7289e8d9702ca8c17c9.jpg','/media/3f/fb/0b/3ffb0b339d58f7289e8d9702ca8c17c9.jpg','/admin/auction/item.photo',NULL,'2016-12-07 02:44:53'),(45,1,'ISA 100.jpg','486eb55e7ad74730151c0dcd699097ce.jpg','/media/48/6e/b5/486eb55e7ad74730151c0dcd699097ce.jpg','/admin/auction/item.photo',NULL,'2016-12-07 02:47:06'),(46,1,'13263718_1200688983299390_6158502289236585588_n.jpg','7bc89975fb7f44e51c13e94cae79c425.jpg','/media/7b/c8/99/7bc89975fb7f44e51c13e94cae79c425.jpg','/admin/auction/item.photo',NULL,'2016-12-07 11:00:14'),(47,1,'DSC_0009.JPG','87afb8fc28b835b8f0499f22cbc67624.JPG','/media/87/af/b8/87afb8fc28b835b8f0499f22cbc67624.JPG','/admin/auction/item.photo',NULL,'2016-12-07 11:02:08'),(48,1,'George Gani - TAncho - 65 cm - Tangerang - Import - Female - Handling Republik Gading Koi.JPG','edbb3ca981069abcea17a41c5a224b95.JPG','/media/ed/bb/3c/edbb3ca981069abcea17a41c5a224b95.JPG','/admin/auction/item.photo',NULL,'2016-12-07 11:03:09'),(49,1,'DSC_0483.JPG','08ce100b52c8fc1c6f27458831358595.JPG','/media/08/ce/10/08ce100b52c8fc1c6f27458831358595.JPG','/admin/auction/item.photo',NULL,'2016-12-07 11:04:08'),(50,1,'DSC_0438.JPG','6f83b608bbffaee0d9d6687ac104c4f6.JPG','/media/6f/83/b6/6f83b608bbffaee0d9d6687ac104c4f6.JPG','/admin/auction/item.photo',NULL,'2016-12-07 11:04:54'),(51,1,'DSC_0021.JPG','37084ce32671f97650d5ec34c0756366.JPG','/media/37/08/4c/37084ce32671f97650d5ec34c0756366.JPG','/admin/auction/item.photo',NULL,'2016-12-07 11:06:23'),(52,1,'DSC_0007.JPG','55dce7ff9ca488ca2375887f6b4791c0.JPG','/media/55/dc/e7/55dce7ff9ca488ca2375887f6b4791c0.JPG','/admin/auction/item.photo',NULL,'2016-12-08 01:01:11'),(53,1,'DSC_0009.JPG','f0181060a22f6514d4e2cff3599964b2.JPG','/media/f0/18/10/f0181060a22f6514d4e2cff3599964b2.JPG','/admin/auction/item.photo',NULL,'2016-12-08 01:02:17'),(54,1,'DSC_0021.JPG','68a0e6b1b4ef41cb6baf03db481007e2.JPG','/media/68/a0/e6/68a0e6b1b4ef41cb6baf03db481007e2.JPG','/admin/auction/item.photo',NULL,'2016-12-08 01:03:05'),(55,1,'DSC_0403.JPG','fb881ec01a784613cf9d75eaaca661ad.JPG','/media/fb/88/1e/fb881ec01a784613cf9d75eaaca661ad.JPG','/admin/auction/item.photo',NULL,'2016-12-08 01:03:59'),(56,1,'DSC_0402.JPG','552244e8b8545ae4d193c798961e83d0.JPG','/media/55/22/44/552244e8b8545ae4d193c798961e83d0.JPG','/admin/auction/item.photo',NULL,'2016-12-08 01:04:46'),(57,1,'DSC_0438.JPG','3f9c50ab746fe60d16a3ae366d3b135b.JPG','/media/3f/9c/50/3f9c50ab746fe60d16a3ae366d3b135b.JPG','/admin/auction/item.photo',NULL,'2016-12-08 01:05:30'),(58,1,'GHM - Kohaku 50 cm - Import - male - Handing Gading Koi Centre.JPG','b554ef5ad703aad36c7b04cc36edbcfc.JPG','/media/b5/54/ef/b554ef5ad703aad36c7b04cc36edbcfc.JPG','/admin/auction/item.photo',NULL,'2016-12-08 01:07:01'),(59,1,'George Gani - TAncho - 65 cm - Tangerang - Import - Female - Handling Republik Gading Koi.JPG','402e81b2b9447a97364c7ba62bd6be82.JPG','/media/40/2e/81/402e81b2b9447a97364c7ba62bd6be82.JPG','/admin/auction/item.photo',NULL,'2016-12-08 01:08:33'),(60,8,'GadingKoi final png.png','7b0f490dc1b2cfa8bac6e1619aa41fc7.png','/media/7b/0f/49/7b0f490dc1b2cfa8bac6e1619aa41fc7.png','mydata.avatar',8,'2016-12-09 09:18:02'),(61,1,'17-inch-koi-1.jpg','aaccb7433f3506bbe97171fc9a1b47a6.jpg','/media/aa/cc/b7/aaccb7433f3506bbe97171fc9a1b47a6.jpg','/admin/auction/item.photo',NULL,'2016-12-09 09:35:43'),(62,19,'cc.jpg','47bbeb12da3df3177589e3814ac010b2.jpg','/media/47/bb/eb/47bbeb12da3df3177589e3814ac010b2.jpg','mydata.avatar',19,'2016-12-09 09:42:53'),(63,1,'Taniguchi.jpg','1383e34e2cf17648c94b7a5072e8a079.jpg','/media/13/83/e3/1383e34e2cf17648c94b7a5072e8a079.jpg','/admin/supplier.media',NULL,'2016-12-09 09:43:09'),(64,1,'Shinsuke.jpg','dff77f8e4867a0b31ed90df725122582.jpg','/media/df/f7/7f/dff77f8e4867a0b31ed90df725122582.jpg','/admin/supplier.media',NULL,'2016-12-09 09:44:11'),(65,1,'ShinMaywa.jpg','9237107b8329fcf24ed1f05858eb3af4.jpg','/media/92/37/10/9237107b8329fcf24ed1f05858eb3af4.jpg','/admin/banner.media',NULL,'2016-12-09 09:46:03'),(66,1,'DSC_0005.JPG','0752cb43c1c64b3f9bdeee557b1f06d9.JPG','/media/07/52/cb/0752cb43c1c64b3f9bdeee557b1f06d9.JPG','/admin/auction/item.photo',NULL,'2016-12-13 08:13:56'),(67,1,'DSC_0009.JPG','214c84d4e677402bdae80801df29d300.JPG','/media/21/4c/84/214c84d4e677402bdae80801df29d300.JPG','/admin/auction/item.photo',NULL,'2016-12-13 08:14:46'),(68,1,'DSC_0021.JPG','8f4a92c7bc544da80716c8fc0a1f55e0.JPG','/media/8f/4a/92/8f4a92c7bc544da80716c8fc0a1f55e0.JPG','/admin/auction/item.photo',NULL,'2016-12-13 08:16:07'),(69,1,'DSC_0402.JPG','67518b78636bd29ab64327e8b0c975da.JPG','/media/67/51/8b/67518b78636bd29ab64327e8b0c975da.JPG','/admin/auction/item.photo',NULL,'2016-12-13 08:16:49'),(70,1,'DSC_0434.JPG','98f3f6e700ee1279178070526f714f94.JPG','/media/98/f3/f6/98f3f6e700ee1279178070526f714f94.JPG','/admin/auction/item.photo',NULL,'2016-12-13 08:17:43'),(71,1,'DSC_0469.JPG','d2be9caa741769858fd21c61780820e8.JPG','/media/d2/be/9c/d2be9caa741769858fd21c61780820e8.JPG','/admin/auction/item.photo',NULL,'2016-12-13 08:18:38'),(72,1,'DSC_0438.JPG','0117221a6e553217bcb70bb7bdd738af.JPG','/media/01/17/22/0117221a6e553217bcb70bb7bdd738af.JPG','/admin/auction/item.photo',NULL,'2016-12-13 08:19:36'),(73,1,'DSC_0436.JPG','2813cc1d75a260b932f0a9b9cc12fde6.JPG','/media/28/13/cc/2813cc1d75a260b932f0a9b9cc12fde6.JPG','/admin/auction/item.photo',NULL,'2016-12-13 08:20:20'),(74,1,'isakoi2.jpg','de3cac24d9902b1c588b9b9fa3284163.jpg','/media/de/3c/ac/de3cac24d9902b1c588b9b9fa3284163.jpg','/admin/supplier.media',13,'2016-12-13 11:20:37'),(75,1,'marujyu2.jpg','484f00e38f5147cd71dea7e423617d6e.jpg','/media/48/4f/00/484f00e38f5147cd71dea7e423617d6e.jpg','/admin/supplier.media',9,'2016-12-13 11:21:32'),(76,1,'Selection_002.png','66f13b1eb70d959311932b4e3f51df49.png','/media/66/f1/3b/66f13b1eb70d959311932b4e3f51df49.png','/admin/auction/item.certificate',NULL,'2016-12-15 02:51:13'),(77,1,'Ikan-Koi-Showa.jpg','23f67dc1549e0b24c554275503463b7b.jpg','/media/23/f6/7d/23f67dc1549e0b24c554275503463b7b.jpg','/admin/auction/item.photo',NULL,'2016-12-15 02:51:24'),(78,1,'beppu2.jpg','0b34d8bb9fd843b137b88efffc61b8f4.jpg','/media/0b/34/d8/0b34d8bb9fd843b137b88efffc61b8f4.jpg','/admin/supplier.media',9,'2016-12-15 10:05:45'),(79,1,'dainichi2.jpg','df063ce6a92d8a3095ad24596acdf869.jpg','/media/df/06/3c/df063ce6a92d8a3095ad24596acdf869.jpg','/admin/supplier.media',7,'2016-12-15 10:05:59'),(80,1,'isakoi2.jpg','ebba5ea4d7b6c3961056bde6776ce3ee.jpg','/media/eb/ba/5e/ebba5ea4d7b6c3961056bde6776ce3ee.jpg','/admin/supplier.media',11,'2016-12-15 10:06:12'),(81,1,'marujyu2.jpg','da986e44f0773d1e8b45d5098e7e9b2f.jpg','/media/da/98/6e/da986e44f0773d1e8b45d5098e7e9b2f.jpg','/admin/supplier.media',12,'2016-12-15 10:06:20'),(82,1,'sakai-ff2.jpg','9c3f80c010890f4f2d5b1ae975cba8b3.jpg','/media/9c/3f/80/9c3f80c010890f4f2d5b1ae975cba8b3.jpg','/admin/supplier.media',6,'2016-12-15 10:06:29'),(83,1,'samuraikoi2.jpg','e4c30223a5e1685950ef4427de10d4b3.jpg','/media/e4/c3/02/e4c30223a5e1685950ef4427de10d4b3.jpg','/admin/supplier.media',10,'2016-12-15 10:06:42'),(84,1,'shinoda2.jpg','4756b2482c1b52e6ae393e0c4c6efd6f.jpg','/media/47/56/b2/4756b2482c1b52e6ae393e0c4c6efd6f.jpg','/admin/supplier.media',8,'2016-12-15 10:06:53'),(85,1,'taniguchi2.jpg','b4bd7cdafdbd4e76e519b8d36d6027d6.jpg','/media/b4/bd/7c/b4bd7cdafdbd4e76e519b8d36d6027d6.jpg','/admin/supplier.media',13,'2016-12-15 10:07:12'),(86,1,'bossco.jpg','44c1d1f534457f0d3c11c8efb4a50060.jpg','/media/44/c1/d1/44c1d1f534457f0d3c11c8efb4a50060.jpg','/admin/banner.media',5,'2016-12-15 10:09:04'),(87,1,'flexy-coat.jpg','a5b2af0343ac207f22cae1502d6060d5.jpg','/media/a5/b2/af/a5b2af0343ac207f22cae1502d6060d5.jpg','/admin/banner.media',4,'2016-12-15 10:09:16'),(88,1,'jg-koifarm.jpg','79fdc33246b8db5b3512b3c2d83ae735.jpg','/media/79/fd/c3/79fdc33246b8db5b3512b3c2d83ae735.jpg','/admin/banner.media',6,'2016-12-15 10:09:25'),(89,1,'shinsuke.jpg','61e69154050851c0ca5dfc364db0ca55.jpg','/media/61/e6/91/61e69154050851c0ca5dfc364db0ca55.jpg','/admin/banner.media',3,'2016-12-15 10:10:00'),(90,1,'MP-VER.jpg','79bbab36532aea7a5395978930a261b8.jpg','/media/79/bb/ab/79bbab36532aea7a5395978930a261b8.jpg','/admin/auction/item.photo',NULL,'2016-12-22 06:56:36'),(91,1,'oke.png','abd64620fab74950784fa8a358725721.png','/media/ab/d6/46/abd64620fab74950784fa8a358725721.png','/admin/auction/item.photo',NULL,'2016-12-22 07:08:08'),(92,1,'hand-1006417_960_720.png','6705c227d41c85194b54b3030597b0c2.png','/media/67/05/c2/6705c227d41c85194b54b3030597b0c2.png','/admin/auction/item.photo',NULL,'2016-12-22 07:13:57'),(93,1,'Selection_721.png','9c43c6532138d68aea6ba24392a91657.png','/media/9c/43/c6/9c43c6532138d68aea6ba24392a91657.png','/admin/auction/item.certificate',NULL,'2016-12-22 09:03:41'),(94,1,'17-inch-koi-1.jpg','c45cdeea35c4a4e42856d79a3222f6e3.jpg','/media/c4/5c/de/c45cdeea35c4a4e42856d79a3222f6e3.jpg','/admin/auction/item.photo',NULL,'2016-12-22 09:03:46'),(95,1,'226b37f315e18e71c35282c4b76c6c1f.jpg','643a15ce7051042aba8b95efce20dab2.jpg','/media/64/3a/15/643a15ce7051042aba8b95efce20dab2.jpg','/admin/auction/item.photo',NULL,'2016-12-22 09:07:56'),(96,1,'jenis-koi-kohaku.jpg','392fb3e7e64a0e65dfaa2623763a9915.jpg','/media/39/2f/b3/392fb3e7e64a0e65dfaa2623763a9915.jpg','/admin/auction/item.photo',NULL,'2016-12-23 02:33:31'),(97,1,'o0923r006.jpg','9a49ec18d907687f388bd83284794ee5.jpg','/media/9a/49/ec/9a49ec18d907687f388bd83284794ee5.jpg','/admin/auction/item.photo',NULL,'2016-12-23 02:52:01'),(98,1,'17-inch-koi-1.jpg','c1bca8e640b45636cab5a82d16b95736.jpg','/media/c1/bc/a8/c1bca8e640b45636cab5a82d16b95736.jpg','/admin/auction/item.photo',NULL,'2016-12-23 02:55:31'),(99,1,'Ikan-Koi-Showa.jpg','136c0d48b34f8ca56e91ff8693fd9c7b.jpg','/media/13/6c/0d/136c0d48b34f8ca56e91ff8693fd9c7b.jpg','/admin/auction/item.photo',NULL,'2016-12-23 02:59:49'),(100,1,'17-inch-koi-1.jpg','d25097e91198455eabe0347387614cc6.jpg','/media/d2/50/97/d25097e91198455eabe0347387614cc6.jpg','/admin/auction/item.photo',NULL,'2016-12-23 03:00:43'),(101,1,'Ikan-Koi-Showa.jpg','2688b68d29ab748ccc6ebc870e02ece6.jpg','/media/26/88/b6/2688b68d29ab748ccc6ebc870e02ece6.jpg','/admin/auction/item.photo',NULL,'2016-12-23 03:02:20'),(102,1,'226b37f315e18e71c35282c4b76c6c1f.jpg','3919fd5963beaa159f54e642bbadff06.jpg','/media/39/19/fd/3919fd5963beaa159f54e642bbadff06.jpg','/admin/auction/item.photo',NULL,'2016-12-23 03:03:20'),(103,1,'flowchart.jpg','152fce2541082afb4be1d88374b37c60.jpg','/media/15/2f/ce/152fce2541082afb4be1d88374b37c60.jpg','/admin/auction/item.photo',NULL,'2016-12-23 03:07:52'),(104,1,'o0923r006.jpg','2566fbc8fda57716d83aed1effc5b74a.jpg','/media/25/66/fb/2566fbc8fda57716d83aed1effc5b74a.jpg','/admin/auction/item.photo',NULL,'2016-12-23 03:08:30'),(105,1,'jenis-koi-kohaku.jpg','b339c7fa3909aec3e30611ca7aac1014.jpg','/media/b3/39/c7/b339c7fa3909aec3e30611ca7aac1014.jpg','/admin/auction/item.photo',NULL,'2016-12-23 03:10:26'),(106,1,'Ikan-Koi-Showa.jpg','afec888e06437f1fe0f435b046aa46d8.jpg','/media/af/ec/88/afec888e06437f1fe0f435b046aa46d8.jpg','/admin/auction/item.photo',NULL,'2016-12-23 03:11:40'),(107,1,'jenis-koi-kohaku.jpg','f5a1c41b0a94696370a787ff4f935569.jpg','/media/f5/a1/c4/f5a1c41b0a94696370a787ff4f935569.jpg','/admin/auction/item.photo',NULL,'2016-12-23 03:12:26'),(108,1,'jenis-koi-kohaku.jpg','6bd704d1e994ccd7e12d17c466b3e3cb.jpg','/media/6b/d7/04/6bd704d1e994ccd7e12d17c466b3e3cb.jpg','/admin/auction/item.photo',NULL,'2016-12-23 03:12:37'),(109,1,'Ikan-Koi-Showa.jpg','5b0841a5bc8d7767ae8c4a417e3ac69e.jpg','/media/5b/08/41/5b0841a5bc8d7767ae8c4a417e3ac69e.jpg','/admin/auction/item.photo',NULL,'2016-12-23 03:18:56'),(110,1,'jenis-koi-kohaku.jpg','953636d4dee4966dc7fbdba35e4c9a1b.jpg','/media/95/36/36/953636d4dee4966dc7fbdba35e4c9a1b.jpg','/admin/auction/item.photo',NULL,'2016-12-23 03:24:56'),(111,1,'flowchart.jpg','e0838bc06fd94889b66f8afc99dbdad8.jpg','/media/e0/83/8b/e0838bc06fd94889b66f8afc99dbdad8.jpg','/admin/auction/item.photo',NULL,'2016-12-23 07:00:32'),(112,1,'jenis-koi-kohaku.jpg','3e869c5604b861e5b4d4152275d76751.jpg','/media/3e/86/9c/3e869c5604b861e5b4d4152275d76751.jpg','/admin/auction/item.photo',NULL,'2016-12-23 07:00:37'),(113,1,'17-inch-koi-1.jpg','faff0565bb47297e49b3606bb53ce861.jpg','/media/fa/ff/05/faff0565bb47297e49b3606bb53ce861.jpg','/admin/auction/item.photo',48,'2016-12-23 07:00:46'),(114,1,'226b37f315e18e71c35282c4b76c6c1f.jpg','9c65d18b3bdfd0b1ef6ad3dac8bf8ab5.jpg','/media/9c/65/d1/9c65d18b3bdfd0b1ef6ad3dac8bf8ab5.jpg','/admin/auction/item.photo',NULL,'2016-12-23 07:24:37');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,'rerocker7@gmail.com'),(2,'imamjinani@gmail.com');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,'About','about','<p>ffwwqfqw</p>','','','','','2016-12-06 11:14:04'),(2,'Contact','contact','<p>Mr. Glenardo Yopie</p>\r\n<p>Jl. Janur ELok Raya QH 3 No 8</p>\r\n<p>Kelapa Gading 14240</p>\r\n<p>HP&amp;WA +62816900003</p>\r\n<p>Email : glenardojopie@gmail.com</p>','','','','','2016-12-06 11:14:13'),(3,'Rules','rules','','','','','','2016-12-06 11:14:27');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `perms`
--

LOCK TABLES `perms` WRITE;
/*!40000 ALTER TABLE `perms` DISABLE KEYS */;
INSERT INTO `perms` VALUES (1,'Banner','create-banner','Create Banner','Allow user to create new banner','2016-11-15 03:46:14'),(2,'Banner','delete-banner','Delete Banner','Allow user to delete exists banner','2016-11-15 03:46:14'),(3,'Banner','read-banner','Read Banner','Allow user to see all exists banner','2016-11-15 03:46:14'),(4,'Banner','update-banner','Edit Banner','Allow user to update exists banner','2016-11-15 03:46:14'),(5,'Event','create-event','Create Event','Allow user to create new event','2016-11-15 03:46:14'),(6,'Event','delete-event','Delete Event','Allow user to delete exists event','2016-11-15 03:46:14'),(7,'Event','read-event','Read Event','Allow user to see all exists event','2016-11-15 03:46:14'),(8,'Event','update-event','Edit Event','Allow user to update exists event','2016-11-15 03:46:14'),(9,'Front Page','read-admin_page','Open Admin Page','Allow user to open admin page','2016-11-15 03:46:14'),(10,'Gallery','create-gallery','Create Gallery','Allow user to create new album gallery','2016-11-15 03:46:14'),(11,'Gallery','delete-gallery','Delete Gallery','Allow user to delete exists album gallery','2016-11-15 03:46:14'),(12,'Gallery','read-gallery','Read Gallery','Allow user to see all exists album gallery','2016-11-15 03:46:14'),(13,'Gallery','update-gallery','Edit Gallery','Allow user to update exists album gallery','2016-11-15 03:46:14'),(14,'Gallery Media','create-gallery_media','Create Gallery Media','Allow user to create new album gallery media','2016-11-15 03:46:14'),(15,'Gallery Media','delete-gallery_media','Delete Gallery Media','Allow user to delete exists album gallery media','2016-11-15 03:46:14'),(16,'Gallery Media','read-gallery_media','Read Gallery Media','Allow user to see all exists album gallery media','2016-11-15 03:46:14'),(17,'Gallery Media','update-gallery_media','Edit Gallery Media','Allow user to update exists album gallery media','2016-11-15 03:46:14'),(18,'Page','create-page','Create Page','Allow user to create new page','2016-11-15 03:46:14'),(19,'Page','delete-page','Delete Page','Allow user to delete exists page','2016-11-15 03:46:14'),(20,'Page','read-page','Read Page','Allow user to see all exists page','2016-11-15 03:46:14'),(21,'Page','update-page','Edit Page','Allow user to update exists page','2016-11-15 03:46:14'),(22,'Post','create-post','Create Post','Allow user to create new post','2016-11-15 03:46:14'),(23,'Post','delete-post','Delete Post','Allow user to delete exists post','2016-11-15 03:46:14'),(24,'Post','read-post','Read Post','Allow user to see all exists post','2016-11-15 03:46:14'),(25,'Post','update-post','Edit Post','Allow user to update exists post','2016-11-15 03:46:14'),(26,'Post Other','create-post_featured','Set Post As Featured','Allow user to set post as featured','2016-11-15 03:46:14'),(27,'Post Other','create-post_editor_pick','Set Post As Editor Pick','Allow user to set post as editor pick','2016-11-15 03:46:14'),(28,'Post Other','delete-post_other_user','Delete Other Reporter Post','Allow user to remove other reporter posts','2016-11-15 03:46:14'),(29,'Post Other','update-post_other_user','Update Other Reporter Post','Allow user to update other reporter posts','2016-11-15 03:46:14'),(30,'Post Other','read-post_other_user','Read Other Reporter Post','Allow user to see other reporter posts','2016-11-15 03:46:14'),(31,'Post Other','create-post_published','Publish Post','Allow user to publish the post','2016-11-15 03:46:14'),(32,'Post Other','update-post_slug','Edit Post Slug','Allow user to update the post slug','2016-11-15 03:46:14'),(33,'Post Other','delete-post_instant','Clear Post Instant Content','Allow user to clear post instant content','2016-11-15 03:46:14'),(34,'Post Category','create-post_category','Create Post Category','Allow user to create new post category','2016-11-15 03:46:14'),(35,'Post Category','delete-post_category','Delete Post Category','Allow user to delete exists post category','2016-11-15 03:46:14'),(36,'Post Category','read-post_category','Read Post Category','Allow user to see all exists post category','2016-11-15 03:46:14'),(37,'Post Category','update-post_category','Edit Post Category','Allow user to update exists post category','2016-11-15 03:46:14'),(38,'Post Tag','create-post_tag','Create Post Tag','Allow user to create new post category','2016-11-15 03:46:14'),(39,'Post Tag','delete-post_tag','Delete Post Tag','Allow user to delete exists post category','2016-11-15 03:46:14'),(40,'Post Tag','read-post_tag','Read Post Tag','Allow user to see all exists post category','2016-11-15 03:46:14'),(41,'Post Tag','update-post_tag','Edit Post Tag','Allow user to update exists post category','2016-11-15 03:46:14'),(42,'Post Selector','create-post_selector','Create Post Selection','Allow user to create new post selection','2016-11-15 03:46:14'),(43,'Post Selector','delete-post_selector','Delete Post Selection','Allow user to delete exists post selection','2016-11-15 03:46:14'),(44,'Post Selector','read-post_selector','Read Post Selection','Allow user to see all exists post selection','2016-11-15 03:46:14'),(45,'Post Selector','update-post_selector','Edit Post Selection','Allow user to update exists post selection','2016-11-15 03:46:14'),(46,'Post Suggestion','create-post_suggestion','Create Post Suggestion','Allow user to create new post suggestion','2016-11-15 03:46:14'),(47,'Post Suggestion','delete-post_suggestion','Delete Post Suggestion','Allow user to delete exists post suggestion','2016-11-15 03:46:14'),(48,'Post Suggestion','read-post_suggestion','Read Post Suggestion','Allow user to see all exists post suggestion','2016-11-15 03:46:14'),(49,'Post Suggestion','update-post_suggestion','Edit Post Suggestion','Allow user to update exists post suggestion','2016-11-15 03:46:14'),(50,'Site Enum','create-site_enum','Create Site Enum','Allow user to create new site enum','2016-11-15 03:46:14'),(51,'Site Enum','delete-site_enum','Delete Site Enum','Allow user to delete exists site enum','2016-11-15 03:46:14'),(52,'Site Enum','read-site_enum','Read Site Enum','Allow user to see all exists site enum','2016-11-15 03:46:14'),(53,'Site Enum','update-site_enum','Edit Site Enum','Allow user to update exists site enum','2016-11-15 03:46:14'),(54,'Site Menu','create-site_menu','Create Site Menu','Allow user to create new site menu','2016-11-15 03:46:14'),(55,'Site Menu','delete-site_menu','Delete Site Menu','Allow user to delete exists site menu','2016-11-15 03:46:14'),(56,'Site Menu','read-site_menu','Read Site Menu','Allow user to see all exists site menu','2016-11-15 03:46:14'),(57,'Site Menu','update-site_menu','Edit Site Menu','Allow user to update exists site menu','2016-11-15 03:46:14'),(58,'Site Parameters','create-site_param','Create Site Parameter','Allow user to create new site parameter','2016-11-15 03:46:14'),(59,'Site Parameters','delete-site_param','Delete Site Parameter','Allow user to delete exists site parameter','2016-11-15 03:46:14'),(60,'Site Parameters','read-site_param','Read Site Parameter','Allow user to see all exists site parameter','2016-11-15 03:46:14'),(61,'Site Parameters','update-site_param','Edit Site Parameter','Allow user to update exists site parameter','2016-11-15 03:46:14'),(62,'Site Statistic','read-site_ranks','Read Site Ranks','Allow user to see all site ranks','2016-11-15 03:46:14'),(63,'Site Statistic','read-site_realtime','Read Realtime Statistic','Allow user to see all site realtime statistic','2016-11-15 03:46:14'),(64,'Site Statistic','read-visitor_statistic','Read Visitor Statistic','Allow user to see all site visitor statistic','2016-11-15 03:46:14'),(65,'Site Cache','delete-cache','Remove All Site Cache','Allow user to remove all site cache','2016-11-15 03:46:14'),(66,'Site Media','delete-media_sizes','Remove All Media Resizes','Allow user to remove all resized media','2016-11-15 03:46:14'),(67,'Slideshow','create-slide_show','Create Slideshow','Allow user to create new slideshow','2016-11-15 03:46:14'),(68,'Slideshow','delete-slide_show','Delete Slideshow','Allow user to delete exists slideshow','2016-11-15 03:46:14'),(69,'Slideshow','read-slide_show','Read Slideshow','Allow user to see all exists slideshow','2016-11-15 03:46:14'),(70,'Slideshow','update-slide_show','Edit Slideshow','Allow user to update exists slideshow','2016-11-15 03:46:14'),(71,'URL Redirection','create-url_redirection','Create URL Redirection','Allow user to create new url redirection','2016-11-15 03:46:14'),(72,'URL Redirection','delete-url_redirection','Delete URL Redirection','Allow user to delete exists url redirection','2016-11-15 03:46:14'),(73,'URL Redirection','read-url_redirection','Read URL Redirection','Allow user to see all exists url redirection','2016-11-15 03:46:14'),(74,'URL Redirection','update-url_redirection','Edit URL Redirection','Allow user to update exists url redirection','2016-11-15 03:46:14'),(75,'User Management','create-user','Create User','Allow user to create new user','2016-11-15 03:46:14'),(76,'User Management','delete-user','Delete User','Allow user to delete exists user','2016-11-15 03:46:14'),(77,'User Management','read-user','Read User','Allow user to see all exists user','2016-11-15 03:46:14'),(78,'User Management','update-user','Edit User','Allow user to update exists user','2016-11-15 03:46:14'),(79,'User Management','update-user_password','Edit User Password','Allow user to update exists user password','2016-11-15 03:46:14'),(80,'User Management','update-user_permission','Edit User Permissions','Allow user to update exists user permissions','2016-11-15 03:46:14'),(81,'User Management','update-user_session','Login As Other User','Allow user to login as other user','2016-11-15 03:46:14'),(82,'Auction','create-auction','Create Auction','Allow user to create new auction','2016-11-15 03:46:14'),(83,'Auction','delete-auction','Delete Auction','Allow user to delete exists auction','2016-11-15 03:46:14'),(84,'Auction','read-auction','Read Auction','Allow user to see all exists auction','2016-11-15 03:46:14'),(85,'Auction','update-auction','Edit Auction','Allow user to update exists auction','2016-11-15 03:46:14'),(86,'Auction Item','create-auction_item','Create Auction Item','Allow user to create new auction item','2016-11-15 03:46:14'),(87,'Auction Item','delete-auction_item','Delete Auction Item','Allow user to delete exists auction item','2016-11-15 03:46:14'),(88,'Auction Item','read-auction_item','Read Auction Item','Allow user to see all exists auction item','2016-11-15 03:46:14'),(89,'Auction Item','update-auction_item','Edit Auction Item','Allow user to update exists auction item','2016-11-15 03:46:14'),(90,'Item Variety','create-item_variety','Create Item Variety','Allow user to create new item variety','2016-11-15 03:46:14'),(91,'Item Variety','delete-item_variety','Delete Item Variety','Allow user to delete exists item variety','2016-11-15 03:46:14'),(92,'Item Variety','read-item_variety','Read Item Variety','Allow user to see all exists item variety','2016-11-15 03:46:14'),(93,'Item Variety','update-item_variety','Edit Item Variety','Allow user to update exists item variety','2016-11-15 03:46:14'),(94,'Item','read-item','Read Item','Allow user to see all exists item','2016-11-15 03:46:14'),(95,'Item Breeder','create-item_breeder','Create Item Breeder','Allow user to create new item breeder','2016-11-15 03:46:14'),(96,'Item Breeder','delete-item_breeder','Delete Item Breeder','Allow user to delete exists item breeder','2016-11-15 03:46:14'),(97,'Item Breeder','read-item_breeder','Read Item Breeder','Allow user to see all exists item breeder','2016-11-15 03:46:14'),(98,'Item Breeder','update-item_breeder','Edit Item Breeder','Allow user to update exists item breeder','2016-11-15 03:46:14'),(99,'Auction Item Other','delete-auction_item_other_user','Delete Other Auction Item','Allow user to remove other auction item','2016-11-15 03:46:14'),(100,'Supplier','create-supplier','Create Supplier','Allow user to create new supplier','2016-11-15 03:46:14'),(101,'Supplier','delete-supplier','Delete Supplier','Allow user to delete exists supplier','2016-11-15 03:46:14'),(102,'Supplier','read-supplier','Read Supplier','Allow user to see all exists supplier','2016-11-15 03:46:14'),(103,'Supplier','update-supplier','Edit Supplier','Allow user to update exists supplier','2016-11-15 03:46:14'),(104,'Item History','read-item_history','Read Item History','Allow user to see all exists item history','2016-11-15 03:46:14');
/*!40000 ALTER TABLE `perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `post_category`
--

LOCK TABLES `post_category` WRITE;
/*!40000 ALTER TABLE `post_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `post_category_chain`
--

LOCK TABLES `post_category_chain` WRITE;
/*!40000 ALTER TABLE `post_category_chain` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_category_chain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `post_schedule`
--

LOCK TABLES `post_schedule` WRITE;
/*!40000 ALTER TABLE `post_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `post_selection`
--

LOCK TABLES `post_selection` WRITE;
/*!40000 ALTER TABLE `post_selection` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_selection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `post_statistic`
--

LOCK TABLES `post_statistic` WRITE;
/*!40000 ALTER TABLE `post_statistic` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_statistic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `post_suggestion`
--

LOCK TABLES `post_suggestion` WRITE;
/*!40000 ALTER TABLE `post_suggestion` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_suggestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `post_tag`
--

LOCK TABLES `post_tag` WRITE;
/*!40000 ALTER TABLE `post_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `post_tag_chain`
--

LOCK TABLES `post_tag_chain` WRITE;
/*!40000 ALTER TABLE `post_tag_chain` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_tag_chain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `post_trending`
--

LOCK TABLES `post_trending` WRITE;
/*!40000 ALTER TABLE `post_trending` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_trending` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `site_enum`
--

LOCK TABLES `site_enum` WRITE;
/*!40000 ALTER TABLE `site_enum` DISABLE KEYS */;
INSERT INTO `site_enum` VALUES (1,'event.seo_schema','Event','Event','2016-11-15 03:46:15'),(2,'event.seo_schema','BusinessEvent','BusinessEvent','2016-11-15 03:46:15'),(3,'event.seo_schema','ChildrensEvent','ChildrensEvent','2016-11-15 03:46:15'),(4,'event.seo_schema','ComedyEvent','ComedyEvent','2016-11-15 03:46:15'),(5,'event.seo_schema','DanceEvent','DanceEvent','2016-11-15 03:46:15'),(6,'event.seo_schema','DeliveryEvent','DeliveryEvent','2016-11-15 03:46:15'),(7,'event.seo_schema','EducationEvent','EducationEvent','2016-11-15 03:46:15'),(8,'event.seo_schema','ExhibitionEvent','ExhibitionEvent','2016-11-15 03:46:15'),(9,'event.seo_schema','Festival','Festival','2016-11-15 03:46:15'),(10,'event.seo_schema','FoodEvent','FoodEvent','2016-11-15 03:46:15'),(11,'event.seo_schema','LiteraryEvent','LiteraryEvent','2016-11-15 03:46:15'),(12,'event.seo_schema','MusicEvent','MusicEvent','2016-11-15 03:46:15'),(13,'event.seo_schema','PublicationEvent','PublicationEvent','2016-11-15 03:46:15'),(14,'event.seo_schema','SaleEvent','SaleEvent','2016-11-15 03:46:15'),(15,'event.seo_schema','ScreeningEvent','ScreeningEvent','2016-11-15 03:46:15'),(16,'event.seo_schema','SocialEvent','SocialEvent','2016-11-15 03:46:15'),(17,'event.seo_schema','SportsEvent','SportsEvent','2016-11-15 03:46:15'),(18,'event.seo_schema','TheaterEvent','TheaterEvent','2016-11-15 03:46:15'),(19,'event.seo_schema','VisualArtsEvent','VisualArtsEvent','2016-11-15 03:46:15'),(20,'post.status','1','Draft','2016-11-15 03:46:15'),(21,'post.status','2','Editor','2016-11-15 03:46:15'),(22,'post.status','3','Schedule','2016-11-15 03:46:15'),(23,'post.status','4','Published','2016-11-15 03:46:15'),(24,'post.seo_schema','CreativeWork','CreativeWork','2016-11-15 03:46:15'),(25,'post.seo_schema','Article','Article','2016-11-15 03:46:15'),(26,'post.seo_schema','NewsArticle','NewsArticle','2016-11-15 03:46:15'),(27,'post.seo_schema','BlogPosting','BlogPosting','2016-11-15 03:46:15'),(28,'post.seo_schema','TechArticle','TechArticle','2016-11-15 03:46:15'),(29,'post.seo_schema','Report','Report','2016-11-15 03:46:15'),(30,'post.seo_schema','Review','Review','2016-11-15 03:46:15'),(31,'post_category.seo_schema','CollectionPage','CollectionPage','2016-11-15 03:46:15'),(32,'post_tag.seo_schema','CollectionPage','CollectionPage','2016-11-15 03:46:15'),(33,'page.seo_schema','WebPage','WebPage','2016-11-15 03:46:15'),(34,'page.seo_schema','AboutPage','AboutPage','2016-11-15 03:46:15'),(35,'page.seo_schema','CheckoutPage','CheckoutPage','2016-11-15 03:46:15'),(36,'page.seo_schema','ContactPage ','ContactPage ','2016-11-15 03:46:15'),(37,'page.seo_schema','QAPage','QAPage','2016-11-15 03:46:15'),(38,'page.seo_schema','SearchResultsPage','SearchResultsPage','2016-11-15 03:46:15'),(39,'user.status','0','Deleted','2016-11-15 03:46:15'),(40,'user.status','1','Banned','2016-11-15 03:46:15'),(41,'user.status','2','Unverified','2016-11-15 03:46:15'),(42,'user.status','3','Verified','2016-11-15 03:46:15'),(43,'user.status','4','Official','2016-11-15 03:46:15'),(44,'auction.status','2','Draft','2016-11-15 04:18:36'),(45,'auction.status','4','Published','2016-11-15 04:18:51'),(46,'item.gender','Male','Male','2016-11-15 06:27:24'),(47,'item.gender','Female','Female','2016-11-15 06:27:56');
/*!40000 ALTER TABLE `site_enum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `site_menu`
--

LOCK TABLES `site_menu` WRITE;
/*!40000 ALTER TABLE `site_menu` DISABLE KEYS */;
INSERT INTO `site_menu` VALUES (1,'header-menu','Home','http://gadingkoiauction.com/',0,1,'2016-11-28 03:23:26'),(2,'header-menu','About','http://gadingkoiauction.com/page/about',0,1,'2016-11-28 03:23:39'),(3,'header-menu','Contact','http://gadingkoiauction.com/page/contact',0,1,'2016-11-28 03:23:50'),(4,'header-menu','Rules','http://gadingkoiauction.com/page/rules',0,1,'2016-11-28 03:23:57');
/*!40000 ALTER TABLE `site_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `site_params`
--

LOCK TABLES `site_params` WRITE;
/*!40000 ALTER TABLE `site_params` DISABLE KEYS */;
INSERT INTO `site_params` VALUES (1,'code_application_facebook','','2016-11-15 03:46:14'),(2,'code_google_analytics','','2016-11-15 03:46:14'),(3,'code_google_analytics_view','','2016-11-15 03:46:14'),(4,'google_analytics_content_group','1','2016-11-15 03:46:14'),(5,'code_google_map','','2016-11-15 03:46:14'),(6,'code_verification_alexa','','2016-11-15 03:46:14'),(7,'code_verification_bing','','2016-11-15 03:46:14'),(8,'code_verification_pinterest','','2016-11-15 03:46:14'),(9,'code_verification_google','','2016-11-15 03:46:14'),(10,'code_verification_yandex','','2016-11-15 03:46:14'),(11,'code_facebook_page_id','','2016-11-15 03:46:14'),(12,'google_analytics_statistic','','2016-11-15 03:46:14'),(13,'media_host','','2016-11-15 03:46:14'),(14,'organization_email','','2016-11-15 03:46:14'),(15,'organization_contact_customer_support','','2016-11-15 03:46:14'),(16,'organization_contact_technical_support','','2016-11-15 03:46:14'),(17,'organization_contact_billing_support','','2016-11-15 03:46:14'),(18,'organization_contact_bill_payment','','2016-11-15 03:46:14'),(19,'organization_contact_sales','','2016-11-15 03:46:14'),(20,'organization_contact_reservations','','2016-11-15 03:46:14'),(21,'organization_contact_credit_card_support','','2016-11-15 03:46:14'),(22,'organization_contact_emergency','','2016-11-15 03:46:14'),(23,'organization_contact_baggage_tracking','','2016-11-15 03:46:14'),(24,'organization_contact_roadside_assistance','','2016-11-15 03:46:14'),(25,'organization_contact_package_tracking','','2016-11-15 03:46:14'),(26,'organization_contact_available_language','','2016-11-15 03:46:14'),(27,'organization_contact_area_served','','2016-11-15 03:46:14'),(28,'organization_contact_opt_tollfree','','2016-11-15 03:46:14'),(29,'organization_contact_opt_his','','2016-11-15 03:46:14'),(30,'site_frontpage_description','The Gading Koi Auction','2016-11-15 03:46:14'),(31,'site_frontpage_keywords','koi, auction, gading koi, koi auction, gading koi auction','2016-11-15 03:46:14'),(32,'site_frontpage_title','Gading Koi Auction','2016-11-15 03:46:14'),(33,'site_name','Gading Koi Auction','2016-11-15 03:46:14'),(34,'site_x_social_facebook','','2016-11-15 03:46:14'),(35,'site_x_social_gplus','','2016-11-15 03:46:14'),(36,'site_x_social_instagram','','2016-11-15 03:46:14'),(37,'site_x_social_linkedin','','2016-11-15 03:46:14'),(38,'site_x_social_myspace','','2016-11-15 03:46:14'),(39,'site_x_social_pinterest','','2016-11-15 03:46:14'),(40,'site_x_social_soundcloud','','2016-11-15 03:46:14'),(41,'site_x_social_tumblr','','2016-11-15 03:46:14'),(42,'site_x_social_twitter','','2016-11-15 03:46:14'),(43,'site_x_social_youtube','','2016-11-15 03:46:14'),(44,'site_theme','default','2016-11-15 03:46:14'),(45,'site_theme_responsive','1','2016-11-15 03:46:14'),(46,'sitemap_news','0','2016-11-15 03:46:14'),(47,'theme_host','','2016-11-15 03:46:14'),(48,'theme_include_fb_js_api','1','2016-11-15 03:46:14'),(49,'theme_include_fb_js_api_with_ads','0','2016-11-15 03:46:14'),(50,'amphtml_support_for_post','0','2016-11-15 03:46:14'),(51,'instant_article_support_for_post','0','2016-11-15 03:46:14');
/*!40000 ALTER TABLE `site_params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `site_ranks`
--

LOCK TABLES `site_ranks` WRITE;
/*!40000 ALTER TABLE `site_ranks` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_ranks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `slideshow`
--

LOCK TABLES `slideshow` WRITE;
/*!40000 ALTER TABLE `slideshow` DISABLE KEYS */;
/*!40000 ALTER TABLE `slideshow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES (6,1,'Sakai ','/media/9c/3f/80/9c3f80c010890f4f2d5b1ae975cba8b3.jpg','Sakai','','2017-12-31 00:00:00','2016-12-03 03:44:58'),(7,1,'Dainici','/media/df/06/3c/df063ce6a92d8a3095ad24596acdf869.jpg','Dainici','','2017-12-31 00:00:00','2016-12-03 03:45:42'),(8,1,'SHinoda','/media/47/56/b2/4756b2482c1b52e6ae393e0c4c6efd6f.jpg','Shined','','2017-12-31 00:00:00','2016-12-03 03:46:05'),(9,1,'Beppu','/media/0b/34/d8/0b34d8bb9fd843b137b88efffc61b8f4.jpg','Beeps','','2017-12-31 00:00:00','2016-12-03 03:46:44'),(10,1,'Samurai','/media/e4/c3/02/e4c30223a5e1685950ef4427de10d4b3.jpg','Samurai','','2017-12-31 00:00:00','2016-12-03 03:47:31'),(11,1,'Isa','/media/eb/ba/5e/ebba5ea4d7b6c3961056bde6776ce3ee.jpg','Isa','','2017-12-31 00:00:00','2016-12-03 03:47:51'),(12,1,'Marujyu','/media/da/98/6e/da986e44f0773d1e8b45d5098e7e9b2f.jpg','Marujyu','','2017-12-31 00:00:00','2016-12-03 03:52:50'),(13,1,'Taniguchi Koi Farm','/media/b4/bd/7c/b4bd7cdafdbd4e76e519b8d36d6027d6.jpg','Taniguci','','2017-12-31 00:00:00','2016-12-09 09:43:59');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `url_redirection`
--

LOCK TABLES `url_redirection` WRITE;
/*!40000 ALTER TABLE `url_redirection` DISABLE KEYS */;
/*!40000 ALTER TABLE `url_redirection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'root','System','$2y$10$dOK9M7TWX8oHlr24SK.lke1.oeYoxPGCHb2bCkHC9gt6ITu8nqoN.','root@system.admin','/media/c1/5e/60/c15e604660b245a9d39a7db04e4030d4.jpg',NULL,NULL,3,'2016-11-15 03:46:15'),(2,'imam','imam jinani','$2y$10$ybhT688hYTj0fwx5mzhnVOQiCP4G7MPdGzPE3l83wHMAxubv3t4ni','immjnn@gmail.com','http://gadingkoiauction.com/theme/default/static/img/elements/user.png',NULL,NULL,3,'2016-11-29 02:17:00'),(5,'tarjo','tarjono','$2y$10$gkKYuAc1MOXbR7CvPfJSeOlAEKHaGSpB21R4ekc.KAq8GLAGGRpgK','rerocker78@gmail.com','http://gadingkoiauction.com/theme/default/static/img/elements/user.png',NULL,NULL,2,'2016-11-29 02:50:53'),(19,'wawanwawan','wawanwawan','$2y$10$jTWZPwTu3PchwEUwC68g7e7JuoSnQQXXOOm7JyM3Vxsn1V.ows4ci','infountukartisss@gmail.com','/media/47/bb/eb/47bbeb12da3df3177589e3814ac010b2.jpg',NULL,NULL,2,'2016-12-09 09:40:27'),(20,'siti','siti','$2y$10$UiiP/Nlbt0XcTO5B41Rw.ufqsofBy5hpxrPnR8QpwDjFBs/a8Fhmu','siti@gmail.com','http://newkoi.dev/theme/default/static/img/elements/user.png',NULL,NULL,2,'2016-12-20 06:18:09'),(21,'rocker','rocker','$2y$10$ydAOysgIOFRL20zIXkcQBeV2MUJzIvGMuRswcymNx59.wkDqPnA8e','rerocker74@gmail.com','http://newkoi.dev/theme/default/static/img/elements/user.png',NULL,NULL,3,'2016-12-20 06:23:34'),(22,'martiah','martiah','$2y$10$Bh.6h5AO8SYsFiSRjlQF0eVOA.qMSInogsRdENgF72edeQW13xLd2','rerocker7@gmail.com',NULL,NULL,NULL,3,'2016-12-20 08:06:27'),(23,'wanto','wanto','$2y$10$93gDyGk8xcXR4Or/Pz32ceZWpPjtfAnHsEWdRaq7wYVMZoktD6T2m','infountukartis@gmail.com','http://newkoi.dev/theme/default/static/img/elements/user.png',NULL,NULL,2,'2016-12-20 08:17:01');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_perms`
--

LOCK TABLES `user_perms` WRITE;
/*!40000 ALTER TABLE `user_perms` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_perms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_session`
--

LOCK TABLES `user_session` WRITE;
/*!40000 ALTER TABLE `user_session` DISABLE KEYS */;
INSERT INTO `user_session` VALUES (1,1,'$2y$10$Kmmcq2PjMPXwRf82tYWTAO.EmduxAjBcLEDWQ05FVzDlWxUCAFS5i','2016-11-15 03:56:11'),(6,1,'$2y$10$fPuor2Xl9Bp6W6Xn8rQJ..RKhGs5ztXYgNOBB95IF3S1SgVXOPGCe','2016-11-22 10:15:08'),(7,1,'$2y$10$l5w2bIVWJkd16sO/noVFt.n4UxeB3jb8D/gYdpjlGMOhTPsV/n.N.','2016-11-23 03:54:02'),(8,1,'$2y$10$kLIUQhk45k4DkVowR9oU.OpSX5.VAiui0vA/q.dRKK.qP8jiYQkg.','2016-11-23 04:27:24'),(17,15,'$2y$10$z.Q7JeOd/PhlobK/icBzqOLyCJ4hB0PO9QxapTAjg/7RvoGFlF0Vi','2016-11-28 05:57:09'),(30,8,'$2y$10$qoWRxDHGrBPMAiYcOX3ekusSRhau8eS3.GUtWyA6GkajZCboEzTI.','2016-11-29 08:45:04'),(38,8,'$2y$10$q0GzPoCWgytrVSpmScMPUukLZA4kUD2obQNjeXWmmClp5oGUeKYFG','2016-12-02 08:36:38'),(39,1,'$2y$10$eJ35AX.xTDbE6saSLll2tum3rYZdKo.fNzwcBP8rXwunXay5kqB8u','2016-12-02 09:04:13'),(40,1,'$2y$10$5Tg9akhGMRDAReEA6huaIev6335dWTSLrS1UmCyjDZGPYOp5HKVii','2016-12-03 03:37:00'),(43,1,'$2y$10$Qm1DOog91ulaNEAF7Cn6lO4g.Kk6/PdBcEK/LyKAE2XiPUJcFUGue','2016-12-03 08:30:50'),(46,1,'$2y$10$5z/8rCh6e5KWonDTp7qKHePDjBjSrKd7BMpmpQsPipdraaw6pbAye','2016-12-06 09:22:55'),(47,1,'$2y$10$ufBWgIKQKsqcuX5u7t.sYeBUrGZL3yA2Oi6YGkBXvaEF/WyVkZEIC','2016-12-06 09:37:46'),(51,1,'$2y$10$i4NI5UPBkxrT5ZxixMGKYOidNi5Ag5Id1F3Eb2gm2PH5GbLSkdtmG','2016-12-06 12:03:01'),(52,14,'$2y$10$.hZG7Jpqxeb0M.Zf01hJ.O5nAAgnVS6wz2zYwpgLJvc3kGYY23hhq','2016-12-07 03:06:17'),(54,16,'$2y$10$17LjjGTI9r3KjQYOKAABQuJqlFFc5xMQBTOUKsShJTzGwZOzEFTji','2016-12-07 03:25:43'),(56,17,'$2y$10$UF4MauWGxDKqaE45Euiuhun27xV9wEoPpnrAH/0o3K/g.owZ.i4/6','2016-12-07 07:36:46'),(62,1,'$2y$10$1VqZ5xeQqBSWiqcdy3vu5.ihgXMFN83uIGTkyb0N7TajdgOM8zloG','2016-12-10 15:12:43'),(63,1,'$2y$10$BwB7hIHxOc1iTZuiZh2xXuoxx0B5uvl23lzdeZD6hQJkWSjMgOhGa','2016-12-11 11:26:03'),(69,8,'$2y$10$kqwlHjrKSAwoqzrlfFALDu8t97rvCL2z56gafwuhh.mzsObQDl3Ue','2016-12-13 10:29:00'),(70,20,'$2y$10$jpSlcfXcv4/9D5x1EcPrvO9EjnQxM1L2ctaXOVmJ/Od3TTQGEKy9m','2016-12-13 11:10:01'),(73,8,'$2y$10$Xq09aUjfBSx/287AOyYwFuoBenCoZcdgPqwq86GX6Zsnfz//.Q0BK','2016-12-13 11:48:02'),(76,2,'$2y$10$16AORJ7bRXMC4fVDAUaw.O39vcAdkUhgDAEJovTgMWNqftY6W7mM6','2016-12-15 02:54:20'),(77,1,'$2y$10$mKclEoA.xPkSMPXhicuJSOKNqrKER3hDaM2Rjfzmcj5a8tCs8SbR.','2016-12-16 08:34:19'),(78,1,'$2y$10$sOq8CwS.a.EJb5JntjSNLe49jMPjZ6yhDddv57cp5BVkvBLFWmwiO','2016-12-16 09:52:56'),(79,1,'$2y$10$hq1VkabpOhrh1ysKTthLyOLvOWTZbzT1gpHeYw4/nliPIPa/N/mu6','2016-12-17 15:15:43'),(80,10,'$2y$10$1duhDOkHICgoT6Zq9V4rl.8kt5hAdX3oQovb1kK8bWgR.RlIN32N2','2016-12-17 17:47:27'),(96,1,'$2y$10$GyDBWlu9SnBGUWf/o9dnaOos5j5DqtZHc5sEDy6ZVbxzhzbNHBidK','2016-12-20 09:40:30'),(100,2,'$2y$10$t282l30NUB8ptEq9f7OywuOHXzwXhf5DxyUBfCAqOT5fbLUwQDdCi','2016-12-23 07:41:09');
/*!40000 ALTER TABLE `user_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-23 14:48:37

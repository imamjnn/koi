<?php

if (!defined('BASEPATH'))
    die;

class Auction extends MY_Controller {

    function __construct() {
        parent::__construct();

    }

    public function single($slug=null){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$slug)
            return $this->show_404();
        
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->model('Auction_model', 'Auction');
        $this->load->model('Banner_model', 'Banner');
        $this->load->model('Supplier_model', 'Supplier');
        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Itembreeder_model', 'IBreeder');
        $this->load->model('Itemvariety_model', 'IVariety');
        $this->load->model('Itemfavorite_model', 'IFavorite');
        $this->load->library('ObjectFormatter', '', 'formatter');
        
        $params = array(
            'items' => array(),
            'pagination' => array(),
            'banners' => null,
            'bids_current' => array(),
            'varietys' => array(),
            'breeders' => array(),
            'suppliers' => null,
            'item' => array()
        );

        $auction = $this->Auction->getBy('slug', $slug);
        if(!$auction)
            return $this->show_404();
        if($auction->status == 2)
            return $this->show_404();
        // items
        $cond['auction'] = $auction->id;
        $cond2 = array('auction' => $auction->id);

        $args = ['q', 'price', 'item_variety', 'item_breeder'];
        foreach ($args as $arg) {
            $arg_val = $this->input->get($arg);
            if ($arg_val)
                $cond[$arg] = $arg_val;
        }

            $this->load->model('Itemvariety_model', 'IVariety');
            $all_variety = $this->IVariety->getByCond([], true);
            $params['varietys'] = $all_variety;

            $this->load->model('Itembreeder_model', 'IBreeder');
            $all_breeder = $this->IBreeder->getByCond([], true);
            $params['breeders'] = $all_breeder;


        $rpp = 8;
        $page = $this->input->get('page');
        if (!$page)
            $page = 1;
        

        $result = $this->AItem->findByCond($cond, $rpp, $page, ['updated' => 'DESC']);
        if ($result)
            $params['items'] = $this->formatter->auction_item($result);

        $fish = $this->AItem->getBy('user', $this->user->id);
        
        
        $auction = $this->formatter->auction($auction, false, false);
        //$category->schema = $this->_schemaCategory($category);
        $params['auction'] = $auction;
        //deb($params);
        
        
        $rpp = 8;
        $page = $this->input->get('page');
        if(!$page)
            $page = 1;
        
        if(!is_dev() && $page == 1)
            $this->output->cache((60*60*5));
        
        /*$item = $this->AItem->getByCond($cond, $rpp, $page);
        if($item)
            $params['items'] = $this->formatter->auction_item($item);
        //deb($params);*/

        $banner = $this->Banner->getByCond(false, $rpp, $page);
        if ($banner)
            $params['banners'] = $this->formatter->banner($banner, false, true);

        $supplier = $this->Supplier->getByCond(false, $rpp, $page);
        if ($supplier)
            $params['suppliers'] = $this->formatter->supplier($supplier, false, true);
        
        $total_result = $this->AItem->findByCondTotal($cond);
        if($total_result > $rpp){
            $pagination_cond = $cond;
            
            unset($pagination_cond['auction']);
            //unset($pagination_cond['status']);
            
            $this->load->helper('pagination');
            $params['pagination'] = calculate_pagination($total_result, $page, $rpp, $pagination_cond);
        }
        
        $view = 'auction/single';
        if($this->theme->exists('auction/single-' . $auction->slug))
            $view = 'auction/single-' . $auction->slug;
        
        $this->respond($view, $params);
    }

    public function item($id=null){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();
        
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Banner_model', 'Banner');
        $this->load->model('Itembreeder_model', 'IBreeder');
        $this->load->model('Itemvariety_model', 'IVariety');
        $this->load->model('Supplier_model', 'Supplier');
        $this->load->model('Auction_model', 'Auction');
        $this->load->library('ObjectFormatter', '', 'formatter');
        
        $params = array(
            'items' => array(),
            'banners' => null,
            'bids' => null,
            'bids_current' => array(),
            'auctions' => array(),
            'suppliers' => null

        );
        
        $item = $this->AItem->getBy('id', $id);
        if(!$item)
            return $this->show_404();
        
        $item = $this->formatter->auction_item($item, false, true);

        $params['item'] = $item;
        //deb($params);

        $bid_current = $this->IBid->getByCond(['auction_item'=>$id],1,false, ['bid_price'=>'DESC']);
        if ($bid_current)
            $params['bids_current'] = $this->formatter->item_bid($bid_current, false, ['user']);

        /*$time1 = date('Y-m-d H:i:s');
        $time2 = 600;
        $hasil = strtotime($time1)+$time2;
        echo date('Y-m-d H:i:s', $hasil);
        if($bid_current->time->time < 600){
            echo 'dirk';
        }else{
            echo 'no';
        }*/

        $auction = $this->Auction->getBy('id', $item->auction);
        if(!$auction)
            return $this->show_404();

        $auction = $this->formatter->auction($auction, false, true);
        $params['auctions'] = $auction;

        // items
        $cond = array(
            'id' => $item->id
        );
        
        $rpp = 8;
        $page = $this->input->get('page');
        if(!$page)
            $page = 1;
        
        if(!is_dev() && $page == 1)
            $this->output->cache((60*60*5));
        
        $items = $this->AItem->findByCond($cond, $rpp, $page);
        if($items)
            $params['items'] = $this->formatter->auction_item($items);

        $banner = $this->Banner->getByCond(false, $rpp, $page);
        if ($banner)
            $params['banners'] = $this->formatter->banner($banner, false, true);

        $supplier = $this->Supplier->getByCond(false, $rpp, $page);
        if ($supplier)
            $params['suppliers'] = $this->formatter->supplier($supplier, false, true);

        $bid = $this->IBid->getByCond(['auction_item'=>$id, 'user'=>$this->user->id],false,false, ['id'=>'DESC']);
        if ($bid)
            $params['bids'] = $this->formatter->item_bid($bid, false, true);
        

        $bid_current = $this->IBid->getByCond(['auction_item'=>$id],1,false, ['bid_price'=>'DESC']);
        if ($bid_current)
            $params['bids_current'] = $this->formatter->item_bid($bid_current, false, ['user']);

        $view = 'auction/item/single';
        if($this->theme->exists('auction/item/single-' . $item->id))
            $view = 'auction/item/single-' . $item->id;
        
        $this->respond($view, $params);
    }

    public function item_modal($id=null){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();
        
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Itembreeder_model', 'IBreeder');
        $this->load->model('Itemvariety_model', 'IVariety');
        $this->load->model('Auction_model', 'Auction');
        $this->load->library('ObjectFormatter', '', 'formatter');
        
        $params = array(
            'items' => array(),
            'banners' => null,
            'bids' => null,
            'bids_current' => array(),
            'auctions' => array(),
            'suppliers' => null

        );
        
        $item = $this->AItem->getBy('id', $id);
        if(!$item)
            return $this->show_404();

        $item = $this->formatter->auction_item($item, false, true);
        $params['item'] = $item;

        $auction = $this->Auction->getBy('id', $item->auction);
        if(!$auction)
            return $this->show_404();
        
        $auction = $this->formatter->auction($auction, false, true);
        $params['auctions'] = $auction;

        // items
        $cond = array(
            'id' => $item->id
        );
        
        $rpp = 12;
        $page = $this->input->get('page');
        if(!$page)
            $page = 1;
        
        if(!is_dev() && $page == 1)
            $this->output->cache((60*60*5));
        
        $items = $this->AItem->findByCond($cond, $rpp, $page);
        if($items)
            $params['items'] = $this->formatter->auction_item($items);
        echo $item->video;
        
    }

    public function detail_modal($id=null){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();
        
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Itembreeder_model', 'IBreeder');
        $this->load->model('Itemvariety_model', 'IVariety');
        $this->load->model('Auction_model', 'Auction');
        $this->load->library('ObjectFormatter', '', 'formatter');
        
        $params = array(
            'items' => array(),
            'banners' => null,
            'bids' => null,
            'bids_current' => array(),
            'auctions' => array(),
            'suppliers' => null

        );
        
        $item = $this->AItem->getBy('id', $id);
        if(!$item)
            return $this->show_404();

        $item = $this->formatter->auction_item($item, false, true);
        $params['item'] = $item;

        $auction = $this->Auction->getBy('id', $item->auction);
        if(!$auction)
            return $this->show_404();
        
        $auction = $this->formatter->auction($auction, false, true);
        $params['auctions'] = $auction;

        // items
        $cond = array(
            'id' => $item->id
        );
        
        $rpp = 12;
        $page = $this->input->get('page');
        if(!$page)
            $page = 1;
        
        if(!is_dev() && $page == 1)
            $this->output->cache((60*60*5));

        $bid_current = $this->IBid->getByCond(['auction_item'=>$id],1,false, ['bid_price'=>'DESC']);
        if ($bid_current)
            $params['bids_current'] = $this->formatter->item_bid($bid_current, false, ['user']);
        
        $items = $this->AItem->findByCond($cond, $rpp, $page);
        if($items)
            $params['items'] = $this->formatter->auction_item($items);
        
        $this->respond('auction/modal_detail', $params);
        
    }

    public function history_modal($id=null){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();
        
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Auction_model', 'Auction');
        $this->load->library('ObjectFormatter', '', 'formatter');
        
        $params = array(
            'bids' => array(),
            'wins' => array()
        );

        $bid_current = $this->IBid->getByCond(['auction_item'=>$id],1,false, ['bid_price'=>'DESC']);
        if ($bid_current)
            $params['bids_current'] = $this->formatter->item_bid($bid_current, false, ['user']);
        //deb($params);

        $bid = $this->IBid->getByCond(['auction_item' => $id], 3, false, ['bid_price' => 'DESC'] );
        if($bid)
            $params['bids'] = $this->formatter->item_bid($bid);

        $this->respond('auction/modal_history', $params);
        
    }

    public function bid($id=null){
        
        /*$bid_price    = $this->input->post('bid_price');
        $data       = array(
                        
                        'bid_price'   => $bid_price);
        $this->load->model('Itembid_model');
        $this->Itembid_model->input_bid($data, 'item_bid');
        */
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();
        
        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->library('ObjectFormatter', '', 'formatter');
        
        $params = array(
            'bids' => array('bid_price'),
            'bid_price' => array()
        );

        $item = $this->AItem->getBy('id', $id);
        if(!$item)
            return $this->show_404();
        
        $item = $this->formatter->auction_item($item, false, true);
        $params['item'] = $item;

        $bid = $this->IBid->getBy('auction_item', $id);
        if ($bid)
            $params['bid_price'] = $this->formatter->bid($bid, false, true);

        $new_object= $this->user->id;
        //$row=$bid->bid_price;
        $bid_price    = $this->input->post('bid_price');
        $data       = array(
            'bid_price'   => $bid_price,
            'time' => date('Y-m-d H:i:s'),
            'user' => $new_object,
            'auction_item' => $id,
            'auction' => $item->auction
            );

        $this->load->model('Itembid_model');
        $this->Itembid_model->input_bid($data, 'item_bid');

        if(!$id){
            echo 'no item';
        }else{
            $this->AItem->set($id, ['current_price' => $bid_price]);
        }

    }

    public function refresh($id=null){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();
        
        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->model('Auction_model', 'Auction');
        $this->load->library('ObjectFormatter', '', 'formatter');
        
        $params = array(
            'bids' => array('bid_price'),
            'bid_price' => array(),
            'auctions' => array(),
            'bids_someone' => array()
        );

        $item = $this->AItem->getBy('id', $id);
        if(!$item)
            return $this->show_404();
        
        $item = $this->formatter->auction_item($item, false, true);
        $params['item'] = $item;

        $auction = $this->Auction->getBy('id', $item->auction);
        if(!$auction)
            return $this->show_404();
        
        $auction = $this->formatter->auction($auction, false, true);
        $params['auctions'] = $auction;

        $bid = $this->IBid->getBy('auction_item', $id);
        if ($bid)
            $params['bid_price'] = $this->formatter->bid($bid, false, true);

        $bid_someone = $this->IBid->getByCond(['auction_item'=>$id],false,false, ['id'=>'DESC']);
        if ($bid_someone)
            $params['bids_someone'] = $this->formatter->item_bid($bid_someone, false, ['user']);

        if($bid_someone){
        foreach ($bid_someone as $bid_s) {
           
            echo '<li class="right clearfix">';
            echo '<span class="chat-img pull-right">
                  <img src="'.$bid_s->user->avatar->_50x50.'" alt="User Avatar" class="img-circle" />
                  </span>';
            echo '<div class="chat-body clearfix">';
            echo '<div class="header-chat-body">';
            echo '<small class="time-text text-muted pull-left">';
            echo '<span class="glyphicon glyphicon-time"></span>'.$bid_s->time->format('d M H:i');
            echo '</small>';
            echo '<strong class="primary-font pull-right">'.$bid_s->user->fullname.'';
            echo '</strong>';

            echo '</div>' ;
            echo '<p>'.number_format($bid_s->bid_price,0,",",".").'</p>';                   
            echo '</div>';
            echo'</li>';
            }
        }

    }

    public function refresh_top_bid($id=null){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();
        
        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->library('ObjectFormatter', '', 'formatter');
        
        $params = array(
            'bids' => array('bid_price'),
            'bid_price' => array(),
            'bids_current' => array()
        );

        $item = $this->AItem->getBy('id', $id);
        if(!$item)
            return $this->show_404();
        
        $item = $this->formatter->auction_item($item, false, true);
        $params['item'] = $item;

        $bid = $this->IBid->getBy('auction_item', $id);
        if ($bid)
            $params['bid_price'] = $this->formatter->bid($bid, false, true);
        //deb($bid);

        $bid_current = $this->IBid->getByCond(['auction_item'=>$id],1,false, ['bid_price'=>'DESC']);
        if ($bid_current)
            $params['bids_current'] = $this->formatter->item_bid($bid_current, false, ['user']);

        echo '<span class="hidden-text">Highest Bid : </span> ';
        if(!$bid){
            echo number_format($item->price,0,',','.');
        }else{
            echo number_format($bid->bid_price,0,',','.');
        }

        if(!$bid_current){
            echo ' By -';
        }else{
            echo ' By ' .$bid_current->user->fullname;
        }
   
    }

    public function refresh_budget($id){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();

        $this->load->model('Budgetbid_model', 'BBid');

        $cond = array(
            'user' => $this->user->id,
            'auction_item' => $id
            );
        $budget = $this->BBid->getByCond($cond, 1, false, ['budget'=>'DESC']);
        
        if(!$budget){
            echo '-';
        }else{
            echo number_format($budget->budget,'0',',','.');
        }
    }

    public function favorite($id){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();

        $this->load->model('Itemfavoritechain_model', 'IFChain');
        $this->load->model('Itemfavorite_model', 'IFavorite');

        $checkfav = $this->IFavorite->getByCond(['auction_item'=>$id, 'user'=>$this->user->id], true);
        
        //deb($checkfav);
        if(!$checkfav){
            $new_object1['user'] = $this->user->id;
            $new_object1['auction_item']= $id;
            $new_object1['id'] = $this->IFavorite->create($new_object1);

            $new_object2['auction_item']= $id;
            $new_object2['item_favorite'] = $new_object1['id'];
            $new_object2['id'] = $this->IFChain->create($new_object2);
        }else{
            echo 'is fav';
        }
        
        /*$new_object1['user'] = $this->user->id;
        $new_object1['auction_item']= $id;
        $new_object1['id'] = $this->IFavorite->create($new_object1);

        $new_object2['auction_item']= $id;
        $new_object2['item_favorite'] = $new_object1['id'];
        $new_object2['id'] = $this->IFChain->create($new_object2);*/

    }

    public function unfavorite($id){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();

        $this->load->model('Itemfavorite_model', 'IFavorite');
        $this->load->model('Itemfavoritechain_model', 'IFChain');

        $cond = array(
            'item_favorite' => $id
            );
        $cond2 = array(
            'id' => $id
            );

        $this->IFavorite->delFav($cond2, 'item_favorite');
        $this->IFChain->delFavC($cond, 'item_favorite_chain');

        return $this->redirect(base_url());
    }

    public function autoemail($id){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();

        $this->load->model('Itembid_model', 'IBid');
        $this->load->library('ObjectFormatter', '', 'formatter');

        $params = array(
            'bids');

        $cond = array(
            'auction_item' => $id);

        $bid = $this->IBid->getByCond($cond, 1, false, ['bid_price' => 'DESC']);
        if($bid)
            $bids = $this->formatter->item_bid($bid);

        $guser = $this->IBid->getUser($cond);
        if($guser)
            $gusers = $this->formatter->item_bid2($guser);
        $list = array();
            foreach ($gusers as $key => $value) {
                $list[]= $value->user->email;
                
        }
                        

        if(empty($bids))
            return  'ra ono';
        //auto send email to winner
        $win = $bids->user->email;
        $mess = '<b>Deal All</b>
                <p>With the receipt of this message, we inform that the auction of <b>'.$bids->auction_item->name.'</b> has ended, 
                here are the results '.base_url('auction/history_modal').'/'.$bids->auction_item->id.'</p>
                '
                ;
        

        $this->load->library('email');
            $this->email->set_newline("\r\n");

            $email = $list;
            $this->email->from('gadingkoiauction@gmail.com', 'Gading Koi Auction');
            $this->email->to($email); 

            $this->email->subject('Auction has been complete');
            $this->email->message($mess);  

            $this->email->send();   

            return $this->redirect('/'.$bids->auction->page);
    }

}

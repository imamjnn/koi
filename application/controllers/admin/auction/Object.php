<?php

if(!defined('BASEPATH'))
    die;

/**
 * The `Object` controller
 */
class Object extends MY_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('Auction_model', 'Auction');
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->model('User_model', 'User');
        $this->load->library('ObjectFormatter', '', 'formatter');
    }

    function edit($id=null, $auction=null){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$id && !$this->can_i('create-auction'))
            return $this->show_404();
        if($id && !$this->can_i('update-auction'))
            return $this->show_404();

        $this->load->library('SiteForm', '', 'form');

        $params = [];

        if($id){
            $object = $this->Auction->get($id);
            if(!$object)
                return $this->show_404();
            $params['title'] = _l('Edit Auction Room');
        }else{
            $object = (object)array('status' => 2);
            $params['title'] = _l('Create New Auction Room');
        }


        $this->form->setObject($object);
        $this->form->setForm('/admin/auction');

        $params['auction'] = $object;

        $statuses= $this->enum->item('auction.status');
        $params['statuses']= $statuses;


        if(!($new_object=$this->form->validate($object)))
            return $this->respond('auction/edit', $params);

        if($new_object === true)
            return $this->redirect('/admin/auction');

        if(!$id){
            $new_object['user'] = $this->user->id;
            $new_object['id'] = $this->Auction->create($new_object);
            $this->event->auction->created($new_object);
        }else{
            $this->Auction->set($id, $new_object);
            $this->event->auction->updated($object, $new_object);

            $object = $this->formatter->auction($object, false, false);
            $this->output->delete_cache($object->page);
            $this->output->delete_cache($object->page . '/feed.xml');
        }


        $auction = $this->Auction->get($id);
        if($auction->status == 4){
            $closing_distance = $auction->time_distance;
            $items = $this->AItem->getBy('auction', $auction->id, true, false, ['id'=>'ASC']);

            //usort( $items, function($a, $b){ return $a->index - $b->index; });
        
            $last_closing_time = date_create($auction->time_end);

            foreach($items as $item){
                $item->time_end = date_add($last_closing_time, date_interval_create_from_date_string($closing_distance . 'minutes'));
                $last_closing_time = $item->time_end;
                $this->AItem->set($item->id, array( 'time_end' => $item->time_end->format('Y-m-d H:i:s') ));
            }

            //auto send email to members
            $mail = $this->User->getByCond([], false, false, ['id'=>'ASC']);
            $auctions = $this->formatter->auction($auction);
            $list = array();
            foreach ($mail as $key => $value) {
                $list[]= $value->email;
            }
            $mess = '<html>
                <table style="height: 101px;" width="585">
                <tbody>
                <tr>
                <td style="height: 50px;"><img src="http://gadingkoiauction.com/theme/default/static/img/elements/nav-logo.png" alt="" width="287" height="86" /></td>
                </tr>
                <tr><th style="height: 20px; background-color: red;">
                <h2><strong>'.$auctions->name.'</strong></h2>
                </th></tr>
                <tr>
                <td style="height: 20px;">
                <p>Start event : <b>'.$auctions->time_start->format('d F Y H:i').'</b></p>
                <p>End event : <b>'.$auctions->time_end->format('d F Y H:i').'</b></p>
                <p>For more details, please visit <a href="'.base_url($auctions->page).'" target="_blank">here</a></p>
                </td>
                </tr>
                </tbody>
                </table>
                </html>';

            $this->load->library('email');
            $this->email->set_newline("\r\n");

            $email = $list;
            $this->email->from('gadingkoiauction@gmail.com', 'Gading Koi Auction');
            $this->email->to($email); 

            $this->email->subject('Event');
            $this->email->message($mess);  

            $this->email->send();
        
        }
 
        $this->redirect('/admin/auction');
    
    }

    function index(){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$this->can_i('read-auction'))
            return $this->show_404();

        $this->load->model('User_model', 'User');
        $params = array(
            'title' => _l('Auction'),
            'auctions' => []
        );

        $cond = array();

        $rpp = true;
        $page= false;

        $result = $this->Auction->getByCond($cond, $rpp, $page, ['id'=>'DESC']);
        if($result){
            $this->load->library('ObjectFormatter', '', 'format');
            $params['auctions'] = $this->format->auction($result, false, true);
        }

        $this->respond('auction/index', $params);
    }

    function remove($id){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$this->can_i('delete-auction'))
            return $this->show_404();

        $this->cache->file->delete('auction');
        $this->event->auction->deleted($id);
        
        $this->Auction->remove($id);
        $this->redirect('/admin/auction');
    }
}
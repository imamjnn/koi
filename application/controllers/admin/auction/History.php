<?php

if(!defined('BASEPATH'))
    die;

/**
 * The `Object` controller
 */
class History extends MY_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->library('ObjectFormatter', '', 'formatter');
    }

    function edit($id=null){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$id && !$this->can_i('create-item_breeder'))
            return $this->show_404();
        if($id && !$this->can_i('update-item_breeder'))
            return $this->show_404();

        $this->load->library('SiteForm', '', 'form');

        $params = [];

        if($id){
            $object = $this->Breeder->get($id);
            if(!$object)
                return $this->show_404();
            $params['title'] = _l('Edit Breeder');
        }else{
            $object = (object)array();
            $params['title'] = _l('Create New Breeder');
        }

        $this->form->setObject($object);
        $this->form->setForm('/admin/auction/item/breeder');

        $params['breeder'] = $object;

        if(!($new_object=$this->form->validate($object)))
            return $this->respond('auction/item/breeder/edit', $params);

        if($new_object === true)
            return $this->redirect('/admin/auction/item/breeder');

        if(!$id){
            $new_object['user'] = $this->user->id;
            $new_object['id'] = $this->Breeder->create($new_object);
            $this->event->breeder->created($new_object);
        }else{
            $this->Breeder->set($id, $new_object);
            $this->event->breeder->updated($object, $new_object);
        }

        $this->cache->file->delete('breeder');
        
        $this->redirect('/admin/auction/item/breeder');
    }

    function show($id=null){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$this->can_i('read-item_history'))
            return $this->show_404();

        $auction_item = $this->AItem->get($id);
        if(!$auction_item)
            return $this->show_404();

        $params = array(
            'title' => _l('History'),
            'bids' => [],
            'items' => array()
        );

        $item = $this->AItem->getByCond(['id'=>$id], false, false, ['updated'=>'DESC']);
        if($item)
            $params['items'] = $this->formatter->auction_item($item);

        $bid = $this->IBid->getByCond(['auction_item'=>$id], false, false, ['time'=>'DESC']);
        if($bid)
            $params['bids'] = $this->formatter->item_bid($bid);
        
        $this->respond('auction/history/index', $params);
    }

    function remove($id=null){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());

        $cond = array('auction_item'=> $id);
        $this->IBid->delBid($cond, 'item_bid');

        $this->redirect('/admin/auction/history/show/'.$id);
    }
}
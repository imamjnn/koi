<?php

if(!defined('BASEPATH'))
    die;

/**
 * The `Item` controller
 */
class Item extends MY_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->model('Auction_model', 'Auction');
        $this->load->library('ObjectFormatter', '', 'formatter');
    }

    function edit($id=null, $auction=null){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$id && !$this->can_i('create-auction_item'))
            return $this->show_404();
        if($id && !$this->can_i('update-auction_item'))
            return $this->show_404();

        $this->load->library('SiteForm', '', 'form');

        $this->load->model('Itemvariety_model', 'IVariety');
        $this->load->model('Itemvarietychain_model', 'IVChain');
        $this->load->model('Itembreeder_model', 'IBreeder');
        $this->load->model('Itembreederchain_model', 'IBChain');
        $this->load->model('Itembid_model', 'IBid');

        $params = array(
            'slug_editable' => true,
            'reporter' => null
        );

        if($id){
            $object = $this->AItem->get($id);
            if(!$object)
                return $this->show_404();

            $params['title'] = _l('Edit Item');

            $object_varieties = $this->IVChain->getBy('auction_item', $id, true);
            $object->variety = $object_varieties ? prop_values($object_varieties, 'item_variety') : array();
            $object_breeders = $this->IBChain->getBy('auction_item', $id, true);
            $object->breeder = $object_breeders ? prop_values($object_breeders, 'item_breeder') : array();

            if(!$this->can_i('update-item_slug'))
                $params['slug_editable'] = false;
            
            if($object->user != $this->user->id){
                $reporter = $this->User->get($object->user);
                if($reporter)
                    $params['reporter'] = $this->formatter->user($reporter);
            }

        }else{
            $object = (object)array('status' => 1);
            if($this->can_i('read-item_variety'))
                $object->variety = [];
            if($this->can_i('read-item_breeder'))
                $object->breeder = [];

            $params['title'] = _l('Create Item');
        }

        $this->form->setObject($object);
        $this->form->setForm('/admin/auction/item');

        $genders = $this->enum->item('item.gender');
        $params['genders'] = $genders;

        $params['auction_item'] = $object;

        //get variety
        if($this->can_i('read-item_variety')){
            $all_varieties = array();
            $params['varieties'] = array();
            
            $varieties = $this->IVariety->getByCond([], true, false, ['name'=>'ASC']);
            if($varieties){
                $all_varieties = $this->formatter->item_variety($varieties, 'id', false);
                $params['varieties'] = prop_as_key($varieties, 'id', 'name');
                
                // let show only all selected tag
                $visible_variety = array();
                if($object->variety){
                    foreach($object->variety as $variety){
                        if(array_key_exists($variety, $params['varieties']))
                            $visible_variety[$variety] = $params['varieties'][$variety];
                    }
                }
                
                $params['varieties'] = $visible_variety;
            }
        }

        //get breeder
        if($this->can_i('read-item_breeder')){
            $all_breeders = array();
            $params['breeders'] = array();
            
            $breeders = $this->IBreeder->getByCond([], true, false, ['fullname'=>'ASC']);
            if($breeders){
                $all_breeders = $this->formatter->item_breeder($breeders, 'id', false);
                $params['breeders'] = prop_as_key($breeders, 'id', 'fullname');
                
                // let show only all selected tag
                $visible_breeder = array();
                if($object->breeder){
                    foreach($object->breeder as $breeder){
                        if(array_key_exists($breeder, $params['breeders']))
                            $visible_breeder[$breeder] = $params['breeders'][$breeder];
                    }
                }
                
                $params['breeders'] = $visible_breeder;
            }
        }

        $auctions = $this->Auction->get($auction);

        if(!$auctions)
            return $this->show_404();
        $params['auctions']= $this->formatter->auction($auctions, true);

        

        if(!($new_object=$this->form->validate($object)))
            return $this->respond('auction/item/edit', $params);

        if($new_object === true)
            return $this->redirect('/admin/auction/' .$auction. '/item');

        // make sure user not change the slug if he's not allowed
        if($id && array_key_exists('slug', $new_object) && !$this->can_i('update-item_slug'))
            unset($new_object['slug']);

        // save variety chain
        $to_insert_variety = array();
        if(!array_key_exists('variety', $new_object))
            $new_object['variety'] = array();
        
        $new_varieties = $new_object['variety'];
        unset($new_object['variety']);
        
        if($this->can_i('read-item_variety')){
        
            $old_varieties = array();
            if($id)
                $old_varieties = $object->variety;
            
            $to_insert = array();
            $to_delete = array();
            
            foreach($new_varieties as $cat){
                if(!in_array($cat, $old_varieties)){
                    $variety = null;
                    if(array_key_exists($cat, $all_varieties))
                        $variety = $all_varieties[$cat];
                    if(!$variety)
                        continue;
                    
                    $to_insert[] = $cat;
                    $this->IVariety->inc($cat, 'items', 1, true);
                    //$this->output->delete_cache($variety->page);
                    //$this->output->delete_cache($variety->page . '/feed.xml');
                }
            }
            
            foreach($old_varieties as $cat){
                $old_variety = null;
                if(array_key_exists($cat, $all_varieties))
                    $old_variety = $all_varieties[$cat];
                if(!in_array($cat, $new_varieties)){
                    $variety = null;
                    if(array_key_exists($cat, $all_varieties))
                        $variety = $all_varieties[$cat];
                    if(!$variety)
                        continue;
                    
                    $to_delete[] = $cat;
                    $this->IVariety->dec($cat, 'items', 1, true);
                }
                if($old_variety){
                    $this->output->delete_cache($old_variety->page);
                    $this->output->delete_cache($old_variety->page . '/feed.xml');
                }
            }
            
            if($to_delete)
                $this->IVChain->removeByCond(['auction_item'=>$id, 'item_variety'=>$to_delete]);

            if($to_insert)
                $to_insert_variety = $to_insert;
        }

        // save breeder chain
        $to_insert_breeder = array();
        if(!array_key_exists('breeder', $new_object))
            $new_object['breeder'] = array();
        
        $new_breeders = $new_object['breeder'];
        unset($new_object['breeder']);
        
        if($this->can_i('read-item_breeder')){
        
            $old_breeders = array();
            if($id)
                $old_breeders = $object->breeder;
            
            $to_insert = array();
            $to_delete = array();
            
            foreach($new_breeders as $cat){
                if(!in_array($cat, $old_breeders)){
                    $breeder = null;
                    if(array_key_exists($cat, $all_breeders))
                        $breeder = $all_breeders[$cat];
                    if(!$breeder)
                        continue;
                    
                    $to_insert[] = $cat;
                    $this->IBreeder->inc($cat, 'items', 1, true);
                    $this->output->delete_cache($breeder->page);
                    $this->output->delete_cache($breeder->page . '/feed.xml');
                }
            }
            
            foreach($old_breeders as $cat){
                $old_breeder = null;
                if(array_key_exists($cat, $all_breeders))
                    $old_breeder = $all_breeders[$cat];
                if(!in_array($cat, $new_breeders)){
                    $breeder = null;
                    if(array_key_exists($cat, $all_breeders))
                        $breeder = $all_breeders[$cat];
                    if(!$breeder)
                        continue;
                    
                    $to_delete[] = $cat;
                    $this->IBreeder->dec($cat, 'items', 1, true);
                }
                if($old_breeder){
                    //$this->output->delete_cache($old_breeder->page);
                    //$this->output->delete_cache($old_breeder->page . '/feed.xml');
                }
            }
            
            if($to_delete)
                $this->IBChain->removeByCond(['auction_item'=>$id, 'item_breeder'=>$to_delete]);

            if($to_insert)
                $to_insert_breeder = $to_insert;
        }
        
        $this->output->delete_cache('/auction/item/feed.xml');
        $this->output->delete_cache('/auction/item/instant.xml');
        
        if($id){
            $fobject = $this->formatter->auction_item($object, false, false);
            $this->output->delete_cache($fobject->page);
            //$this->output->delete_cache($fobject->amp);
        }

        if($new_object){
            $new_object['updated'] = date('Y-m-d H:i:s');
            if(!$id){
                $new_object['user'] = $this->user->id;
                $new_object['auction'] = $auction;
                $new_object['id'] = $this->AItem->create($new_object);
                $id = $new_object['id'];
                
                $this->event->auction_item->created($new_object);

                // new bid version
                $data = array(
                        'user' => $this->user->id,
                        'bid_price' => $this->input->post('price'),
                        'auction_item' => $id,
                        'auction' => $auction
                        );
                $this->IBid->insert_bid($data);
            }else{
                $this->AItem->set($id, $new_object);
                
                $this->event->auction_item->updated($object, $new_object);
            }
            
            // Delete all post cache that use me
        }

        if($to_insert_variety && $id){
            foreach($to_insert_variety as $index => $variety)
                $to_insert_variety[$index] = ['auction_item'=>$id, 'item_variety'=>$variety];
            $this->IVChain->create_batch($to_insert_variety);
        }

        if($to_insert_breeder && $id){
            foreach($to_insert_breeder as $index => $breeder)
                $to_insert_breeder[$index] = ['auction_item'=>$id, 'item_breeder'=>$breeder];
            $this->IBChain->create_batch($to_insert_breeder);
        }

        $this->redirect('/admin/auction/' .$auction. '/item');
    }

    function index($auction, $id=null){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$this->can_i('read-auction_item'))
            return $this->show_404();

        $auctions = $this->Auction->getByCond([], 7);
        
        if(!$auctions)
            return $this->show_404();

        $this->load->library('ObjectFormatter', '', 'formatter');
        
        $auctions = $this->formatter->auction($auctions, true);
        if(!array_key_exists($auction, $auctions))
            return $this->show_404();

        $params = array(
            'title' => _l('Auction Room'),
            'items' => [],
            'auctions' => $auctions,
            'auction_item' => null,
            'auction' => $auctions[$auction],
            'pagination' => array()
        );

        $cond = array('auction'=>$auction);

        $rpp = 10;
        $page= $this->input->get('page');
        if(!$page)
            $page = 1;

        $result = $this->AItem->getByCond($cond, $rpp, $page, ['updated'=>'DESC']);
        if($result)
            $params['items'] = $this->formatter->auction_item($result, false, false);

        // for pagination
        $total_result = $this->AItem->count();

        if($total_result > $rpp){
            $pagination_cond = $cond;
            $this->load->helper('pagination');
            $params['pagination'] = calculate_pagination($total_result, $page, $rpp, $pagination_cond);
        }
        $params['total']=$total_result;

        $this->respond('auction/item/index', $params);
    }

    function remove($id, $auction){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$this->can_i('delete-auction_item'))
            return $this->show_404();
        
        $auction_item = $this->AItem->get($id);
        if(!$auction_item)
            return $this->show_404();

        if($auction_item->user != $this->user->id && !$this->can_i('delete-auction_item_other_user'))
            return $this->show_404();
        
        $this->load->model('Itemvarietychain_model', 'IVChain');
        $this->load->model('Itemvariety_model', 'IVariety');
        $this->load->model('Itembreederchain_model', 'IBChain');
        $this->load->model('Itembreeder_model', 'IBreeder');
        $this->load->model('Itemfavorite_model', 'IFavorite');
        $this->load->model('Itemfavoritechain_model', 'IFChain');
        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Budgetbid_model', 'BBid');

        $this->AItem->remove($id);

        $cond = array('auction_item'=>$id);
        $this->IFavorite->delFav($cond, 'item_favorite');
        $this->IFChain->delFavC($cond, 'item_favorite_chain');
        $this->IBid->delBid($cond, 'item_bid');
        $this->BBid->delBudget($cond, 'budget_bid');
        
        // remove fish variety chain and dec total fishes of the variety
        $varies_chain = $this->IVChain->getBy('auction_item', $id, true);
        if($varies_chain){
            $varies_chain_id = array();
            $varies_id = prop_values($varies_chain, 'item_variety');
            $varies = $this->IVariety->get($varies_id, true);
            $varies = $this->formatter->item_variety($varies, 'id', false);
            foreach($varies_chain as $varie_chain){
                $varies_chain_id[] = $varie_chain->id;
                if(!array_key_exists($varie_chain->item_variety, $varies))
                    continue;
                $varie = $varies[$varie_chain->item_variety];
                $this->IVariety->dec($varie->id, 'items', 1, true);
                
            }
            
            $this->IVChain->remove($varies_chain_id);
        }
        
        // remove fish breeder chain and dec total fishes of the breeder
        $brees_chain = $this->IBChain->getBy('auction_item', $id, true);
        if($brees_chain){
            $brees_chain_id = array();
            $brees_id = prop_values($brees_chain, 'item_breeder');
            $brees = $this->IBreeder->get($brees_id, true);
            $brees = $this->formatter->item_breeder($brees, 'id', false);
            foreach($brees_chain as $bree_chain){
                $brees_chain_id[] = $bree_chain->id;
                if(!array_key_exists($bree_chain->item_breeder, $brees))
                    continue;
                $bree = $brees[$bree_chain->item_breeder];
                $this->IBreeder->dec($bree->id, 'items', 1, true);
                
            }
            
            $this->IBChain->remove($brees_chain_id);
        }
        
        
        $this->event->auction_item->deleted($auction_item);
        
        $auction_item = $this->formatter->auction_item($auction_item, false, false);
        
       
        
        $this->redirect('/admin/auction/' .$auction. '/item');
    }
}
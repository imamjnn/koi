<?php

if(!defined('BASEPATH'))
    die;

/**
 * The `Object` controller
 */
class Object extends MY_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('Supplier_model', 'Supplier');
    }

    function edit($id=null){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$id && !$this->can_i('create-supplier'))
            return $this->show_404();
        if($id && !$this->can_i('update-supplier'))
            return $this->show_404();

        $this->load->library('SiteForm', '', 'form');

        $params = [];

        if($id){
            $object = $this->Supplier->get($id);
            if(!$object)
                return $this->show_404();
            $params['title'] = _l('Edit Supplier');
        }else{
            $object = (object)array();
            $params['title'] = _l('Create New Supplier');
        }

        $this->form->setObject($object);
        $this->form->setForm('/admin/supplier');

        $params['supplier'] = $object;

        if(!($new_object=$this->form->validate($object)))
            return $this->respond('supplier/edit', $params);

        if($new_object === true)
            return $this->redirect('/admin/supplier');

        if(!$id){
            $new_object['user'] = $this->user->id;
            $new_object['id'] = $this->Supplier->create($new_object);
            $this->event->supplier->created($new_object);
        }else{
            $this->Supplier->set($id, $new_object);
            $this->event->supplier->updated($object, $new_object);
        }

        $this->cache->file->delete('supplier');
        $this->redirect('/admin/supplier');
    }

    function index(){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$this->can_i('read-supplier'))
            return $this->show_404();

        $params = array(
            'title' => _l('Suppliers'),
            'suppliers' => []
        );

        $cond = array();

        $rpp = true;
        $page= false;

        $result = $this->Supplier->getByCond($cond, $rpp, $page, ['name'=>'ASC']);
        if($result){
            $this->load->library('ObjectFormatter', '', 'format');
            $params['suppliers'] = $this->format->supplier($result, false, true);
        }

        $this->respond('supplier/index', $params);
    }

    function remove($id){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$this->can_i('delete-supplier'))
            return $this->show_404();

        $this->cache->file->delete('supplier');
        $this->event->supplier->deleted($id);
        
        $this->Supplier->remove($id);
        $this->redirect('/admin/supplier');
    }
}
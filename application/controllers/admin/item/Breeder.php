<?php

if(!defined('BASEPATH'))
    die;

/**
 * The `Object` controller
 */
class Breeder extends MY_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('Itembreeder_model', 'Breeder');
    }

    function edit($id=null){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$id && !$this->can_i('create-item_breeder'))
            return $this->show_404();
        if($id && !$this->can_i('update-item_breeder'))
            return $this->show_404();

        $this->load->library('SiteForm', '', 'form');

        $params = [];

        if($id){
            $object = $this->Breeder->get($id);
            if(!$object)
                return $this->show_404();
            $params['title'] = _l('Edit Breeder');
        }else{
            $object = (object)array();
            $params['title'] = _l('Create New Breeder');
        }

        $this->form->setObject($object);
        $this->form->setForm('/admin/auction/item/breeder');

        $params['breeder'] = $object;

        if(!($new_object=$this->form->validate($object)))
            return $this->respond('auction/item/breeder/edit', $params);

        if($new_object === true)
            return $this->redirect('/admin/auction/item/breeder');

        if(!$id){
            $new_object['user'] = $this->user->id;
            $new_object['id'] = $this->Breeder->create($new_object);
            $this->event->breeder->created($new_object);
        }else{
            $this->Breeder->set($id, $new_object);
            $this->event->breeder->updated($object, $new_object);
        }

        $this->cache->file->delete('breeder');
        
        $this->redirect('/admin/auction/item/breeder');
    }

    function index(){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$this->can_i('read-item_breeder'))
            return $this->show_404();

        $params = array(
            'title' => _l('Breeders'),
            'breeders' => [],
            'pagination' => array()
        );

        $cond = array();
        
        $rpp = 30;
        $page= $this->input->get('page');
        if(!$page)
            $page = 1;
        
        $filter_name = $this->input->get('q');

        $result = $this->Breeder->findByName($cond, $filter_name, $rpp, $page, ['fullname'=>'ASC']);
        unset($cond['id']);
        
        if($result)
            $params['breeders'] = $result;

                
        // for pagination
        $total_result = $this->Breeder->findByNameTotal($cond, $filter_name);
        if($total_result > $rpp){
            $pagination_cond = $cond;
            if($filter_name)
                $pagination_cond['q'] = $filter_name;
            
            $this->load->helper('pagination');
            $params['pagination'] = calculate_pagination($total_result, $page, $rpp, $pagination_cond);
        }
        
        $this->respond('auction/item/breeder/index', $params);
    }

    function remove($id){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$this->can_i('delete-item_breeder'))
            return $this->show_404();

        $this->event->breeder->deleted($id);
        $this->Breeder->remove($id);
        $this->redirect('/admin/auction/item/breeder');
    }
}
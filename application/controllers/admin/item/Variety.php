<?php

if(!defined('BASEPATH'))
    die;

/**
 * The `Category` controller
 */
class Variety extends MY_Controller
{

    function __construct(){
        parent::__construct();

        $this->load->model('Itemvariety_model', 'IVariety');
        $this->load->library('ObjectFormatter', '', 'formatter');
    }

    function edit($id=null){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$id && !$this->can_i('create-item_variety'))
            return $this->show_404();
        if($id && !$this->can_i('update-item_variety'))
            return $this->show_404();

        $this->load->library('SiteForm', '', 'form');

        $params = [];

        if($id){
            $object = $this->IVariety->get($id);
            if(!$object)
                return $this->show_404();
            $params['title'] = _l('Edit Item Variety');
        }else{
            $object = (object)array();
            $params['title'] = _l('Create New Item Variety');
        }

        $this->form->setObject($object);
        $this->form->setForm('/admin/auction/item/variety');

        $params['variety'] = $object;
        
        if(!($new_object=$this->form->validate($object)))
            return $this->respond('auction/item/variety/edit', $params);

        if($new_object === true)
            return $this->redirect('/admin/auction/item/variety');

        if(!$id){
            $new_object['user'] = $this->user->id;
            $new_object['id'] = $this->IVariety->create($new_object);
            
            $this->event->item_variety->created($new_object);
        }else{
            $this->IVariety->set($id, $new_object);
            
            $this->event->item_variety->updated($object, $new_object);
            
            $object = $this->formatter->item_variety($object, false, false);    
            $this->output->delete_cache($object->page);
            $this->output->delete_cache($object->page . '/feed.xml');
            
            // Delete all post cache that use me
        }

        $this->redirect('/admin/auction/item/variety');
    }

    function index(){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$this->can_i('read-item_variety'))
            return $this->show_404();

        $params = array(
            'title' => _l('Item Variety'),
            'varieties' => []
        );

        $cond = array();

        $rpp = true;
        $page= false;

        $result = $this->IVariety->getByCond($cond, $rpp, $page, ['name'=>'ASC']);

        if($result){
            $params['varieties'] = $result;
        }

        $this->respond('auction/item/variety/index', $params);
    }

    function remove($id){
        if(!$this->user)
            return $this->redirect('/admin/me/login?next=' . uri_string());
        if(!$this->can_i('delete-item_variety'))
            return $this->show_404();
        
        $variety = $this->IVariety->get($id);
        if(!$variety)
            return $this->show_404();
        
        $this->load->model('Itemvarietychain_model', 'IVChian');

        $this->event->item_variety->deleted($variety);
        
        $variety = $this->formatter->item_variety($variety, false, false);
        $this->output->delete_cache($variety->page);
        $this->output->delete_cache($variety->page . '/feed.xml');
        
        $this->IVariety->remove($id);
        $this->IVChian->removeBy('item_variety', $id);
        
        $this->redirect('/admin/auction/item/variety');
    }
}
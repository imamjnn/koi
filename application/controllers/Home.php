<?php

if (!defined('BASEPATH'))
    die;

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    private function _schemaHome() {
        $schemas = array();

        $data = array(
            '@context' => 'http://schema.org',
            '@type' => 'Organization',
            'name' => $this->setting->item('site_name'),
            'url' => base_url(),
            'logo' => $this->theme->asset('/static/image/logo/logo.png')
        );

        // social url
        $socials = array();
        $known_socials = array(
            'facebook',
            'gplus',
            'instagram',
            'linkedin',
            'myspace',
            'pinterest',
            'soundcloud',
            'tumblr',
            'twitter',
            'youtube'
        );

        foreach ($known_socials as $soc) {
            $url = $this->setting->item('site_x_social_' . $soc);
            if ($url)
                $socials[] = $url;
        }

        if ($socials)
            $data['sameAs'] = $socials;

        // phone contact number
        $contacts = array();
        $known_contacts = array(
            'baggage_tracking' => 'baggage tracking',
            'bill_payment' => 'bill payment',
            'billing_support' => 'billing support',
            'credit_card_support' => 'credit card support',
            'customer_support' => 'customer support',
            'emergency' => 'emergency',
            'package_tracking' => 'package tracking',
            'reservations' => 'reservations',
            'roadside_assistance' => 'roadside assistance',
            'sales' => 'sales',
            'technical_support' => 'technical support'
        );
        $contact_served = $this->setting->item('organization_contact_area_served');
        if ($contact_served) {
            $contact_served = explode(',', $contact_served);
            if (count($contact_served) == 1)
                $contact_served = $contact_served[0];
        }

        $contact_language = $this->setting->item('organization_contact_available_language');
        if ($contact_language) {
            $contact_language = explode(',', $contact_language);
            if (count($contact_language) == 1)
                $contact_language = $contact_language[0];
        }

        $contact_options = array();
        if ($this->setting->item('organization_contact_opt_tollfree'))
            $contact_options[] = 'TollFree';
        if ($this->setting->item('organization_contact_opt_his'))
            $contact_options[] = 'HearingImpairedSupported';

        foreach ($known_contacts as $cont => $name) {
            $phone = $this->setting->item('organization_contact_' . $cont);
            if (!$phone)
                continue;
            $contact = array(
                '@type' => 'ContactPoint',
                'telephone' => $phone,
                'contactType' => $name
            );
            if ($contact_served)
                $contact['areaServed'] = $contact_served;
            if ($contact_language)
                $contact['availableLanguage'] = $contact_language;
            if ($contact_options)
                $contact['contactOption'] = $contact_options;
            $contacts[] = $contact;
        }

        if ($contacts)
            $data['contactPoint'] = $contacts;

        $schemas[] = $data;

        return $schemas;
    }

    private function _fillHomeContent(&$params) {
        $this->load->model('Auction_model', 'Auction');
        $this->load->model('User_model', 'User');
        $this->load->model('Banner_model', 'Banner');
        $this->load->model('Supplier_model', 'Supplier');
        $this->load->model('Event_model', 'Event');
        $this->load->model('Itemfavorite_model', 'IFavorite');
        $this->load->library('ObjectFormatter', '', 'formatter');

        $cond = array(
            'status' => 4
        );

        $rpp = 5;
        $page = $this->input->get('page');
        if (!$page)
            $page = 1;

        if ($this->user)
            $cond2 = array('user' => $this->user->id);

        if ($this->user)
            $item_fav = $this->IFavorite->getByCond($cond2, false);
        if ($this->user)
            if ($item_fav)
                $params['itm_fav'] = $this->formatter->item_favorite($item_fav);

        $auction = $this->Auction->getByCond($cond, $rpp, $page);
        if ($auction)
            $params['auctions'] = $this->formatter->auction($auction, true, true);


        $banner = $this->Banner->getByCond(false, $rpp, $page);
        if ($banner)
            $params['banners'] = $this->formatter->banner($banner, false, true);

        $rpp = 8;

        if ($this->user)
            $params['items'] = $this->IBid->getItem(['item_bid.user'=>$this->user->id]);

        $page = $this->input->get('page');
        if (!$page)
            $page = 1;

        $supplier = $this->Supplier->getByCond(false, $rpp, $page);
        if ($supplier)
            $params['suppliers'] = $this->formatter->supplier($supplier, false, true);
    }

    public function index() {
        $this->load->model('Itembid_model', 'IBid');
        $params = array(
            'events' => array(),
            'banners' => array(),
            'users' => array(),
            'auctions' => array(),
            'suppliers' => array(),
            'home' => (object) array(
                'schema' => $this->_schemaHome()
            )
        );
        
        $this->_fillHomeContent($params);
        
        $this->respond('home', $params);
    }

    private function loginDone() {


        // only if i can see admin home page
        // or back to index otherwise

        return $this->redirect('/');
    }

    private function loginSet($user) {
        $this->user = $user;

        $session = array(
            'user' => $user->id,
            'hash' => password_hash(time() . '-' . $user->id, PASSWORD_DEFAULT),
            
        );

        $this->load->model('Usersession_model', 'USession');
        $this->USession->create($session);

        $cookie_name = config_item('sess_cookie_name');
        $cookie_expr = config_item('sess_expiration');
        $this->input->set_cookie($cookie_name, $session['hash'], $cookie_expr);
    }

    public function login() {

        if ($this->user)
            return $this->loginDone();

        $this->load->library('SiteForm', '', 'form');
        $this->form->setForm('login');

        $params = array(
            'title' => _l('Login'),
            'events' => array(),
            'banners' => array(),
            'users' => array(),
            'auctions' => array(),
            'suppliers' => array()
        );

        $this->_fillHomeContent($params);

        if (!($login = $this->form->validate()))
            return $this->respond('home', $params);

        $this->load->model('User_model', 'User');

        $name = $login['email'];
        $field = filter_var($name, FILTER_VALIDATE_EMAIL) ? 'email' : 'name';
        $user = $this->User->getBy($field, $name, 1);

        if ($user)
            $params['users'] = $user;

        if (!$user) {
            $this->form->setError('email', 'Email not found');
            return $this->respond('home', $params);
        }

        if (!password_verify($login['password'], $user->password)) {
            $this->form->setError('password', 'Invalid password');
            return $this->respond('home', $params);
        }

        $this->event->me->logged_in($user);
        $this->loginSet($user);
        $this->loginDone();
    }

}
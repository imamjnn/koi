<?php

if(!defined('BASEPATH'))
    die;

class Bid extends MY_Controller
{
    function __construct(){
        parent::__construct();
        
    }
    
    public function index(){
        $this->load->model('Autobid_model', 'ABid');
        $params = array('bids');

        $bid = $this->ABid->getByCond(false, false, false, ['id'=>'ASC']);
        if($bid)
            $params['bids'] = $bid;
        //deb($params);
        
        $this->respond('bid', $params);
    }

    public function input_bid($id=null){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();

        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Budgetbid_model', 'BBid');
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->library('ObjectFormatter', '', 'formatter');

        $id_usr = $this->user->id;
        $item = $this->AItem->getBy('id', $id);
        if(!$item)
            return $this->show_404();
        //deb($item);
        $bid = $this->IBid->getByCond(['auction_item'=>$id]);
        $budget_bid = $this->BBid->getByCond(['user'=>$id_usr, 'auction_item'=>$id]);
        $big_budget = $this->BBid->getByCond(['user'=>(object)['!=',$id_usr], 'auction_item'=>$id], 1, false, ['budget'=>'DESC']);
        $big_bid = $this->IBid->getByCond(['user'=>(object)['!=',$id_usr], 'auction_item'=>$id],1,false, ['bid_price'=>'DESC']);
        
        $xxx = $this->db->get_where('item_bid', array('bid_price'=>$bid->bid_price));
        $yyy = $xxx->row();
        //echo $yyy->bid_price;

        $bid_current = $this->IBid->getByCond(['auction_item'=>$id],1,false, ['bid_price'=>'DESC']);
        if ($bid_current)
            $params['bids_current'] = $this->formatter->item_bid($bid_current, false, ['user']);

        
        $this->AItem->set($id, ['current_price'=>$bid->bid_price]);

        $time1= $bid_current->time->time;
        $time2= strtotime($item->time_end);
        $hasil = $time2-$time1;


        if($hasil < 180){
            $time12 = 180;
            $time22 = strtotime($item->time_end);
            $hasil2 = $time22+$time12;
            $newendt = date('Y-m-d H:i:s',$hasil2);
            $this->AItem->set($id, ['time_end'=>$newendt]);
            echo '<script>parent.window.location.reload(true);</script>';
        }else{
        }

        if($bid->user == $id_usr){
        }elseif(!$budget_bid){
            echo '<div class="go-bid">!Let&#39;s bid now</div>';
        }elseif($bid->bid_price < $budget_bid->budget){
            $data = array(
                'user' => $this->user->id,
                'bid_price' => $bid->bid_price +100000,
                'auction_item' => $id,
                'auction' => $item->auction,
                );
            $this->IBid->insert_bid($data);
            //return $this->redirect('bid');
        }else{
            echo '<div class="go-bid" id="kop">!Let`s bid now</div>';
            echo '<div id="kopi"></div>';
        }

        if($bid->user != $id_usr){

        }
        elseif(!$big_budget){
        }
        elseif($big_bid->bid_price < $big_budget->budget){
            $data = array(
                'user' => $big_budget->user,
                'bid_price' => $bid->bid_price +100000,
                'auction_item' => $big_budget->auction_item,
                'auction' => $item->auction,
                );
            
            $this->IBid->insert_bid($data);
            
        }else{
        }

        $cond = array('auction_item' => $id);
        $bid2 = $this->IBid->getByCond($cond, 20, false, ['id'=>'DESC']);
        $bid3 = $this->IBid->getItemGroup($cond, 'bid_price');
        //deb($bid3);
        if($bid2)
            $params['bids'] = $this->formatter->item_bid($bid2);

        $this->respond('partial/bid_area_v2_content', $params);

        /*if(empty(!$bid2)){
            foreach ($bid2 as $b2) {
                echo '<li class="right clearfix">';
                echo '<span class="chat-img pull-right">
                      <img src="'.$b2->user->avatar->_50x50.'" alt="User Avatar" class="img-circle" />
                      </span>';
                echo '<div class="chat-body clearfix">';
                echo '<div class="header-chat-body">';
                echo '<small class="time-text text-muted pull-left">';
                echo '<span class="glyphicon glyphicon-time"></span>'.$b2->time->format('d M H:i');
                echo '</small>';
                echo '<strong class="primary-font pull-right">'.$b2->user->fullname.'';
                echo '</strong>';

                echo '</div>' ;
                echo '<p>'.number_format($b2->bid_price,0,",",".").'</p>';                   
                echo '</div>';
                echo'</li>';
            }
        }*/
    }

    public function automail($id) {
        if (!$this->user)
            return $this->redirect('/home?next=' . uri_string());
        if (!$id)
            return $this->show_404();

        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Budgetbid_model', 'BBid');
        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->model('User_model', 'User');
        $this->load->library('ObjectFormatter', '', 'formatter');

        $id_usr = $this->user->id;
        $item = $this->AItem->getBy('id', $id);
        if (!$item)
            return $this->show_404();

        $bid = $this->IBid->getByCond(['auction_item' => $id],1);
        $budget_bid = $this->BBid->getByCond(['auction_item' => $id, 'user' => (object)['!=', $id_usr]]);

        $this->AItem->set($id, ['current_price' => $bid->bid_price]);

        if ($budget_bid->budget < $bid->bid_price) {

            $cond = array(
                'auction_item' => $id
            );

            $bid = $this->IBid->getByCond($cond, 1, false, ['bid_price' => 'DESC']);
            if ($bid)
                $bids = $this->formatter->item_bid($bid);

            $guser = $this->IBid->getusermail($cond);
            if ($guser)
                $gusers = $this->formatter->item_bid2($guser);
           
            $list = array();
            foreach ($gusers as $key => $value) {
                $list[] = $value->user->email;
            }



            $mess = 'Hi, your auction item has been defeated, please visit http://gadingkoiauction.com';

            $this->load->library('email');
            $this->email->set_newline("\r\n");

            $email = $list;
            $this->email->from('gadingkoiauction@gmail.com', 'Auction notice');
            $this->email->to($email);

            $this->email->subject('Notification');
            $this->email->message($mess);

            $this->email->send();
        }
    }

    public function input_budget($id=null){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();

        $this->load->model('Budgetbid_model', 'BBid');
        $this->load->model('Auctionitem_model', 'AItem');

        $item = $this->AItem->getBy('id', $id);
        if(!$item)
            return $this->show_404();

        $id_usr = $this->user->id;

        $data = array(
            'user' => $this->user->id,
            'budget' => $this->input->post('budget'),
            'auction_item' => $id,
            'created' => date('Y-m-d H:i:s')
            );
        $this->BBid->insert_budget($data);
    }

    public function extratime($id=null){
        if(!$this->user)
            return $this->redirect('/home?next='.uri_string());
        if(!$id)
            return $this->show_404();

        $this->load->model('Auctionitem_model', 'AItem');
        $this->load->model('Itembid_model', 'IBid');
        $this->load->library('ObjectFormatter', '', 'formatter');

        $item = $this->AItem->getBy('id', $id);
        if(!$item)
            return $this->show_404();

        $bid_current = $this->IBid->getByCond(['auction_item'=>$id],1,false, ['bid_price'=>'DESC']);
        if ($bid_current)
            $params['bids_current'] = $this->formatter->item_bid($bid_current, false, ['user']);

        /*$time1 = 600;
        $time2 = strtotime($item->time_end);
        $hasil = $time2+$time1;
        $newendt = date('Y-m-d H:i:s',$hasil);
        $this->AItem->set($id, ['time_end'=>$newendt]);*/
        $timend = strtotime($item->time_end);
        echo $timend;
    }

    function new(){
        $this->load->model('Itembid_model', 'IBid');
        $this->load->model('Auctionitem_model', 'AItem');

        $item = $this->AItem->getByCond(false, false, false);
        foreach($item as $itm){
            $bid['bids'] = $this->IBid->getByCond(['auction_item'=>$itm->id], false, false);
        }
        $bid['bids'] = $this->IBid->getByCond(['auction_item'=>2], 2, false, ['bid_price'=>'ASC']);
        
        $this->respond('new', $bid);
    }
}
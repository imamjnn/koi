<?php

if (!defined('BASEPATH'))
    die;

class Register extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Register_model');
        $this->load->model('User_model', 'User');
    }

    private function _schemaHome() {
        $schemas = array();

        $data = array(
            '@context' => 'http://schema.org',
            '@type' => 'Organization',
            'name' => $this->setting->item('site_name'),
            'url' => base_url(),
            'logo' => $this->theme->asset('/static/image/logo/logo.png')
        );

        // social url
        $socials = array();
        $known_socials = array(
            'facebook',
            'gplus',
            'instagram',
            'linkedin',
            'myspace',
            'pinterest',
            'soundcloud',
            'tumblr',
            'twitter',
            'youtube'
        );

        foreach ($known_socials as $soc) {
            $url = $this->setting->item('site_x_social_' . $soc);
            if ($url)
                $socials[] = $url;
        }

        if ($socials)
            $data['sameAs'] = $socials;

        // phone contact number
        $contacts = array();
        $known_contacts = array(
            'baggage_tracking' => 'baggage tracking',
            'bill_payment' => 'bill payment',
            'billing_support' => 'billing support',
            'credit_card_support' => 'credit card support',
            'customer_support' => 'customer support',
            'emergency' => 'emergency',
            'package_tracking' => 'package tracking',
            'reservations' => 'reservations',
            'roadside_assistance' => 'roadside assistance',
            'sales' => 'sales',
            'technical_support' => 'technical support'
        );
        $contact_served = $this->setting->item('organization_contact_area_served');
        if ($contact_served) {
            $contact_served = explode(',', $contact_served);
            if (count($contact_served) == 1)
                $contact_served = $contact_served[0];
        }

        $contact_language = $this->setting->item('organization_contact_available_language');
        if ($contact_language) {
            $contact_language = explode(',', $contact_language);
            if (count($contact_language) == 1)
                $contact_language = $contact_language[0];
        }

        $contact_options = array();
        if ($this->setting->item('organization_contact_opt_tollfree'))
            $contact_options[] = 'TollFree';
        if ($this->setting->item('organization_contact_opt_his'))
            $contact_options[] = 'HearingImpairedSupported';

        foreach ($known_contacts as $cont => $name) {
            $phone = $this->setting->item('organization_contact_' . $cont);
            if (!$phone)
                continue;
            $contact = array(
                '@type' => 'ContactPoint',
                'telephone' => $phone,
                'contactType' => $name
            );
            if ($contact_served)
                $contact['areaServed'] = $contact_served;
            if ($contact_language)
                $contact['availableLanguage'] = $contact_language;
            if ($contact_options)
                $contact['contactOption'] = $contact_options;
            $contacts[] = $contact;
        }

        if ($contacts)
            $data['contactPoint'] = $contacts;

        $schemas[] = $data;

        return $schemas;
    }

    public function index() {
        if (!is_dev())
            $this->output->cache((60 * 60 * 15));

        $params = array(
            'events' => array(),
            'banners' => array(),
            'suppliers' => array(),
            'home' => (object) array(
                'schema' => $this->_schemaHome()
            )
        );

        $this->load->model('Banner_model', 'Banner');
        $this->load->model('Supplier_model', 'Supplier');
        $this->load->model('Event_model', 'Event');
        $this->load->library('ObjectFormatter', '', 'formatter');
    $this->load->library('Recaptcha');

        $cond = array();

        $rpp = 5;
        $page = $this->input->get('page');
        if (!$page)
            $page = 1;

        $event = $this->Event->getByCond($cond, $rpp, $page);
        if ($event)
            $params['events'] = $this->formatter->event($event, false, true);


        $banner = $this->Banner->getByCond($cond, $rpp, $page);
        if ($banner)
            $params['banners'] = $this->formatter->banner($banner, false, true);

        $rpp = 4;

        $page = $this->input->get('page');
        if (!$page)
            $page = 1;

        $supplier = $this->Supplier->getByCond($cond, $rpp, $page);
        if ($supplier)
            $params['suppliers'] = $this->formatter->supplier($supplier, false, true);

        $this->respond('register', $params);
    }

    public function submit() {
        // Load the library
        $this->load->library('recaptcha');
        //passing post data dari view
    
    // Catch the user's answer
        $captcha_answer = $this->input->post('g-recaptcha-response');

    // Verify user's answer
        $response = $this->recaptcha->verifyResponse($captcha_answer);

    if ($response['success']) {
        $this->load->helper(array('form', 'url'));
        $nama = $this->input->post('name');
        $fullname = $this->input->post('fullname');
        $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
        $email = $this->input->post('email');
        $avatar = base_url('/theme/default/static/img/elements/user.png'); 

        //memasukan ke array
        $data = array(
            'password' => $password,
            'name' => $nama,
            'fullname' => $fullname,
            'email' => $email,
            'avatar' => $avatar
        );
        //tambahkan akun ke database
        $this->load->model('Register_model');
        $this->Register_model->add_account($data);
                
                
        $this->load->library('email');
        $config = array();
        $config['charset'] = 'utf-8';
        $config['useragent'] = 'Codeigniter';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['protocol'] = "smtp";
        $config['mailtype'] = "html";
        $config['smtp_host'] = "ssl://smtp.gmail.com"; //pengaturan smtp
        $config['smtp_port'] = "465";
        $config['smtp_timeout'] = "400";
        $config['smtp_user'] = "gadingkoiauction@gmail.com"; // isi dengan email kamu
        $config['smtp_pass'] = "koiganteng"; // isi dengan password kamu
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['starttls'] = TRUE;
        $config['wordwrap'] = TRUE;
    //memanggil library email dan set konfigurasi untuk pengiriman email

        $this->email->initialize($config);
    //konfigurasi pengiriman
        $this->email->from($config['smtp_user']);
        $this->email->to($email);
        $this->email->subject("Verification Account");

        $this->email->message(
                "Thank you for registering, to verify please click the link below<br>" .
                site_url("register/verify/" . md5($email))
        );

        if ($this->email->send()) {
            echo "Berhasil melakukan registrasi, silahkan cek email kamu";
        } else {
            echo "Berhasil melakukan registrasi, namu gagal mengirim verifikasi email";
        }
        echo $this->email->print_debugger();
                
    } else {
            // Something goes wrong
         var_dump($response);
//            echo'captcha tidak boleh kosong';
        }
        
    }

    function verify($hash = NULL) {
        $this->load->helper('url');
        $this->load->model('Register_model');
        $this->Register_model->verifyEmailID($hash);
        echo "<div style='color: red; width: 100%; text-align: center; margin: 50px auto 0; font-weight: bold; font-size: 25px;'>Congratulations, you've verified your account.</div>";
        echo "<div style='color: red; width: 100%; text-align: center; font-weight: bold; font-size: 25px;'><br><a href='" . base_url("Home") . "'>Back to menu login</a></div>";
    }

    function cekmail() {
        $email = $this->input->post('email');
        
        $data = array(
            'email' => $email
        );
        
        $this->load->model('Register_model');
        $ada = $this->Register_model->cekemail($email);
        
        header('Content-Type: application/json');
        
        if($ada)
            echo json_encode(false);
        else
            echo json_encode(true);
    }

    function edit($id = null) {

        if (!is_dev())
            $this->output->cache((60 * 60 * 15));

        $this->load->helper(array('form', 'url'));

        $params = array(
            'events' => array(),
            'banners' => array(),
            'users' => array(),
            'suppliers' => array(),
            'home' => (object) array(
                'schema' => $this->_schemaHome()
            )
        );

        $this->load->model('Banner_model', 'Banner');
        $this->load->library('ObjectFormatter', '', 'formatter');
        $this->load->model('User_model', 'User');
        $this->load->library('SiteForm', '', 'form');

        $cond = array();

        $rpp = 5;
        $page = $this->input->get('page');
        if (!$page)
            $page = 1;

        $rpp = 100;
        $banner = $this->Banner->getByCond($cond, $rpp, $page);
        if ($banner)
            $params['banners'] = $this->formatter->banner($banner, false, true);

        $result = $this->User->get($id);


        if ($result)
            $params['user'] = $this->formatter->user($result, false, true);

        $this->respond('edit', $params);
    }

    public function update() {
        //passing post data dari view
        $this->load->helper(array('form', 'url'));
        $id = $this->input->post('idx');
        $nama = $this->input->post('name');
        $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
        $email = $this->input->post('email');

        //memasukan ke array
        $data = array(
            'id' => $id,
            'password' => $password,
            'name' => $nama,
            'email' => $email
        );
        //tambahkan akun ke database
        $this->Register_model->edit_account($data);

        redirect('Home', 'refresh');
    }

    public function verification($key) {
        $this->load->helper('url');
        $this->load->model('Register_model');
        $this->Register_model->changeActiveState($key);
        echo "Selamat kamu telah memverifikasi akun kamu";
        echo "<br><br><a href='" . site_url("Register") . "'>Kembali ke Menu Login</a>";
    }

    function mydata($id = null) {

        if (!is_dev())
            $this->output->cache((60 * 60 * 15));

        $this->load->helper(array('form', 'url'));

        $params = array(
            'events' => array(),
            'banners' => array(),
            'users' => array(),
            'suppliers' => array(),
            'home' => (object) array(
                'schema' => $this->_schemaHome()
            )
        );

        $this->load->model('Banner_model', 'Banner');
        $this->load->library('ObjectFormatter', '', 'formatter');
        $this->load->model('User_model', 'User');
        $this->load->library('SiteForm', '', 'form');

        $cond = array();

        $rpp = 5;
        $page = $this->input->get('page');
        if (!$page)
            $page = 1;

        $rpp = 100;
        $banner = $this->Banner->getByCond($cond, $rpp, $page);
        if ($banner)
            $params['banners'] = $this->formatter->banner($banner, false, true);

        $result = $this->User->get($id);


        if ($result)
            $params['user'] = $this->formatter->user($result, false, true);

        $this->respond('mydata', $params);
    }

    public function updatemydata() {
        //passing post data dari view
        $this->load->library('MediaFile', '', 'media');
        $this->load->library('ObjectFormatter', '', 'formatter');
        $this->load->library('SiteForm', '', 'form');


        $this->load->helper(array('form', 'url'));
        $id = $this->input->post('idy');
        $nama = $this->input->post('name');
        $fullname = $this->input->post('fullname');
        $email = $this->input->post('email');

        $file = $_FILES['avatar'];
        $avatar = $this->media->processUpload('avatar', $file['name'], 'mydata.avatar', $this->user->id);

        $avatar = $avatar['local_media_file'];


        $this->load->library('upload');


        //memasukan ke array
        $data = array(
            'id' => $id,
            'name' => $nama,
            'fullname' => $fullname,
            'email' => $email,
            'avatar' => $avatar
        );
        //tambahkan akun ke database
        $this->Register_model->updatemydata_account($data);

        redirect('Register/mydata', 'refresh');
    }

}
<?php

if (!defined('BASEPATH'))
    die;

class Verifymember extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function logout() {
        if (!$this->user)
            return $this->redirect('/');

        $cookie_name = config_item('sess_cookie_name');
        $this->input->set_cookie($cookie_name, '', -15000);

        $this->USession->remove($this->session->id);
        $this->event->me->logged_out($this->user);
        $this->redirect('home');
    }

}

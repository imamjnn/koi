<?php

if(!defined('BASEPATH'))
    die;

/**
 * The model of table `banner`
 */
class Budgetbid_model extends MY_Model
{
    /**
     * Table name
     * @var string
     */
    public $table = 'budget_bid';

    /**
     * Constructor
     */
    function __construct(){
        $this->load->database();
        parent::__construct();
    }

    function insert_budget($data){
        $this->db->insert('budget_bid', $data);
    }

    function delBudget($cond, $table){
        $this->db->where($cond);
        $this->db->delete($table);
    }
}
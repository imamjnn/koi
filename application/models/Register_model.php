<?php

class Register_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function add_account($data) {
        $this->load->database();
        $this->db->insert('user', $data);
        return TRUE;
    }

    function edit_account($data) {
        $this->load->database();
        $this->db->where('id', $this->input->post('idx'));
        $this->db->update('user', $data);
    }

    function updatemydata_account($data) {
        $this->load->database();
        $this->db->where('id', $this->input->post('idy'));
        $this->db->update('user', $data);
    }

    function verifyEmailID($key) {
        $data = array('status' => 3);
        $this->db->where('md5(email)', $key);
        return $this->db->update('user', $data);
    }

    function cekemail($email) {
        $this->load->database();
        $this->db->where("email", $email);
        $query = $this->db->get("user");
        return $query->result_array();
    }

}
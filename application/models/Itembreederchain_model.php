<?php

if(!defined('BASEPATH'))
    die;

/**
 * The model of table `fish_variety_chain`
 */
class Itembreederchain_model extends MY_Model
{
    /**
     * Table name
     * @var string
     */
    public $table = 'item_breeder_chain';

    /**
     * Constructor
     */
    function __construct(){
        $this->load->database();
        parent::__construct();
    }
}
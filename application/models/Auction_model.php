<?php

if(!defined('BASEPATH'))
    die;

/**
 * The model of table `banner`
 */
class Auction_model extends MY_Model
{
    /**
     * Table name
     * @var string
     */
    public $table = 'auction';

    /**
     * Constructor
     */
    function __construct(){
        $this->load->database();
        parent::__construct();
    }
}
<?php

if(!defined('BASEPATH'))
    die;

/**
 * The model of table `item_variety`
 */
class Itemvariety_model extends MY_Model
{
    /**
     * Table name
     * @var string
     */
    public $table = 'item_variety';

    /**
     * Constructor
     */
    function __construct(){
        $this->load->database();
        parent::__construct();
    }

   
}
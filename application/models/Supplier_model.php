<?php

if(!defined('BASEPATH'))
    die;

/**
 * The model of table `banner`
 */
class Supplier_model extends MY_Model
{
    /**
     * Table name
     * @var string
     */
    public $table = 'supplier';

    /**
     * Constructor
     */
    function __construct(){
        $this->load->database();
        parent::__construct();
    }
}
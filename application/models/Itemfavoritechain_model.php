<?php

if(!defined('BASEPATH'))
    die;

/**
 * The model of table `fish_variety_chain`
 */
class Itemfavoritechain_model extends MY_Model
{
    /**
     * Table name
     * @var string
     */
    public $table = 'item_favorite_chain';

    /**
     * Constructor
     */
    function __construct(){
        $this->load->database();
        parent::__construct();
    }

    function delFavC($id, $table){
        $this->db->where($id);
        $this->db->delete($table);
    }
}
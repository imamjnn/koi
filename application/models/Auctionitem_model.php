<?php

if(!defined('BASEPATH'))
    die;

/**
 * The model of table `Auction Item`
 */
class Auctionitem_model extends MY_Model
{
    /**
     * Table name
     * @var string
     */
    public $table = 'auction_item';

    /**
     * Constructor
     */
    function __construct(){
        $this->load->database();
        parent::__construct();
    }

     /**
     * Filter item by random condition
     * @param array cond The condition.
     * @param integer total Result per page.
     * @param integer page Page number.
     * @param array order The order condition.
     */
    public function findByCond($cond, $rpp=12, $page=1, $order=['id'=>'DESC']){
        $auction_item = $this->table;
        
        $this->db->select("$auction_item.*");
        
        if(array_key_exists('q', $cond)){
            $this->db->like("$auction_item.name", $cond['q']);
            unset($cond['q']);
            
        }
        
        
        if(array_key_exists('item_variety', $cond)){
            
            $this->load->model('Itemvarietychain_model', 'IVChain');
            $item_variety = $this->IVChain->table;
            $this->db->join($item_variety, "$item_variety.auction_item = $auction_item.id", 'LEFT');
            
            $method = is_array($cond['item_variety']) ? 'where_in' : 'where';
            $this->db->$method("$item_variety.item_variety", $cond['item_variety']);
            $this->db->group_by("$item_variety.auction_item");
            unset($cond['item_variety']);
        }
        
        
        if(array_key_exists('item_breeder', $cond)){
            
            $this->load->model('Itembreederchain_model', 'IBChain');
            $item_breeder = $this->IBChain->table;
            $this->db->join($item_breeder, "$item_breeder.auction_item = $auction_item.id", 'LEFT');
            
            $method = is_array($cond['item_breeder']) ? 'where_in' : 'where';
            $this->db->$method("$item_breeder.item_breeder", $cond['item_breeder']);
            $this->db->group_by("$item_breeder.auction_item");
            unset($cond['item_breeder']);
        }
        
        
//        if(array_key_exists('item', $cond)){
//            $this->load->model('Itemchain_model', 'FChain');
//            $item_item = $this->FChain->table;
//            $this->db->join($item_item, "$item_item.item = $auction_item.id", 'LEFT');
//            
//            $method = is_array($cond['item']) ? 'where_in' : 'where';
//            $this->db->$method("$item_item.item", $cond['item']);
//            $this->db->group_by("$item_item.item");
//            unset($cond['item']);
//        }
//        deb($cond);
        
        return $this->getByCond($cond, $rpp, $page, $order);
    }
    
    /**
     * Count total by random condition
     * @param array cond The condition.
     */
    public function findByCondTotal($cond){
        $auction_item = $this->table;
        
        if(array_key_exists('q', $cond)){
            $this->db->like($auction_item . '.title', $cond['q']);
            unset($cond['q']);
        }
        
        if(array_key_exists('item_variety', $cond)){
            $this->load->model('Itemvarietychain_model', 'IVChain');
            $item_variety = $this->IVChain->table;
            $this->db->join($item_variety, "$item_variety.auction_item = $auction_item.id", 'LEFT');
            
            $method = is_array($cond['item_variety']) ? 'where_in' : 'where';
            $this->db->$method("$item_variety.item_variety", $cond['item_variety']);
            $this->db->group_by("$item_variety.auction_item");
            unset($cond['item_variety']);
        }
        
        
    

        if(array_key_exists('item_breeder', $cond)){
            $this->load->model('Itembreederchain_model', 'IBChain');
            $item_breeder = $this->IBChain->table;
            $this->db->join($item_breeder, "$item_breeder.auction_item = $auction_item.id", 'LEFT');
            
            $method = is_array($cond['item_breeder']) ? 'where_in' : 'where';
            $this->db->$method("$item_breeder.item_breeder", $cond['item_breeder']);
            $this->db->group_by("$item_breeder.auction_item");
            unset($cond['item_breeder']);
        }

        if(array_key_exists('item', $cond)){
            $this->load->model('Itemchain_model', 'FChain');
            $item = $this->FChain->table;
            $this->db->join($item, "$item.item = $item.id", 'LEFT');
            
            $method = is_array($cond['item']) ? 'where_in' : 'where';
            $this->db->$method("$item.item", $cond['item']);
            unset($cond['item']);
        }
        
        return $this->countByCond($cond);
    }

}

<?php

if(!defined('BASEPATH'))
    die;

/**
 * The model of table `gallery`
 */
class Itemfavorite_model extends MY_Model
{
    /**
     * Table name
     * @var string
     */
    public $table = 'item_favorite';

    /**
     * Constructor
     */
    function __construct(){
        $this->load->database();
        parent::__construct();
    }

    function delFav($id, $table){
        $this->db->where($id);
        $this->db->delete($table);
    }

}
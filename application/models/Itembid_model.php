<?php

if(!defined('BASEPATH'))
    die;

/**
 * The model of table `banner`
 */
class Itembid_model extends MY_Model
{
    /**
     * Table name
     * @var string
     */
    public $table = 'item_bid';

    /**
     * Constructor
     */
    function __construct(){
        $this->load->database();
        parent::__construct();
    }

    function input_bid($data){
        $this->db->set($data);
        $this->db->insert($this->db->dbprefix .'item_bid');
    }

    function delBid($cond, $table){
        $this->db->where($cond);
        $this->db->delete($table);
    }

    function setBid($cond, $setbid){
        $this->db->where($cond);
        $this->db->set($setbid);
        $this->db->update('item_bid');
    }

    function getUser($cond){
        $this->db->select('user');
        $this->db->group_by('user'); 
        $this->db->where($cond);
        $rows =$this->db->get('item_bid');
        return $rows->result();
    }

    function getItem($cond){
        $this->db->select('item_bid.user, auction_item.name, auction_item.id, auction_item.time_end');
        $this->db->join('auction_item', 'item_bid.auction_item=auction_item.id'); 
        $this->db->group_by('item_bid.user, auction_item.name, auction_item.id, auction_item.time_end');
        $this->db->where($cond);
        $rows =$this->db->get('item_bid');
        return $rows->result();
    }

    function getItemGroup($cond, $grup){
        $this->db->distinct();
        $this->db->where($cond);
        $rows =$this->db->get('item_bid');
        return $rows->result();
    }

    function getusermail($cond){
        $this->db->select('user');
        $this->db->order_by('id', 'DESC');
        $this->db->where($cond);      
        $this->db->limit(1,1);
//        $this->db->offset(1);
        $query = $this->db->get('item_bid');
        return $query->result();
//        $rows = $this->db->get('item_bid');
//        return $rows->result();
    }

    // new bid version
    function insert_bid($data){
        $this->db->insert('item_bid', $data);
    }

    /**
     * Find user by name
     * @param array condition, where 'q' key is used to match name or fullname.
     * @param integer rpp, result per page.
     * @param integer page, The page number.
     * @param array order, The order.
     */
    public function findByName($cond, $query, $rpp=12, $page=1, $order=['id'=>'DESC']){
        if($query){
            $this->db->group_start();
            $this->db->like('bid_price', $query);
            $this->db->or_like('time', $query);
            $this->db->group_end();
        }
        
        return $this->getByCond($cond, $rpp, $page, $order);
    }
    
    /**
     * Find total result by condition.
     * @param array cond The condition.
     */
    public function findByNameTotal($cond, $query){
        if($query){
            $this->db->group_start();
            $this->db->like('bid_price', $query);
            $this->db->or_like('time', $query);
            $this->db->group_end();
        }
        
        return $this->countByCond($cond);
    }
}
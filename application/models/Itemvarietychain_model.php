<?php

if(!defined('BASEPATH'))
    die;

/**
 * The model of table `fish_variety_chain`
 */
class Itemvarietychain_model extends MY_Model
{
    /**
     * Table name
     * @var string
     */
    public $table = 'item_variety_chain';

    /**
     * Constructor
     */
    function __construct(){
        $this->load->database();
        parent::__construct();
    }
}
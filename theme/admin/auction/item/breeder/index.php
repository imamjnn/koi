<!DOCTYPE html>
<html lang="en-US">
<head>
    <?= $this->theme->file('head') ?>
</head>
<body>
    <?= $this->theme->file('header') ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <?php if(ci()->can_i('create-item_breeder')): ?>
                    <a class="btn btn-primary pull-right" href="<?= base_url('/admin/auction/item/breeder/0') ?>"><?= _l('Create New') ?></a>
                    <?php endif; ?>
                    <h1><?= $title ?></h1>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <form method="get">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="search" name="q" placeholder="Find breeder" class="form-control" autofocus="autofocus" value="<?= $this->input->get('q') ?>">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </form>                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <?php foreach($breeders as $breeder): ?>
                            <div class="col-md-4">
                                <div class="list-group">
                                    <a href="<?= base_url('admin/auction/item/breeder/' . $breeder->id) ?>" class="list-group-item">
                                        <h4 class="list-group-item-heading"><?= $breeder->fullname ?></h4>
                                        <div class="list-group-item-text">
                                            <div class="text-ellipsis"><?= $breeder->phone ?></div>
                                            <div class="text-ellipsis"><?= $breeder->address ?></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                        
                        <?php if($pagination): ?>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <ul class="pagination">
                                <?php foreach($pagination as $label => $link): ?>
                                    <?php $active = $link == '#'; ?>
                                    <li<?= ($active?' class="disabled"':'') ?>><a<?= (!$active?' href="' . $link . '"':'') ?>><?= $label ?></a></li>
                                <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
    <?= $this->theme->file('foot') ?>
</body>
</html>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <?= $this->theme->file('head') ?>
</head>
<body>
    <?= $this->theme->file('header') ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1><?= $title ?></h1>
                </div>                
                <form class="row" method="post">
                    <div class="col-md-12">
                        
                        <div class="row">
                            <div class="col-md-4">
                                <?= $this->form->field('avatar') ?>
                                
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= $this->form->field('fullname') ?>
                                        <?= $this->form->field('about') ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $this->form->field('address') ?>
                                        <?= $this->form->field('email') ?>
                                        <?= $this->form->field('phone') ?>
                                    </div>
                                </div>
                            </div>
                                
                        </div>
                        
                        <div class="row">
                            <div class="col-md-5">
                                <?php if(ci()->can_i('delete-item_breeder') && property_exists($breeder, 'id')): ?>
                                <a href="<?= base_url('/admin/auction/item/breeder/' . $breeder->id . '/remove') ?>" class="btn btn-danger btn-confirm" data-title="<?= _l('Delete Confirmation') ?>" data-confirm="<?= hs(_l('Are you sure want to delete this breeder permanently?')) ?>"><?= _l('Delete') ?></a>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-7 text-right">
                                <div class="form-group">
                                    <a href="<?= base_url('/admin/auction/item/breeder') ?>" class="btn btn-default"><?= _l('Cancel') ?></a>
                                    <button class="btn btn-primary"><?= _l('Save') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <?= $this->theme->file('foot') ?>
    <?= $this->form->focusInput(); ?>
</body>
</html>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <?= $this->theme->file('head') ?>
</head>
<body>
    <?= $this->theme->file('header') ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <?php if(ci()->can_i('create-auction_item')): ?>
                    <a class="btn btn-primary pull-right" href="<?= base_url('/admin/auction/' . $auction->id . '/item/0') ?>"><?= _l('Create Item') ?></a>
                    <?php endif; ?>
                    <h1><?= $title ?></h1>
                </div>
                
                <div class="row">
                    <div class="col-md-3">
                        <fieldset>
                            <legend><?= _l('List Room') ?></legend>
                            
                            <div class="list-group">
                                <?php foreach($auctions as $auc): ?>
                                <a href="<?= base_url('/admin/auction/' . $auc->id . '/item') ?>" class="list-group-item<?= ( $auc->id == $auction->id ? ' active' : '' ) ?>">
                                    <?= $auc->name ?>
                                </a>
                                <?php endforeach; ?>
                            </div>
                            <div class="list-group">
                                <a href="<?= base_url('/admin/auction') ?>" class="list-group-item"><?= _l('See All') ?></a>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    Total items: <?= number_format($total, 0, '.', '.'); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-9">
                        <?php $can_see_auction_item = ci()->can_i('update-auction_item'); ?>
                        <?php foreach($items as $itm): ?>
                        <div class="col-md-4">
                            <div class="thumbnail<?= ( $itm->time_end->time < time() ? ' list-group-item-danger' : '' ) ?>">
                            <?php if($can_see_auction_item): ?>
                                <a href="<?= base_url('/admin/auction/' . $auction->id .'/item/' . $itm->id ) ?>" class="btn btn-default btn-xs thumbnail-closer"><i class="glyphicon glyphicon-pencil"></i></a> 
                            <?php endif; ?>
                            <img src="<?= $itm->photo->_253x142 ?>" alt="<?= $itm->name ?>" class="img-responsive"></a>    
                            <h4><?= $itm->name ?></h4>
                            <p><?= $itm->time_end->format('d M Y | H:i') ?> <a href="<?= base_url('admin/auction/history/show/'. $itm->id) ?>">view history</a></p>
                            
                            </div>
                        </div>
                        <?php endforeach; ?>

                        <?php if($pagination): ?>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <ul class="pagination">
                                <?php foreach($pagination as $label => $link): ?>
                                    <?php $active = $link == '#'; ?>
                                    <li<?= ($active?' class="disabled"':'') ?>><a<?= (!$active?' href="' . $link . '"':'') ?>><?= $label ?></a></li>
                                <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?= $this->theme->file('foot') ?>
</body>
</html>
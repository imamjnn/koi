<!DOCTYPE html>
<html lang="en-US">
<head>
    <?= $this->theme->file('head') ?>
</head>
<body>
    <?= $this->theme->file('header') ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <?php if(ci()->can_i('create-item_variety')): ?>
                    <a class="btn btn-primary pull-right" href="<?= base_url('/admin/auction/item/variety/0') ?>"><?= _l('Create New') ?></a>
                    <?php endif; ?>
                    <h1><?= $title ?></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach($varieties as $variety): ?>
            <div class="col-md-4">
                <div class="list-group">
                    <a href="<?= base_url('admin/auction/item/variety/' . $variety->id) ?>" class="list-group-item">
                        <h4 class="list-group-item-heading"><?= $variety->name ?></h4>
                        <div class="list-group-item-text">
                            <div class="text-ellipsis"><?= $variety->description ?></div>
                        </div>
                    </a>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
    
    <?= $this->theme->file('foot') ?>
</body>
</html>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <?= $this->theme->file('head') ?>
</head>
<body>
    <?= $this->theme->file('header') ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1><?= $title ?> - <?= $auctions->name ?></h1>
                </div>
                
                <form class="row" method="post">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <?= $this->form->field('name') ?>
                                <?= $this->form->field('size') ?>
                                <?= $this->form->field('age') ?>
                                <?= $this->form->field('gender', $genders) ?>
                                <?= $this->form->field('price') ?>
                                <?= $this->form->field('current_price') ?>
                            </div>
                            <div class="col-md-4">
                                <?= $this->form->field('description') ?>
                                <?php if(ci()->can_i('read-item_variety')): ?>
                                <?= $this->form->field('variety', $varieties) ?>
                                <?php endif; ?> 
                                <?php if(ci()->can_i('read-item_breeder')): ?>
                                <?= $this->form->field('breeder', $breeders) ?>
                                <?php endif; ?> 
                                <?= $this->form->field('certificate') ?>                           
                            </div>
                            <div class="col-md-4">
                                <?= $this->form->field('photo') ?>
                                <?= $this->form->field('video') ?> 
                                                           
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-5">
                               <?php if(ci()->can_i('delete-auction_item') && property_exists($auction_item, 'id')): ?>
                                    <a href="<?= base_url('/admin/auction/' . $auctions->id . '/item/' . $auction_item->id . '/remove') ?>" class="btn btn-danger btn-confirm" data-title="<?= _l('Delete Confirmation') ?>" data-confirm="<?= hs(_l('Are you sure want to delete this items permanently?')) ?>"><?= _l('Delete') ?></a>
                                    <?php endif; ?>
                            </div>
                            <div class="col-md-7 text-right">
                                <div class="form-group">
                                    <a href="<?= base_url('/admin/auction/' . $auctions->id. '/item') ?>" class="btn btn-default"><?= _l('Cancel') ?></a>
                                    <button class="btn btn-primary"><?= _l('Save') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <?= $this->theme->file('foot') ?>
    <?= $this->form->focusInput(); ?>
    
</body>
</html>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <?= $this->theme->file('head') ?>
</head>
<body>
    <?= $this->theme->file('header') ?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <?php if(ci()->can_i('create-auction')): ?>
                    <a class="btn btn-primary pull-right" href="<?= base_url('/admin/auction/0') ?>"><?= _l('Create New') ?></a>
                    <?php endif; ?>
                    <h1><?= $title ?></h1>
                </div>
                
                <div class="row">
                    <?php if($auctions): ?>
                        <?php foreach($auctions as $auc): ?>
                        <div class="col-md-6">
                            <div class="list-group">
                                <a class="list-group-item<?= ( $auc->time_end->time < time() ? ' list-group-item-danger' : '' ) ?>" href="<?= base_url('/admin/auction/' . $auc->id . '/item') ?>">
                                    <h4 class="list-group-item-heading"><?= $auc->name ?></h4>
                                    <p class="list-group-item-text"><b>Start time: <?= $auc->time_start->format('d M Y H:i') ?> - <?= $auc->time_end->format('d M Y H:i') ?></b></p>
                                    <p class="list-group-item-text"><?= base_url($auc->page) ?></p>
                                </a>
                                <div class="list-group-closer">
                                    <a href="<?= base_url('/admin/auction/' . $auc->id) ?>" class="btn btn-default btn-xs">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a>
                                    <?php if($auc->status->value == 2): ?>
                                    <span class="label label-default">draft</span>
                                   
                                    <?php elseif($auc->status->value == 3): ?>
                                    <span class="label label-info">
                                        <i class="glyphicon glyphicon-time"></i>
                                        <?= $auc->published->format('M d, H:i'); ?>
                                    </span>
                                    <?php else: ?>
                                    <a href="<?= base_url($auc->page) ?>" class="btn btn-default btn-xs">
                                        <i class="glyphicon glyphicon-new-window"></i>
                                    </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    
    <?= $this->theme->file('foot') ?>
</body>
</html>
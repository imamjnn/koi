<!DOCTYPE html>
<html lang="en-US">
<head>
    <?= $this->theme->file('head') ?>
</head>
<body>
    <?= $this->theme->file('header') ?>

   <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1><?= $title ?></h1>
                </div>
                
                <form class="row" method="post">
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $this->form->field('name') ?>
                                <?= $this->form->field('description') ?>
                                <?= $this->form->field('slug') ?>
                            </div>
                            <div class="col-md-6">
                                
                                <?= $this->form->field('time_start') ?>
                                <?= $this->form->field('time_end') ?>
                                <?= $this->form->field('time_distance') ?>
                                <?= $this->form->field('status', $statuses) ?>
                                <div id="auction-published-schedule-date">
                                <?= (ci()->can_i('create-auction_published') ? $this->form->field('published') : '') ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-5">
                                <?php if(ci()->can_i('delete-auction') && property_exists($auction, 'id')): ?>
                                <a href="<?= base_url('/admin/auction/' . $auction->id . '/remove') ?>" class="btn btn-danger btn-confirm" data-title="<?= _l('Delete Confirmation') ?>" data-confirm="<?= hs(_l('Are you sure want to delete this auction permanently?')) ?>"><?= _l('Delete') ?></a>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-7 text-right">
                                <div class="form-group">
                                    <a href="<?= base_url('/admin/auction') ?>" class="btn btn-default"><?= _l('Cancel') ?></a>
                                    <button class="btn btn-primary"><?= _l('Save') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>   
    
    <?= $this->theme->file('foot') ?>
    <?= $this->form->focusInput(); ?>

    <script type="text/javascript">
        $('#field-status').change(function(){
            var val = $(this).val();
            $('#auction-published-schedule-date')[(val==3?'show':'hide')]();
        }).change();
    </script>

</body>
</html>
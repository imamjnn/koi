<!DOCTYPE html>
<html lang="en-US">
<head>
    <?= $this->theme->file('head') ?>
</head>
<style type="text/css">
	.thumbnail {
    padding:0px;
}
.panel {
	position:relative;
}
.panel>.panel-heading:after,.panel>.panel-heading:before{
	position:absolute;
	top:11px;left:-16px;
	right:100%;
	width:0;
	height:0;
	display:block;
	content:" ";
	border-color:transparent;
	border-style:solid solid outset;
	pointer-events:none;
}
.panel>.panel-heading:after{
	border-width:7px;
	border-right-color:#f7f7f7;
	margin-top:1px;
	margin-left:2px;
}
.panel>.panel-heading:before{
	border-right-color:#ddd;
	border-width:8px;
}
</style>
<body>
    <?= $this->theme->file('header') ?>

    <div class="container">
        <div class="row">
  		<?php if($items): ?>
  		<?php foreach($items as $item): ?>
		<div class="col-sm-1">
			<div class="thumbnail"><img class="img-responsive user-photo" src="<?= $item->photo->_70x70 ?>"></div>
		</div>
		<div class="col-sm-7">
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="pull-right">History Bid</i>
					<strong><?= $item->name ?></strong> 
				</div>
		<?php endforeach; ?>
		<?php endif; ?>

				<?php if($bids): ?>
  				<?php foreach($bids as $bid): ?>
				<div class="panel-body">
					Rp <?= number_format($bid->bid_price,0,',','.') ?> |
					<?= $bid->time->format('d M Y H:i') ?> |
					By <a href="<?= base_url('admin/user/'.$bid->user->id) ?>"><?= $bid->user->fullname ?></a>
				</div>
				<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>
	
		
	</div>
            
    </div>
    
    <?= $this->theme->file('foot') ?>
</body>
</html>
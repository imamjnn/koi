
 <!-- bidding chat area -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="label-chat-heading" id="top-bid">
                        <span class="hidden-text">
                            Highest Bid : 
                        </span> 
                    </div>

                    <div class="chat-heading pull-right">
                    <span class="hidden-text">Rem. Time</span> <b id="countdown"></b></div>

                </div>
                <div class="panel-body">
                    <ul class="chat">
                        <div id="jnn"></div>
                        <div id="show"></div>

                    </ul>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-9">
                            <form  method="post" name="form" action="">
                            <div class="input-group" style="margin:5px 0;">
                                <input name="bid_price" id="bid_price" type="text" class="form-control input-sm" placeholder="Type your bid..." />
                                <span class="input-group-btn">
                                    <button class="btn btn-send-bid btn-sm submit_button" type="submit" name="submit" id="btn-chat">Send</button>
                                </span>
                            </div>
                            </form>
                        </div>
                        <div class="col-md-3">
                            <div class="row" style="margin:5px 0;">
                                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                                    <div class="btn-group" role="group" style="margin:0 5px;">
                                        <button type="button" class="btn btn-custom-bid btn-block btn-sm" data-toggle="collapse" data-target="#place-budget">Run Auto Bid</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="place-budget" class="collapse">
                        <div class="auto-bid-area">
                        <div class="input-group" style="margin:5px 0;">
                            <input id="btn-input" type="text" class="form-control" placeholder="Your Budget..." />
                            <span class="input-group-btn">
                                <button class="btn btn-custom" id="btn-autoBid" onclick="toggleAutoBit()">Run Auto Bid</button>
                            </span>
                        </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- bidding chat area end -->

<script type="text/javascript">
var autoBitTimer = null;
var myLastBit = 0;
var bitIncrement = 100000; // Change this to set minimun increase bit
function toggleAutoBit(){
    if( autoBitTimer ){
        $('#btn-autoBid').html('Run Auto Bid');
        $('#btn-input').val(0);
        $('#place-budget').collapse('hide');
        clearInterval(autoBitTimer);
        return;
    }
    
    $('#btn-autoBid').html('Stop Auto Bid');
    
    autoBitTimer = setInterval(function(){
        var targetBudget = parseInt($('#btn-input').val());
        var currentBit   = $('#top-bid').html().replace(/[^0-9]/g, '');
        currentBit = parseInt(currentBit);
        if(currentBit <= myLastBit)
            return;
        var myBit = currentBit + bitIncrement;
        
        // check if bit budget is out of last bit
        if(myBit > targetBudget){
            alert('Budget out of last bit');
            return toggleAutoBit();
        }
        
        $("#bid_price").val(myBit);
        $(".submit_button").click();
    }, 1000);
}


    function autoRefresh_div()
    {
      $("#show").load("<?= base_url('auction/refresh/'.$item->id) ?>");// a function which will load data from other file after x seconds
      $("#top-bid").load("<?= base_url('auction/refresh_top_bid/'.$item->id) ?>");

      
    }
 
    setInterval('autoRefresh_div()', 1000); 

    $(function() {       

        $(".submit_button").click(function() {
            var textcontent = $("#bid_price").val();
            var currentBit   = $('#top-bid').html().replace(/[^0-9]/g, '');
            
            // remove unused formatter?
            textcontent = textcontent.replace(/[^0-9]/g, '');
            
            // Save my last bit data.
            // incase using autobit
            myLastBit = parseInt(textcontent);
            
            var dataString = 'bid_price='+ myLastBit;
            if(!myLastBit){
                alert("Enter some bid..");
                $("#bid_price").focus();
            }
            else if (myLastBit <= currentBit){
                alert("Sorry!, Your bid too low");
                $("#bid_price").focus();
            }
            else{
                $("#flash").show();
                $("#flash").fadeIn(400).html('<span class="load">Loading..</span>');
               
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('auction/bid/'.$item->id) ?>",
                    data: dataString,
                    cache: true,
                    beforeSend: function() { 
                        $("#jnn").html('Loading ...', true);
                        $("#btn-chat").prop('disabled', true); // disable button
                    },
                    success: function(html){
                        $("#btn-chat").prop('disabled', false); // enable button
                        $("#jnn").html('');
                        document.getElementById('bid_price').value='';
                    }  
                });
            }
            return false;
        });
    });
</script>

<script>
function formatAngka(angka) {
    if (typeof(angka) != 'string') angka = angka.toString();
    var reg = new RegExp('([0-9]+)([0-9]{3})');
    while(reg.test(angka)) angka = angka.replace(reg, '$1.$2');
    return angka;
}

$('#bid_price').on('keypress', function(e) {
    var c = e.keyCode || e.charCode;
    switch (c) {
        case 8: case 9: case 27: case 13: return;
        case 65:
            if (e.ctrlKey === true) return;
        }
if (c < 48 || c > 57) e.preventDefault();
}).on('keyup', function() {
    var inp = $(this).val().replace(/\./g, '');
  
new Number(inp);
$(this).val(formatAngka(inp));
});
</script>

<?php
$date = $item->time_end;
$exp_date = strtotime($date);
$now = time();
echo $exp_date;
if ($now < $exp_date) {
?>
<script>
// Count down milliseconds = server_end - server_now = client_end - client_now
var server_end = <?php echo $exp_date; ?> * 1000;
var server_now = <?php echo $now; ?> * 1000;
var client_now = new Date().getTime();
var end = server_end - server_now + client_now; // this is the real end time

var _second = 1000;
var _minute = _second * 60;
var _hour = _minute * 60;
var _day = _hour *24
var timer;

function showRemaining()
{
    var now = new Date();
    var distance = end - now;
    if (distance < 0 ) {
       clearInterval( timer );
       window.setTimeout('window.location="<?= base_url('auction/autoemail/'.$item->id) ?>"; ',1000);

       return;
    }
    var days = Math.floor(distance / _day);
    var hours = Math.floor( (distance % _day ) / _hour );
    var minutes = Math.floor( (distance % _hour) / _minute );
    var seconds = Math.floor( (distance % _minute) / _second );

    var countdown = document.getElementById('countdown');
    countdown.innerHTML = '';
    if (days) {
        countdown.innerHTML += days +' Days '   ;
    }
    if (hours) {
    countdown.innerHTML += hours +':'  ;
    }
    if (minutes) {
    countdown.innerHTML += minutes +':'  ;
    }
    countdown.innerHTML += seconds;
}

timer = setInterval(showRemaining, 1000);
</script>
<?php
} else {
    $url=base_url('auction/'.$auctions->slug);
    header('Location:'.$url);
}
?>
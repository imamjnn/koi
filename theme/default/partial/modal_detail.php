<div class="row">
	<div class="col-lg-12">
		<h4 class="modal-title block-title-modal secondary-title"><?= $item->name ?></h4><button class="close" data-dismiss="modal" type="button">&times;</button>
	</div>
</div>
<div class="row">
	<!--image-->
	<div class="col-sm-4"><img class="col-xs-12 no-gutter" src="img/item/koi-1.jpg"></div><!--details-->
	<div class="col-sm-8 modal-custom-content">
		<table class="table table-borderless table-no-margin" id="item-table">
			<tr>
				<td class="item-attribute-label" style="width: 150px">CODE</td>
				<td style="width: 10px">:</td>
				<td style="width: 150px">SA0301</td>
			</tr>
			<tr>
				<td class="item-attribute-label">BREEDER</td>
				<td>:</td>
				<td>Sakai Fish Farm</td>
			</tr>
			<tr>
				<td class="item-attribute-label">VARIETY</td>
				<td>:</td>
				<td>Kohaku</td>
			</tr>
			<tr>
				<td class="item-attribute-label">FISH RATING</td>
				<td>:</td>
				<td>
					<div>
						<span aria-hidden="true" class="glyphicon glyphicon-star"></span> <span aria-hidden="true" class="glyphicon glyphicon-star"></span> <span aria-hidden="true" class="glyphicon glyphicon-star"></span> <span aria-hidden="true" class="glyphicon glyphicon-star-empty"></span> <span aria-hidden="true" class="glyphicon glyphicon-star-empty"></span>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="3"><strong>70 cm | 4 Year(s) old | Female</strong></td>
			</tr>
		</table>
	</div>
	<div class="col-sm-8 modal-custom-content">
		<table class="table table-borderless table-no-margin" id="item-table">
			<tr>
				<td class="item-attribute-label" style="width: 150px">CLOSE TIME</td>
				<td style="width: 10px">:</td>
				<td class="close-time" style="width: 150px">20/Aug/14 21:00</td>
			</tr>
			<tr>
				<td class="item-attribute-label">REMAINING TIME</td>
				<td>:</td>
				<td>
					<div class="finishTime"></div>
				</td>
			</tr>
			<tr>
				<td class="item-attribute-label">START PRICE</td>
				<td>:</td>
				<td>Rp. 1,000,000</td>
			</tr>
			<tr>
				<td class="item-attribute-label">CURRENT PRICE</td>
				<td>:</td>
				<td style="color: red">Rp. 40,000,000</td>
			</tr>
		</table>
		<hr>
	</div>
	<div class="col-md-8 modal-custom-content">
		<label>Note :</label>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
	</div>
</div>
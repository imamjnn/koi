<div id="left-column" class="col-md-3">
    <div class="login_member">
        <?php if (!$this->user) { ?>
            <?php
            $errors = $this->form->getError();
            if (!$errors)
                $errors = array()
                ?>
            <form method="post" action="<?= base_url('Home/login'); ?>" data-name="<?= $this->form->name ?>">
                <fieldset id="left-login-form" class="grey-bg">
                    <div class="form-group">
                        <label>Email:</label>
                        <?php if (array_key_exists('email', $errors)): ?>
                            <div class="errorr" style="font-size: 13px; color: red; padding: 0 0 0 10px;"><?= $errors['email']; ?></div>
                        <?php endif; ?>
                        <input type="text" name="email" class="form-control" id="email" placeholder="Username" required>
                    </div>
                    <div class="form-group">
                        <label>Password:</label>
                        <?php if (array_key_exists('password', $errors)): ?>
                            <div class="errorr" style="font-size: 13px; color: red; padding: 0 0 0 10px;"><?= $errors['password']; ?></div>
                        <?php endif; ?>
                        <input type="password" name="password" class="form-control" id="pwd" placeholder="Password" required>
                    </div>
                    <button type="submit" class="btn btn-custom" style="font-size:13px; font-weight: bold;letter-spacing:1px;">Login</button>
                    <div style="margin-top:15px; margin-bottom:10px; font-size: 12px; line-height:16px; display: block">
                        Don't have an account? please register <a style="font-size:12px;color:#ed1c24" href="<?php echo base_url("register"); ?>">here</a>
                    </div>
                </fieldset>
            </form>
        <?php } else { ?>
            <fieldset id="member-card" class="grey-bg">
                <label class="customer">Welcome <?= $this->user->fullname ?></label>
                <div class="account-card">
                    <h6>My Account</h6>
                    <ul style="margin-left:-30px;">
                        <li><a href="<?php echo base_url('register/edit/' . $this->user->id) ?>">Change Password</a></li>
                        <li><a href="<?= base_url('register/mydata/' . $this->user->id) ?>">My Data</a></li>
                        <li><a href="<?php echo base_url("Verifymember/logout"); ?>">Logout</a></li>

                    </ul>
                </div>
            </fieldset>
        <?php } ?>
    </div>

    <div class="block-menu">
        <h6 style="color:#c9c9c9;">Sponsors</h6>
        <div id="slider-ads" class="flexslider" style="margin: 0 0 20px 0; padding: 15px; border: none; border-radius: 0; background-color: transparent; max-width:100%">
            <ul class="slides">
                <?php if ($banners): ?>
                    <?php foreach ($banners as $banner): ?>
                        <li><a target="_blank" href="<?= $banner->link ?>"><img src="<?= $banner->media->_780x ?>" /></a></li>
                    <?php endforeach; ?>
                <?php endif; ?>    
            </ul>
        </div>
    </div>

    <script type="text/javascript" charset="utf-8">
        $(window).load(function () {
            $('#slider-ads.flexslider').flexslider({
                slideshowSpeed: 2000,
                animationSpeed: 700,
                animation: "fade",
                controlNav: false,
                directionNav: false,
                useCSS: false
            });
            $('#oth-pro').click(function () {
                $('#sub-oth-pro').slideToggle();
                return false;
            })
        });
    </script>
</div>
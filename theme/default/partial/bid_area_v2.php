<!-- bidding chat area -->
<?php
    $time1= $bids_current->time->time;
    $time2= strtotime($item->time_end);
    $hasil = $time2-$time1;
?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="label-chat-heading" id="top-bid">
                        <span class="hidden-text">
                    </div>
                    <div class="chat-heading pull-right">
                        <small><span class="hidden-text">Rem. Time :</span> <i id="countdown"></i></small>
                    </div>
                </div>
                <div class="chat-area">
                <div class="panel-body">
                    <ul class="chat">
                            <div id="jnn"></div>
                            <div id="show"></div>
                    </ul>
                </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-12">
                        <b>Your current budget :</b> <i id="cur-budget"></i>
                        <div id="extra-time"></div>
                            <form  method="post" name="form" action="">
                            <div class="input-group" style="margin:5px 0;">
                                <input name="budget" id="budget" type="text" class="form-control input-sm" placeholder="Type your bid..." />
                                <span class="input-group-btn">
                                    <button class="btn btn-send-bid btn-sm submit_button" type="submit" name="submit" id="btn-chat">Send</button>
                                </span>
                            </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- bidding chat area end -->


<script>
    function autoRefresh_div(){$("#show").load("<?= base_url('bid/insert/'.$item->id) ?>",function(){window.isAllowedToSendEmail&&autoMail(window.isAllowedToSendEmail)}),$("#top-bid").load("<?= base_url('auction/refresh_top_bid/'.$item->id) ?>"),$("#cur-budget").load("<?= base_url('auction/refresh_budget/'.$item->id) ?>"),$("#curr").html("<?= $item->time_end->time ?>")}function autoMail(a){window.isAllowedToSendEmail=null;var b=$("#top-bid").html().replace(/[^0-9]/g,"");a=a.replace(/[^0-9]/g,"");var c="budget="+a;return a?parseInt(a)<=b?(alert("Sorry!, Your bid too low"),$("#budget").focus()):$.ajax({type:"POST",url:"<?= base_url('bid/automail/' . $item->id) ?>",dataType:"json",data:c,cache:!1,success:function(a){$("#btn-chat").prop("disabled",!1),$("#jnn").html("")}}):(alert("Enter some bid.."),$("#budget").focus()),!1}function formatAngka(a){"string"!=typeof a&&(a=a.toString());for(var b=new RegExp("([0-9]+)([0-9]{3})");b.test(a);)a=a.replace(b,"$1.$2");return a}window.isAllowedToSendEmail=!1,setInterval("autoRefresh_div()",1e3),$(function(){$(".submit_button").click(function(){var a=$("#budget").val(),b=$("#top-bid").html().replace(/[^0-9]/g,"");$("#kofet").html();a=a.replace(/[^0-9]/g,"");var d="budget="+a;return a?parseInt(a)<=b?(alert("Sorry!, Your bid too low"),$("#budget").focus()):$.ajax({type:"POST",url:"<?= base_url('bid/budget/'.$item->id) ?>",data:d,cache:!0,beforeSend:function(){$("#jnn").html("Loading ...",!0),$("#btn-chat").prop("disabled",!0)},success:function(b){$("#btn-chat").prop("disabled",!1),$("#jnn").html(""),document.getElementById("budget").value="",window.isAllowedToSendEmail=a}}):(alert("Enter some bid.."),$("#budget").focus()),!1})}),$("#budget").on("keypress",function(a){var b=a.keyCode||a.charCode;switch(b){case 8:case 9:case 27:case 13:return;case 65:if(a.ctrlKey===!0)return}(b<48||b>57)&&a.preventDefault()}).on("keyup",function(){var a=$(this).val().replace(/\./g,"");new Number(a),$(this).val(formatAngka(a))});
</script>

<?php
$date = $item->time_end;
$exp_date = strtotime($date);
$now = time();

if ($now < $exp_date) {
?>
<script>
var server_end = <?php echo $exp_date; ?> * 1000;var server_now = <?php echo $now; ?> * 1000;var client_now = new Date().getTime();var end = server_end - server_now + client_now;var _second = 1000;var _minute = _second * 60;var _hour = _minute * 60;var _day = _hour *24;var timer;function showRemaining(){var now = new Date();var distance = end - now;if (distance <= 0 ) {
       clearInterval( timer );window.setTimeout('window.location.href="<?= base_url('auction/autoemail/'.$item->id) ?>"; ',1000);return;}$(".submit_button").click(function() {if (distance < 180000){$('#extra-time').html('<i style="color: blue">The auction time has been extended 3 minutes, Please reload the page!</i>');}});var days = Math.floor(distance / _day);var hours = Math.floor( (distance % _day ) / _hour );var minutes = Math.floor( (distance % _hour) / _minute );var seconds = Math.floor( (distance % _minute) / _second );var countdown = document.getElementById('countdown');countdown.innerHTML = '';if (days) {countdown.innerHTML += days +' Days '   ;}if (hours) {countdown.innerHTML += hours +':'  ;}if (minutes) {countdown.innerHTML += minutes +':'  ;}countdown.innerHTML += seconds;}timer = setInterval(showRemaining, 1000);
</script>
<?php
} else {
    $url=base_url('auction/'.$auctions->slug);
    header('Location:'.$url);
}
?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#F30000" />

    <link type="text/css" href="<?= $this->theme->asset('static/vendor/css/bootstrap.min.css') ?>" rel="stylesheet" media="screen" />
    <link type="text/css" href="<?= $this->theme->asset('static/vendor/css/bootstrap-theme.css') ?>" rel="stylesheet" media="screen" />
    <link rel="stylesheet" href="<?= $this->theme->asset('static/css/font-awesome.min.css') ?>">
    <link type="text/css" href="<?= $this->theme->asset('static/css/magnific-popup.css') ?>" rel="stylesheet" media="screen" />
    <link type="text/css" href="<?= $this->theme->asset('static/css/koi.css') ?>" rel="stylesheet" media="screen" />
    <link type="text/css" href="<?= $this->theme->asset('static/css/chat.css') ?>" rel="stylesheet" media="screen" />
    <link type="text/css" href="<?= $this->theme->asset('static/css/flexslider.css') ?>" rel="stylesheet" media="screen" />
    
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,400i,600,600i,700,700i" rel="stylesheet">
    
    <script type="text/javascript" src="<?= $this->theme->asset('static/js/jquery-1.10.2.min.js') ?>" charset="utf-8"></script>
    <script type="text/javascript" src="<?= $this->theme->asset('static/vendor/js/bootstrap.min.js') ?>" charset="utf-8"></script>
    <script type="text/javascript" src="<?= $this->theme->asset('static/js/jquery.magnific-popup.min.js') ?>" charset="utf-8"></script>
    <script type="text/javascript" src="<?= $this->theme->asset('static/vendor/js/jquery.flexslider.js') ?>" charset="utf-8"></script>

    
    <title>Gading Koi Auction</title> 
</head>

<body>

<script>
    
    $(document).ready(function() {
        $('.carousel').carousel({
        interval: 3000
        })
    });
    
    $(document).ready(function() {
        $('.image-link').magnificPopup({
            type:'image'});
    });
    
    $(document).ready(function() {

        $('.image-popup-vertical-fit').magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          mainClass: 'mfp-img-mobile',
          image: {
            verticalFit: true
          }
          
        });

        $('.image-popup-fit-width').magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          image: {
            verticalFit: false
          }
        });
        
        $('.image-popup-no-margins').magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: false,
          fixedContentPos: true,
          mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
          image: {
            verticalFit: true
          },
          zoom: {
            enabled: true,
            duration: 150 // don't foget to change the duration also in CSS
          }
        });

      });
    
</script>

<div id="cvr">
    <div id="ajax-loader">
        <div id="ajax-loader-container">
            <img src="img/elements/preloader.gif" style="width: 60px; height: 60px" /><br>Loading Data...
        </div>
    </div>
    
    <div id="top-header"></div>
    <div id="page-wrapper" class="container">
        
        <!-- header -->
                <div id="header" class="row">

                    <!-- start navbar -->
                    <div class="nav-holder">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="logo-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                                <a href="#menu" class="menu-link"><i class="fa fa-bars"></i></a>
                            </button>
                            <!--logo holder -->
                            <div class="logo-block">
                                <a href="<?= base_url() ?>" class="inner-logo">''</a>
                            </div>
                            <!--logo holder ends here-->
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <?php
                                $menus = $this->db->get_where('site_menu', array('parent' => 0, 'group' => 'header-menu'));
                                foreach ($menus->result() as $menu) {
                                    $submenus = $this->db->get_where('site_menu', array('parent' => $menu->id, 'group' => 'header-menu'));
                                    if ($submenus->num_rows() > 0) {
                                        echo '<li>
            
            <a href="#' . $menu->id . '" data-toggle="collapse" style="padding-left: 0px;">' . $menu->label . ' <span class="caret"></span><i class="zmdi zmdi-chevron-down"></i></a>';
                                        echo '<ul class="sub-menu" id="' . $menu->id . '" style="padding-left: 0px;">';
                                        foreach ($submenus->result() as $submenu) {
                                            echo '<li><a href="' . $submenu->url . '">' . $submenu->label . '</a></li>';
                                        }
                                        echo '</ul>';

                                        echo '</li>';
                                    } else {
                                        echo '<li><a href="' . $menu->url . '">' . $menu->label . '</a></li>';
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <!-- navbar -->
                </div>
                <!-- header -->

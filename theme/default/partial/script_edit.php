<script>
    $('document').ready(function ()
    {
        /* validation */
        $("#edit-form").validate({
            rules:
                    {
                        name: {
                            required: true,
                            minlength: 3
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            minlength: 8,
                            maxlength: 15
                        },
                        conf_password: {
                            required: true,
                            equalTo: '#password'
                        }
                    },
            messages:
                    {
                        name: {
                            required: "please enter username",
                            minlength: "minimum 3 characters"
                        },
                        email: {
                            required: "please enter a valid email address",
                            minlength: "minimum 3 characters"
                        },
                        password: {
                            required: "please provide a password",
                            minlength: "password at least have 8 characters",
                            maxlength: "maximum 15 characters"
                        },
                        conf_password: {
                            required: "please retype your password",
                            equalTo: "password doesn't match !"
                        }
                    },
            submitHandler: submitForm
        });
        /* validation */

        /* form submit */
        function submitForm()
        {
            var data = $("#edit-form").serialize();

            $.ajax({
                type: 'POST',
                url: "<?php echo site_url(); ?>Register/update",
                data: data,
                success: function()
                {
                    $('#name').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('#conf_password').val('');
                    
//                    $("#edit-form").val('reset');
                    
                    $('#submit_button').val('Update');
                    
                    if (result.error) {
                        $('#result').append('<div style="margin: 0 0 25px 0;" class="alert alert-danger"><button type="button" class="close">×</button>Update is failed!</div>');
                    } else {
                        $('#result').html('<div style="margin: 0 0 25px 0;" class="alert alert-success"><button type="button" class="close">×</button>Update is successful!</div>');
                        
                    }

                    window.setTimeout(function () {
                        $(".alert").fadeTo(500, 0).slideUp(500, function () {
                            
                            $(this).remove();
                        });
                    }, 5000);

                    $('.alert .close').on("click", function (e) {
                        $(this).parent().fadeTo(500, 0).slideUp(500);
                        
                    });

                    
                }
            });
        }
        /* form submit */

    });
</script>
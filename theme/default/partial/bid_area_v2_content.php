<?php if($bids): ?>
    <?php foreach($bids as $b): ?>
    <li class="<?php if($b->user->id == $this->user->id){echo 'right';}else{echo 'left';} ?> clearfix">
        <span class="chat-img <?php if($b->user->id == $this->user->id){echo 'pull-right';}else{echo 'pull-left';} ?>">
            <img src="<?= $b->user->avatar->_50x50 ?>" alt="User Avatar" class="img-circle" />
       </span>
       <div class="chat-body clearfix">
           <div class="header-chat-body">
                <small class="time-text text-muted <?php if($b->user->id == $this->user->id){echo 'pull-left';}else{echo '';} ?>"><span class="glyphicon glyphicon-time"></span><?= $b->time->format('d M H:i') ?></small>
                <strong class="primary-font <?php if($b->user->id == $this->user->id){echo 'pull-right';}else{echo '';} ?>"><?= $b->user->fullname ?></strong> 
            </div>
            <p>
                <?= number_format($b->bid_price,0,",",".") ?>
            </p>
        </div>
    </li>
    <?php endforeach; ?>
<?php endif; ?>
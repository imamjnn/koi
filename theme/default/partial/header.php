<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="theme-color" content="#F30000" />

        <link rel="stylesheet" href="<?= $this->theme->asset('static/vendor/css/bootstrap.min.css') ?>">
        <link rel="stylesheet" href="<?= $this->theme->asset('static/vendor/css/bootstrap-theme.css') ?>">
        <link rel="stylesheet" href="<?= $this->theme->asset('static/css/font-awesome.min.css') ?>">
        <link rel="stylesheet" href="<?= $this->theme->asset('static/css/koi.css') ?>">
        <link rel="stylesheet" href="<?= $this->theme->asset('static/css/flexslider.css') ?>">
        
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,400i,600,600i,700,700i" rel="stylesheet">

         <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

        <script type="text/javascript" src="<?= $this->theme->asset('static/js/jquery-1.10.2.min.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->theme->asset('static/js/jquery.min.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->theme->asset('static/js/jquery.validate.min.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->theme->asset('static/vendor/js/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->theme->asset('static/vendor/js/jquery.flexslider.js') ?>"></script>
        
        <title>Gading Koi Auction</title> 
    </head>

    <body>

        <script>
            $(document).ready(function () {
                $('.carousel').carousel({
                    interval: 3000
                })
            });
        </script>

        <div id="cvr">
            <div id="ajax-loader">
                <div id="ajax-loader-container">
                    <img src="img/elements/preloader.gif" style="width: 60px; height: 60px" /><br>Loading Data...
                </div>
            </div>

            <div id="top-header"></div>
            <div id="page-wrapper" class="container">

                <!-- header -->
                <div id="header" class="row">

                    <!-- start navbar -->
                    <div class="nav-holder">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="logo-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                                <a href="#menu" class="menu-link"><i class="fa fa-bars"></i></a>
                            </button>
                            <!--logo holder -->
                            <div class="logo-block">
                                <a href="<?= base_url() ?>" class="inner-logo">''</a>
                            </div>
                            <!--logo holder ends here-->
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <?php
                                    $menus = $this->menu->item('header-menu');
                                    if($menus && array_key_exists(0, $menus)){
                                        foreach($menus[0] as $menu){
                                            echo '<li' . ( $menu->active ? ' class="active"' : '' ) . '><a href="' . $menu->url . '">' . $menu->label . '</a></li>';
                                        }
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <!-- navbar -->
                </div>
                <!-- header -->

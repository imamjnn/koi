<script>
    window.isAllowedToSendEmail = false;
    function autoRefresh_div()
        {
          $("#show").load("<?= base_url('bid/insert/'.$item->id) ?>",function(){
            if (window.isAllowedToSendEmail)
                autoMail(window.isAllowedToSendEmail);
        });
          $("#top-bid").load("<?= base_url('auction/refresh_top_bid/'.$item->id) ?>");
          $("#cur-budget").load("<?= base_url('auction/refresh_budget/'.$item->id) ?>");
          $("#curr").html("<?= $item->time_end->time ?>");
        }
    setInterval('autoRefresh_div()', 1000); 

    $(function() {       

        $(".submit_button").click(function() {
            var textcontent = $("#budget").val();
            var currentBit  = $('#top-bid').html().replace(/[^0-9]/g, '');            
            var petko = $('#kofet').html();

            textcontent = textcontent.replace(/[^0-9]/g, '');
            var dataString = 'budget='+ textcontent;
            if(!textcontent){
                alert("Enter some bid..");
                $("#budget").focus();
            }
            
            else if (parseInt(textcontent) <= currentBit){
                alert("Sorry!, Your bid too low");
                $("#budget").focus();
            }
           
            else{
               
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('bid/budget/'.$item->id) ?>",
                    data: dataString,
                    cache: true,
                    beforeSend: function() { 
                        $("#jnn").html('Loading ...', true);
                        $("#btn-chat").prop('disabled', true); // disable button
                    },
                    success: function(html){
                        $("#btn-chat").prop('disabled', false); // enable button
                        $("#jnn").html('');
                        document.getElementById('budget').value='';
                        window.isAllowedToSendEmail = textcontent;
                    }  
                });
            }
            return false;
        });
    });
</script>

<script>
    function autoMail(textcontent) {
        window.isAllowedToSendEmail = null;
        //var textcontent = $("#budget").val();
        var currentBit = $('#top-bid').html().replace(/[^0-9]/g, '');

        textcontent = textcontent.replace(/[^0-9]/g, '');
        var dataString = 'budget=' + textcontent;
        if (!textcontent) {
            alert("Enter some bid..");
            $("#budget").focus();
        } else if (parseInt(textcontent) <= currentBit) {
            alert("Sorry!, Your bid too low");
            $("#budget").focus();
        } else {

            $.ajax({
                type: "POST",
                url: "<?= base_url('bid/automail/' . $item->id) ?>",
                dataType: "json",
                data: dataString,
                cache: false,
                success: function (html) {
                    $("#btn-chat").prop('disabled', false); // enable button
                    $("#jnn").html('');
                }
            });
        }
        return false;
    }

</script>

<script>
function formatAngka(angka) {
    if (typeof(angka) != 'string') angka = angka.toString();
    var reg = new RegExp('([0-9]+)([0-9]{3})');
    while(reg.test(angka)) angka = angka.replace(reg, '$1.$2');
    return angka;
}

$('#budget').on('keypress', function(e) {
    var c = e.keyCode || e.charCode;
    switch (c) {
        case 8: case 9: case 27: case 13: return;
        case 65:
            if (e.ctrlKey === true) return;
        }
if (c < 48 || c > 57) e.preventDefault();
}).on('keyup', function() {
    var inp = $(this).val().replace(/\./g, '');
  
new Number(inp);
$(this).val(formatAngka(inp));
});
</script>

<?php
$date = $item->time_end;
$exp_date = strtotime($date);
$now = time();

if ($now < $exp_date) {
?>
<script>
// Count down milliseconds = server_end - server_now = client_end - client_now
var server_end = <?php echo $exp_date; ?> * 1000;
var server_now = <?php echo $now; ?> * 1000;
var client_now = new Date().getTime();
var end = server_end - server_now + client_now; // this is the real end time

var _second = 1000;
var _minute = _second * 60;
var _hour = _minute * 60;
var _day = _hour *24;
var timer;

function showRemaining()
{
    var now = new Date();
    var distance = end - now;

    if (distance < 0 ) {
       clearInterval( timer );
       window.setTimeout('window.location.href="<?= base_url('auction/autoemail/'.$item->id) ?>"; ',1000);

       return;
    }

    $(".submit_button").click(function() {
        if (distance < 600000){
            $('#extra-time').html('<i style="color: blue">The auction time has been extended 10 minutes, Please reload the page!</i>');
        }
    });

    var days = Math.floor(distance / _day);
    var hours = Math.floor( (distance % _day ) / _hour );
    var minutes = Math.floor( (distance % _hour) / _minute );
    var seconds = Math.floor( (distance % _minute) / _second );

    var countdown = document.getElementById('countdown');
    countdown.innerHTML = '';
    if (days) {
        countdown.innerHTML += days +' Days '   ;
    }
    if (hours) {
    countdown.innerHTML += hours +':'  ;
    }
    if (minutes) {
    countdown.innerHTML += minutes +':'  ;
    }
    countdown.innerHTML += seconds;
}
timer = setInterval(showRemaining, 1000);
</script>
<?php
} else {
    $url=base_url('auction/'.$auctions->slug);
    header('Location:'.$url);
}
?>
<script>
        (function ($, W, D) {
            var JQUERY4U = {};
            JQUERY4U.UTIL = {
                setupFormValidation: function () {
                    $('#google_form').validate({
                        ignore: ":hidden:not(#keycode)",
                        rules: {
                            "hiddencode": {
                                required: function () {
                                    if (grecaptcha.getResponse() == '') {
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }}}
                    });
                }}
            $(D).ready(function ($) {
                JQUERY4U.UTIL.setupFormValidation();
            });
        })(jQuery, window, document);
</script>

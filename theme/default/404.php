    <?= $this->theme->file('partial/header') ?>
    <!-- content starts here -->
          <div class="row gutter">
            <div class="col-12-sm">
              <img class="center-block full-img" src="<?= base_url('theme/default/static/img/elements/404.jpg') ?>">
              <h1 class="block-title text-center">Oops! Something is wrong</h1>
              <p class="text-center">There&#39;s nothing more out there, wanna back <a class="specials" href="<?= base_url() ?>">home</a>&#63;</p>
            </div>
          </div>
          <div class="btm-margin"></div>
          <div class="push"></div>
          <!-- content ends here -->
          </div>

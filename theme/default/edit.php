
<?= $this->theme->file('partial/header') ?>

<script type="text/javascript" charset="utf-8">
    $(window).load(function () {
        $('#slider-block.flexslider').flexslider({
            slideshowSpeed: 6000,
            animationSpeed: 1000,
            animation: "fade",
            controlNav: false,
            directionNav: false,
            useCSS: false
        });
    });
</script>
<div class="row">
    <?= $this->theme->file('partial/sidebar') ?>
    <div id="content" class="col-md-9">
        <div id="content-wrapper">
            <div class="row">
                <div class="col-sm-12 rules">
                    <div class="registrasi">
                        <?= $this->theme->file('partial/script_edit') ?>

                        <div class="title_register">
                            Change New Password
                        </div>
                        <center><div id="result"></div></center>
                        <div class="content_register">
                            <form autocomplete="off" method="post" id="edit-form">
                                <table class="tabel_register">
                                    <tr>
                                        <td>
                                            <input type="hidden" value="<?= $this->user->id ?>" name="idx">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="jenis_name">Username</td>
                                        <td class="input_name"><input type="text" value="<?= $this->user->name ?>" name="name" id="name" placeholder="Username" class="input_text" disabled /></td>
                                    </tr>
                                    <tr>
                                        <td class="jenis_name">Email</td>
                                        <td class="input_name"><input type="email" value="<?= $this->user->email ?>" name="email" id="email" placeholder="Email" class="input_text" disabled /></td>
                                    </tr>
                                    <tr>
                                        <td class="jenis_name">Password</td>
                                        <td class="input_name"><input type="password" name="password" id="password" placeholder="Password" class="input_text" required></td>
                                    </tr>
                                    <tr>
                                        <td class="jenis_name">Confirm Password</td>
                                        <td class="input_name"><input type="password" name="conf_password" id="conf_password" placeholder="Confirm Password" class="input_text" required></td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td class="input_name">
                                            <input class="submit" id="submit_button" type="submit" value="Update"/>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--Content End-->

<?= $this->theme->file('partial/footer') ?>

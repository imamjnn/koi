<?= $this->theme->file('partial/header') ?>

<script type="text/javascript" charset="utf-8">
    $(window).load(function () {
        $('#slider-block.flexslider').flexslider({
            slideshowSpeed: 6000,
            animationSpeed: 1000,
            animation: "fade",
            controlNav: false,
            directionNav: false,
            useCSS: false
        });
    });
</script>
<div class="row">
    <?= $this->theme->file('partial/sidebar') ?>
    <div id="content" class="col-md-9">
        <div id="content-wrapper">
            <div class="row">
                <div class="col-sm-12 rules">
                    <div class="registrasi">


                        <div class="title_register">
                            ACCOUNT DETAIL
                        </div>
                        <center><div id="result"></div></center>
                        <div class="content_register">

                            <?php echo form_open_multipart("Register/updatemydata"); ?>
                            <table class="tabel_register">
                                <tr>
                                    <td>
                                        <input type="hidden" value="<?= $this->user->id ?>" name="idy">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="jenis_name">Username</td>
                                    <td class="input_name"><input type="text" value="<?= $this->user->name ?>" name="name" id="name" class="input_text" required /></td>
                                </tr>
                                <tr>
                                    <td class="jenis_name">Fullname</td>
                                    <td class="input_name"><input type="text" value="<?= $this->user->fullname ?>" name="fullname" id="fullname" class="input_text" required /></td>
                                </tr>
                                <tr>
                                    <td class="jenis_name">Email</td>
                                    <td class="input_name"><input type="email" value="<?= $this->user->email ?>" name="email" id="email" class="input_text" required /></td>
                                </tr>
                                <tr>
                                    <td class="jenis_name">Avatar</td>
                                    <td class="input_name"><img style="padding: 5px;border: 1px solid #c6c6c6; border-radius: 3px;width: 100px; height: 100px;" src="<?= $this->user->avatar ?>" /></td>
                                </tr>
                                <tr>
                                    <td class="jenis_name">Edit Avatar</td>
                                    <td class="input_name"><input type="file" name="avatar" id="avatar" class="input_text" required /></td>

                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="input_name">
                                        <input class="submit" id="submit_button" type="submit" value="Update Account"/>
                                        <button class="submit btn-danger" value="Cancel" onclick="window.history.go(-1); return false;" >Cancel</button>
                                    </td>
                                </tr>
                            </table>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--Content End-->

<?= $this->theme->file('partial/footer') ?>
<?= $this->theme->file('partial/header2') ?>


<script type="text/javascript" charset="utf-8">
    $(window).load(function () {
        $('#slider-block.flexslider').flexslider({
            slideshowSpeed: 6000,
            animationSpeed: 1000,
            animation: "fade",
            controlNav: false,
            directionNav: false,
            useCSS: false
        });
    });
</script>
<script>
    
    $(document).ready(function() {
        $('.carousel').carousel({
        interval: 3000
        })
    });
    
    $(document).ready(function() {
        $('.image-link').magnificPopup({
            type:'image'});
    });
    
    $(document).ready(function() {

        $('.image-popup-vertical-fit').magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          mainClass: 'mfp-img-mobile',
          image: {
            verticalFit: true
          }
          
        });

        $('.image-popup-fit-width').magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          image: {
            verticalFit: false
          }
        });
        
        $('.image-popup-no-margins').magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: false,
          fixedContentPos: true,
          mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
          image: {
            verticalFit: true
          },
          zoom: {
            enabled: true,
            duration: 150 // don't foget to change the duration also in CSS
          }
        });

      });
    
    $(document).ready(function(){
        autoPlayYouTubeModal();
    });
    
</script>

<div class="row">
    <?= $this->theme->file('partial/sidebar') ?>
    <div id="content" class="col-md-9">
        <div id="content-wrapper">
            <div class="row">
                <div class="col-sm-12 rules">

                    <h3><?= $auction->name ?></h3>

                </div>
            </div>


            <div class="row">
                <div class="col-sm-12 rules custom-cont" style="padding-bottom: 0;">

                    <div class="row gutter">
                        <h4 class="block-title secondary-title">Fishes in Auction</h4>
                        <button class="btn mysearch no-gutter" type="button" data-toggle="collapse" data-target="#filter-area"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                    <!--form-->
                    <div id="filter-area" class="collapse">
                        <label><small>Sort List By</small></label>
                        <div class="row">
                            <form method="get">
                                <div class="col-xs-12 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <?php if ($items): ?>

                                            <select name="price" class ="form-control input-sm" data-table="items" data-label="price" data-value="id">
                                                <option value="00" selected disabled>Price</option>
                                                <?php foreach ($items as $itm): ?>
                                                    <option value="<?= $itm->price ?>"<?= ($itm->id == $this->input->get('price') ? ' selected="selected"' : '') ?>><?= $itm->price ?></option>
                                                <?php endforeach; ?>
                                            </select>

                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <?php if ($varietys): ?>

                                            <select name="item_variety" class ="form-control input-sm">
                                                <option value="00" selected disabled>Variety</option>
                                                <?php foreach ($varietys as $varss): ?>
                                                    <option value="<?= $varss->id ?>"<?= ($varss->id == $this->input->get('item_variety') ? ' selected="selected"' : '') ?>><?= $varss->name ?></option>
                                                <?php endforeach; ?>
                                            </select>

                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <?php if ($breeders): ?>
                                            <select name="item_breeder" class = "selectpicker form-control input-sm" title="Breeder">
                                                <option value="00" selected disabled>Breeder</option>
                                                <?php foreach ($breeders as $bred): ?>
                                                    <option value="<?= $bred->id ?>"<?= ($bred->id == $this->input->get('item_breeder') ? ' selected="selected"' : '') ?>><?= $bred->fullname ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-3 col-md-3">
                                    <button class="selectpicker form-control input-sm btn btn-custom btn-custom-sm" title="Update List"><span class="glyphicon glyphicon-repeat"></span> Update List</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end form -->
                </div>
            </div>


            <?php if($items): ?>
                <?php foreach ($items as $itm): ?> 
                    <?php if(time() < $itm->time_end->time + 7200):?>
            <div class="row">
                <div class="col-xs-5 col-sm-3 col-md-3">
                    <div id="badgeBlock61" style="position: relative"></div>
                    <a class="image-popup-no-margins" href="<?= $itm->photo ?>"><img class="thumbnail" src="<?= $itm->photo->_143x232 ?>" style="width: 100%;" /></a>
                </div>
                <div class="col-sm-9 col-md-9">
                    <div class="col-xs-12 no-gutter">
                        <h6 class="block-title text-uppercase"><a href="#"><strong><?= $itm->name ?></strong></a></h6>
                    </div>
                    <!--
                    <div class="col-xs-6 col-md-6 no-gutter">
                        <button type="button" class="btn btn-rate" data-toggle="modal" data-target="#rate-this-fish">Rate This Fish</button>
                    </div>
                    -->
                    <div class="row gutter">
                      <div class="col-xs-12 col-md-6 col-lg-6 no-gutter">
                        <table class="table table-borderless table-no-margin" id="item-table">
                            <tr>
                                <td style="width: 150px" class="item-attribute-label" style="width: 10px">CODE</td>
                                <td style="width: 10px"> : </td>
                                <td style="width: 150px"> GK<?= $itm->id ?></td>
                            </tr>
                            
                            <tr>
                                <td class="item-attribute-label">BREEDER</td>
                                <td> : </td>
                                <?php if(property_exists($itm, 'breeder')): ?>
                                    <?php foreach($itm->breeder as $bree): ?>
                                     <td> <?= $bree->fullname ?></td>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tr>
                            <tr>
                                <td class="item-attribute-label">VARIETY</td>
                                <td> : </td>
                                <?php if(property_exists($itm, 'variety')): ?>
                                     <?php foreach($itm->variety as $vari): ?>
                                         <td> <?= $vari->name ?></td>
                                     <?php endforeach; ?>
                                 <?php endif; ?>
                            </tr>

                            <tr>
                                <td colspan="3"><strong><?= $itm->size ?> | <?= $itm->age ?> | <?= $itm->gender ?></strong></td>
                            </tr>
                            <tr>
                                <td class="item-attribute-label">CERTIFICATE</td>
                                <td> : </td>
                                <td> <a class="image-popup-fit-width" href="<?= $itm->certificate ?>">View</a></td>
                            </tr>
                        </table>
                      </div>
                      <div class="col-xs-12 col-md-6 col-lg-6 no-gutter">
                        <table class="table table-borderless table-no-margin" id="item-table">
                            <tr>
                                <td style="width: 150px" class="item-attribute-label">CLOSE TIME</td>
                                <td style="width: 10px"> : </td>
                                <td style="width: 150px" class="close-time"> <?= $itm->time_end->format('d M Y H:i') ?> </td>
                            </tr>
                            <tr>
                                <td class="item-attribute-label">REMAINING TIME</td>
                                <td> : </td>
                                <td><div data="61" class="finishTime">
                                    <?php
                                        $now = new DateTime('now');
                                        $end = new DateTime($itm->time_end->format('d M Y H:i:s'));
                                         
                                        $diff = $now->diff($end);

                                        if($itm->time_end->time < time()){
                                            echo 'Expired';
                                        }else{
                                            echo $diff->format('%d Days %H:%i');
                                        }
                                    ?>
                                </div></td>
                            </tr>
                            <tr>
                                <td class="item-attribute-label">START PRICE</td>
                                <td> : </td>
                                <td> Rp <?= number_format($itm->price,0,",",".") ?> </td>
                            </tr>
                            <tr>
                                <td class="item-attribute-label">CURRENT PRICE</td>
                                <td> : </td>
                                <td style="color: red">Rp <?= number_format($itm->current_price,0,",",".") ?></td>
                            </tr>
                            <tr>
                                <td class="item-attribute-label">-</td>
                                <td>  </td>
                                <td style="color: red"></td>
                            </tr>

                        </table>
                      </div>
                    </div>
                    <div class="row gutter">
                      <div class="col-xs-12 col-sm-6 col-md-6 no-gutter">
                        <ul class="index-action-list">
                            <li>
                                <button type="button" class="btn btn-custom favorite" data-id="<?= $itm->id ?>" title="Add To Favourite"><span class="glyphicon glyphicon-heart" ></span> Add to Favourite</button><div id="unfav"></div
                            </li>
                            <li>
                                <button type="button" class="show-video btn btn-custom" data-toggle="modal" data-target="#videoModal" data-id="<?= $itm->id ?>" id="getItem">Video</button>
                            </li>
                        </ul>
                      </div>
                      <div class="col-xs-12 col-sm-6 col-md-6 no-gutter">
                        <ul class="index-action-list">
                            <li>
                                <button type="button" class="show-fish-detail btn btn-custom" data-toggle="modal" data-target="#fish-details" data-id="<?= $itm->id ?>" id="getItem">Fish Details</button>
                            </li>
                            <?php if($auction->time_start->time > time()): ?>
                                <li><a onclick="noStart()" class="btn btn-danger">BID</a></li>
                            <?php elseif($itm->time_end->time < time()): ?>
                                <button type="button" class="show-history btn btn-history" data-toggle="modal" data-target="#historyModal" data-id="<?= $itm->id ?>" id="getItem">View History</button>
                            <?php else: ?>
                                <li><a href="<?= base_url('auction/item/'.$itm->id) ?>" class="btn btn-danger">BID</a></li>
                            <?php endif; ?>
                        </ul>
                      </div>
                    </div>
                </div>
            </div>
            <hr>
        <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <div class="row">
                        <div class="page">
                        <?php if($pagination): ?>
                            <ul class="pagination">
                            <?php foreach($pagination as $label => $link): ?>
                                <li <?= ( $link == '#' ? ' class="active"' : '' ) ?>><a href="<?= $link ?>"><?= $label ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        </div>
                    </div>

            <div class="row">
                                <div class="col-sm-12 rules" style="padding: 10px 20px 0; margin: 20px 0px">
                                    <h4 class="block-title news">Supplier</h4> 
                                    <!--images carousel-->
                                    <div class="carousel slide" id="myCarousel">
                                        <div class="carousel-inner supplier-box">
                                            <div class="item active">
                                                <ul class="thumbnails">
                                                    <?php if ($suppliers): ?>
                                                        <?php foreach ($suppliers as $index => $supplier): ?>
                                                            <?php if($index == 4 ) break; ?>
                                                            <li class="thumbnail supplier-list">
                                                                <a target="_blank" href="<?= $supplier->link ?>"><img src="<?= $supplier->media ?>" alt="" class="logo" /></a>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>  
                                                </ul>
                                            </div><!-- /Slide1 -->
                                            <div class="item">
                                                <ul class="thumbnails">
                                                    <?php if ($suppliers): ?>
                                                        <?php foreach ($suppliers as $index => $supplier): ?>
                                                            <?php if($index < 4 ) continue; ?>
                                                            <li class="thumbnail supplier-list">
                                                                <a target="_blank" href="<?= $supplier->link ?>"><img src="<?= $supplier->media ?>" alt="" class="logo" /></a>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>  
                                                </ul>
                                            </div><!-- /Slide1 -->
                                        </div>
                                    </div>
                                    <!--images carousel end-->

                                </div>
                            </div>
        </div>
    </div>
</div> <!--Content End-->

<!--modal play video-->
<div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="videoModal" aria-hidden="true">
    <div class="modal-dialog">
        <div id="videoModal" class="modal-content">
            <div class="no-bg modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-video">
                    <div class="embed-responsive embed-responsive-16by9">
                        <div>
                            <div id="video-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--modal play video end-->

<!--modal fish detail-->
<div class="modal fade" id="fish-details" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body lead">
                <div id="fish-detail-content"></div>
            </div>
        </div>
    </div>
</div>
<!--modal fish detail end-->

<!--modal view history-->
<div class="modal fade" id="historyModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div id="view-history"></div>
        </div>
    </div>
</div>
<!--modal view history-->

<?= $this->theme->file('partial/footer') ?> 
<script>
     $('.show-video').click(function(){
         var Id=$(this).attr('data-id');
 
         $.ajax({url:"<?= base_url('auction/item_modal') ?>/"+Id,
            cache:false,
            beforeSend:function(){
                $("#video-content").html("Please wait....");
                },
            success:function(result){
            $("#video-content").html(result);
             
         }});
     });

     $('.favorite').click(function(){
         var Id=$(this).attr('data-id');
 
         $.ajax({url:"<?= base_url('auction/favorite') ?>/"+Id,
            cache:false,
            
            success:function(result){
            location.reload();
             
         }});
     });

     $('.unfavorite').click(function(){
         var Id=$(this).attr('data-id');
 
         $.ajax({url:"<?= base_url('auction/unfavorite') ?>/"+Id,
            cache:false,
            
            success:function(result){
            location.reload();
             
         }});
     });

     

 </script> 
 <script>
     $('.show-fish-detail').click(function(){
         var Id=$(this).attr('data-id');
 
         $.ajax({url:"<?= base_url('auction/detail_modal') ?>/"+Id,
            cache:false,
            beforeSend:function(){
                $("#fish-detail-content").html("Please wait....");
                },
            success:function(result){
            $("#fish-detail-content").html(result);
             
         }});
     });

     $('.show-history').click(function(){
         var Id=$(this).attr('data-id');
 
         $.ajax({url:"<?= base_url('auction/history_modal') ?>/"+Id,
            cache:false,
            beforeSend:function(){
                $("#view-history").html("Please wait....");
                },
            success:function(result){
            $("#view-history").html(result);
             
         }});
     });
 </script>

  
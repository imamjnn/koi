<?php if($items): ?>
<?php foreach($items as $itm): ?>
<div class="row">
    <div class="col-lg-12">
        <h4 class="modal-title block-title-modal secondary-title"><?= $itm->name ?></h4><button class="close" data-dismiss="modal" type="button">&times;</button>
    </div>
</div>
<div class="row">
    <!--image-->
    <div class="col-sm-4"><img class="col-xs-12 no-gutter" src="<?= $itm->photo->_270x450 ?>"></div><!--details-->
    <div class="col-sm-8 modal-custom-content">
        <table class="table table-borderless table-no-margin" id="item-table">
            <tr>
                <td class="item-attribute-label">CODE</td>
                <td style="width: 10px"> : </td>
                <td style="width: 150px"> GK<?= $itm->id ?></td>
            </tr>
            
            <tr>
                <td class="item-attribute-label" style="width: 150px">BREEDER</td>
                <td style="width: 10px">:</td>
                <?php if(property_exists($itm, 'breeder')): ?>
                    <?php foreach($itm->breeder as $bree): ?>
                        <td style="width: 150px"> <?= $bree->fullname ?></td>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tr>
            <tr>
                <td class="item-attribute-label">VARIETY</td>
                <td>:</td>
                <?php if(property_exists($itm, 'variety')): ?>
                    <?php foreach($itm->variety as $vari): ?>
                        <td> <?= $vari->name ?></td>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tr>
            <tr>
                <td colspan="3"><strong><?= $itm->size ?> | <?= $itm->age ?> | <?= $itm->gender ?></strong></td>
            </tr>
        </table>
    </div>
    <div class="col-sm-8 modal-custom-content">
        <table class="table table-borderless table-no-margin" id="item-table">
            <tr>
                <td class="item-attribute-label" style="width: 150px">CLOSE TIME</td>
                <td style="width: 10px">:</td>
                <td class="close-time" style="width: 150px"><?= $itm->time_end->format('d M Y H:i') ?></td>
            </tr>
            <tr>
                <td class="item-attribute-label">REMAINING TIME</td>
                <td>:</td>
                <td>
                    <div class="finishTime">
                        <?php
                            $now = new DateTime('now');
                            $end = new DateTime($itm->time_end->format('d M Y H:i:s'));
                            $diff = $now->diff($end);

                            echo $diff->format('%d Days %H:%I');
                        ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="item-attribute-label">START PRICE</td>
                <td>:</td>
                <td>Rp <?= number_format($itm->price,'0',',','.') ?></td>
            </tr>
            <tr>
                <td class="item-attribute-label">CURRENT PRICE</td>
                <td>:</td>
                <td style="color: red">
                    <?php if(!$bids_current): ?>
                        Rp. -
                    <?php else: ?>
                        Rp <?= number_format($bids_current->bid_price,'0',',','.') ?>
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td class="item-attribute-label">
                <a class="btn btn-danger" href="<?= base_url('auction/item/'.$itm->id) ?>">BID</a>
                </td>
            </tr>
        </table>
        <hr>
    </div>
    <div class="col-md-8 modal-custom-content">
        <label>Note :</label>
        <p><?= $item->description ?></p>
    </div>
</div>
<?php endforeach; ?>
<?php endif; ?>
<?= $this->theme->file('partial/header2') ?>

        <script type="text/javascript" charset="utf-8">
            $(window).load(function() {
                $('#slider-block.flexslider').flexslider({
                    slideshowSpeed: 6000,
                    animationSpeed: 1000,
                    animation: "fade",
                    controlNav: false,
                    directionNav: false,
                    useCSS: false
                });
            });
        </script>

        <div class="row">
            <?= $this->theme->file('partial/sidebar') ?>
            <div id="content" class="col-md-9">
                <div id="content-wrapper">
                    
                    
                    <!-- fish details -->
                        <div class="row" style="margin: 20px 0px 10px">
                          <a class="btn btn-custom-elastic small-txt pull-right" href="<?= $auctions->page ?>"><< Back to Auction Room</a>
                        </div>
                        <div class="row" style="margin-bottom: 30px;">
                            <div class="col-xs-12">
                                <h4 class="block-title-label text-uppercase"><?= $item->name ?></h4>
                            </div>
                            <div class="col-xs-5 col-sm-3 col-md-3">
                                <div id="badgeBlock61" style="position: relative"></div>
                                <a class="image-popup-no-margins" href="<?= $item->photo ?>"><img class="thumbnail" src="<?= $item->photo->_144x177 ?>" style="width: 100%;" /></a>
                            </div>
                            <div class="col-sm-9 col-md-9">
                                <div class="col-xs-12 col-sm-6 col-md-6 no-gutter">
                                    <table class="table table-borderless table-no-margin" id="item-table">
                                       
                                        <tr>
                                            <td style="width: 150px" class="item-attribute-label">BREEDER</td>
                                            <td style="width: 10px"> : </td>
                                                <?php if(property_exists($item, 'breeder')): ?>
                                                <?php foreach($item->breeder as $bree): ?>
                                            <td style="width: 150px"> <?= $bree->fullname ?></td>
                                                <?php endforeach; ?>
                                                <?php endif; ?>
                                            
                                        </tr>
                                        <tr>
                                            <td class="item-attribute-label">VARIETY</td>
                                            <td> : </td>
                                            <?php if(property_exists($item, 'variety')): ?>
                                                <?php foreach($item->variety as $vari): ?>
                                                 <td> <?= $vari->name ?></td>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><strong><?= $item->size ?> | <?= $item->age ?> | <?= $item->gender ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td class="item-attribute-label">CERTIFICATE</td>
                                            <td> : </td>
                                            <td> <a class="image-popup-fit-width" href="<?= $item->certificate ?>">View</a></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 no-gutter">
                                    <table class="table table-borderless table-no-margin" id="item-table">
                                        <tr>
                                            <td style="width: 150px" class="item-attribute-label">CLOSE TIME</td>
                                            <td style="width: 10px"> : </td>
                                            <td style="width: 150px" class="close-time"> <?= $item->time_end->format('d M Y H:i') ?> </td>
                                        </tr>
                                            <td class="item-attribute-label">START PRICE</td>
                                            <td> : </td>
                                            <td> Rp <?= number_format($item->price,0,",",".") ?> </td>
                                        </tr>
                                        <tr>
                                            <td class="item-attribute-label">CURRENT PRICE</td>
                                            <td> : </td>
                                            
                                            <td style="color: red">
                                            <?php if(!$bids_current): ?>
                                                Rp -
                                            <?php else: ?>
                                                Rp <?= number_format($bids_current->bid_price,0,",",".") ?>
                                            <?php endif; ?> 
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-xs-12 no-gutter">
                                    <hr>
                                    <label>Note :</label>
                                    <p><?= $item->description ?></p>
                                </div>
                            </div>
                        </div>
                    <!-- fish details end -->
                        
                    
                    <hr class="no-top-margin favorite-tbl">
                    <?= $this->theme->file('partial/bid_area_v2') ?>
   
                </div>
            </div>
        </div> <!--Content End-->

        <div id="footer-block" class="row">
            <div class="col-md-12" style="text-align: center; color: #fff; padding-top: 50px; padding-bottom: 10px; text-shadow: none">&copy; Gading Koi Auction &#64; 2016</div>
        </div>    

    </div>

</div>

</body>

</html>
<style type="text/css">
  .table > thead > tr > th,
.table > tbody > tr > th,
.table > tfoot > tr > th,
.table > thead > tr > td,
.table > tbody > tr > td,
.table > tfoot > tr > td {
  padding: 5px;
  line-height: 1.42857143;
  vertical-align: top;
  border-top: 1px solid #ddd;
  font-size: 12px;
}
</style>
<center>
<div class="modal-header">
	<button class="close" data-dismiss="modal" type="button">&times;</button>
	<p>Bid History</p>  
</div>
<div class="modal-body">
<?php if(!$bids): ?>
  not found
<?php else: ?>
	          
  <table class="table borderless">
    <tbody>
    <b>Winner :</b> <?= $bids_current->user->fullname ?> <i style="color:red">(Rp <?= number_format($bids_current->bid_price,0,',','.') ?>)</i>
    <?php if($bids): ?>
    	<?php foreach($bids as $bid): ?>
    	<tr>
        	<td><?= $bid->time->format('d F Y - H:i') ?></td>
        	<td><?= $bid->user->fullname ?></td>
        	<td>Rp <?= number_format($bid->bid_price,0,',','.') ?></td>
      	</tr>
      	<?php endforeach; ?>
  	<?php endif; ?>
    </tbody>
  </table>
<?php endif; ?>

</div>
</center>

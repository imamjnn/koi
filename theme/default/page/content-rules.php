<div class="row">
    <div class="col-sm-12 rules" style="padding: 10px 30px; margin: 20px 0px; border: solid 1px #e6e6e6">
        <h2>Auction Rule</h2>
        <ul id="terms" style="text-align: justify;">
            <li>
                <div class="eng">
                    <h5 class="terms-number">1. The Auction Of This Service Adopts An Automatic Bidding System.</h5>
                    <p>When you place a bid, you enter the maximum amount you are willing to pay. We will place bids on your behalf using the automatic bidding system, which is based on the current high bid. We will bid only as much as necessary to make sure that you remain the high bidder, up to your maximum amount.??</p>
                </div>
                <div>
                    <h5 class="terms-number">1. Layanan Lelang Ini Mengadopsi Sitem Penawaran Otomatis.?</h5>
                    <p>Ketika anda melakukan penawaran, anda akan memasukkan jumlah maksimum penawaran yang anda bersedia untuk bayar. Kami akan mengajukan penawaran atas nama anda dengan menggunakan sistem penawaran otomatis, yang berdasarkan kepada tawaran tertinggi saat itu. Kami akan melakukan penawaran kembali hanya sebanyak jumlah yang dipelukan untuk memastikan bahwa anda akan tetap menjadi penawar dengan penawaran tertinggi, hingga pada jumlah maksimum yang Anda telah tetapkan pada sebelumnya.?</p>
                </div>
            </li>
            <li>
                <div class="eng">
                    <h5 class="terms-number">2. The Bidding Period, The Bidding Time, Successful Bid Time And Time Concerning Management Is Based On West Indonesia Time.??</h5>
                </div>
                <div>
                    <h5 class="terms-number">2. Periode Penawaran, Waktu Penawaran, Waktu Ketika Penawaran Berhasil, Dan Waktu Yang Berkaitan Dengan Manajemen Berdasarkan Zona Waktu Indonesia. Bagian Barat(WIB)</h5>
                </div>
            </li>
            <li>
                <div class="eng">
                    <h5 class="terms-number">3. Bidding Is Available Only In The Bidding Period Provided To Each Koi.?</h5>
                    <p>The bidding period starts from the event beginning to the bidding end time. If another bidder places a higher bid 5 minutes before the auction ends, the ending time is extended 5 minutes.</p>
                </div>
                <div>
                    <h5 class="terms-number">3. Penawaran Hanya Dapat Dilakukan Dalam Periode Penawaran Yang Diberikan Kepada Masing - Masing Koi.</h5>
                    <p>Periode penawaran dimulai ketika awal acara berlangsung hingga pada waktu penawaran berakhir. Jika penawar lain melakukan penawaran yang lebih tinggi pada saat 5 menit sebelum waktu penawaran berakhir, maka waktu akhir penawaran akan ditambahkan selama 5 menit.?</p>
                </div>
            </li>
            <li>
                <div class="eng">
                    <h5 class="terms-number">4. After Placing A Bid, We Will Not Accept Cancellation.?</h5>
                    <p>When bidders cancel biddings, we may forbid the use of our service or cancel membership of the bidders in accordance with Gading Koi Auction assessment of the situation.??</p>
                </div>
                <div>
                    <h5 class="terms-number">4. Apabila Telah Melakukan Penawaran, Maka Kami Tidak Akan Menerima Adanya Pembatalan Penawaran.?</h5>
                    <p>Ketika penawar melakukan pembatalan penawaran, maka kami mungkin akan melarang penawar tersebut untuk menggunakan lagi layanan lelang kami ataupun menghapus keanggotaannya dari layanan Lelang Kami, yang dimana tindakan-tindakan tersebut akan terlebih dahulu disesuaikan dengan kondisi dan situasi yang mungkin akan terjadi. Please read the rule for more information.</p>
                </div>
            </li>
            
        </ul>

        <hr>

        <h4 class="block-title" style="margin-bottom:20px">Terms &amp; Conditions</h4>

        <div><span class="eng"><span style="line-height: 1.428571429;">1. Max payment will be three days after auction ends.&nbsp;</span><br></span></div>
        <div><span class="eng">(note: If you need more time please call us)</span><div><span style="font-style: italic;">1. Maksimal pembayaran adalah tiga hari setelah pelelangan berakhir.&nbsp;</span></div><div><span style="font-style: italic;">(catatan: jika anda membutuhkan waktu lebih silahkan hubungi kami)</span></div></div>
        <div><span style="font-style: italic;"><br></span></div>
        <div><span class="eng">2. Fish(es) could be taken in <span style="font-weight: bold;">maksimum one week</span>&nbsp;after auction ends.</span></div>
        <div><span class="eng">(note: if one week after auction ends and the fish still in samuraikoi you'll be guaranteed for the fish. If one week is over or your fish has been shipped the guarantee is over and all is on your own risk)&nbsp;</span></div>
        <div><span style="font-style: italic;">2. Ikan dapat diambil atau dikirim<span style="font-weight: bold;">maksimal</span> <span style="font-weight: bold;">satu minggu</span> setelah pelelangan berakhir.</span></div>
        <div><span style="font-style: italic;">(catatan: jika selama satu minggu tersebut ikan masih berada di Gading Koi maka ikan masih dalam tanggungan Gading Koi. Apabila sudah melewati batas waktu atau ikan sudah dikirimkan maka resiko akan menjadi tanggung jawab pelanggan)</span></div>
        <div><span style="font-style: italic;"><br></span></div>
        <div><span class="eng">3. For customer who lives in Jakarta, the shipping cost is free.</span></div>
        <div><span style="font-style: italic;">3. Untuk pelanggan yang tinggal dikota Jakarta, gratis biaya pengiriman.</span><br></span></div>
        <div><span class="eng">4. For customer who lives outside Jakarta area, please call us for further information about shipping cost. We can send anywhere in Indonesia.</span></div>
        <div><span style="font-style: italic;">4. Untuk pelanggan yang tinggal diluar Jakarta, silahkan hubungi kami untuk informasi lebih lanjut mengenai biaya pengiriman. Kami melayani pengiriman ke seluruh Nusantara</span><br></div>
        <div><br></div>
    </div>
</div>
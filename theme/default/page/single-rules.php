<?= $this->theme->file('partial/header') ?>

<div class="row">
    <?= $this->theme->file('partial/sidebar') ?>
    <div id="content" class="col-md-9">
        <div id="content-wrapper">
            <?= $this->theme->file('page/content-rules') ?>
        </div>
    </div>
</div> <!--Content End-->


<?= $this->theme->file('partial/footer') ?>            
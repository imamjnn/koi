
<div class="row">
    <?= $this->theme->file('partial/sidebar') ?>
    <div id="content" class="col-md-9">
        <div id="content-wrapper">
            <div class="row">
                <div class="col-sm-12 rules" style="padding: 10px 30px; margin: 20px 0px; border: solid 1px #e6e6e6">
                    
                    <?= $page->content ?> 
                    
                </div>
            </div>
        </div>
    </div>
</div> <!--Content End-->
